// Michael Riley 
// 31jan20


#ifndef SKYTRAQ_HPP
#define SKYTRAQ_HPP

#include "Arduino.h"

typedef enum 
{
	msg1 = 0x1,
	msg2 = 0x2
}msgs;



#define msgStart = 0x3F; // ASCII '?'
#define msgDelim = 0x2C; // ASCII ','

typedef struct
{
	double lat, lon, time;
	char north_south, east_west;
	bool validity;
}NMEA_msg;


#pragma region AN0003_Output_System_Messages
uint8_t softwareVersion_msg80[14] = {0x00};
uint8_t softwareCrc_msg81[4] = {0x00};
uint8_t ack_msg83[2] = {0x00};
uint8_t nack_msg84[2] = {0x00};
uint8_t positionUpdateRate_msg86[2] = {0x00};
uint8_t gpsDatum_msgAE[3] = {0x00};
uint8_t gpsWaas_msgB3[2] = {0x00};
uint8_t gpsPinning_msgB4[2] = {0x00};
uint8_t gpsNavMode_msgB5[2] = {0x00};
uint8_t gps1ppsMode_msgB6[2] = {0x00};
#pragma endregion AN0003_Output_System_Messages

#pragma region AN0003_Input_System_Messages

// Force System to restart (0x1)
uint8_t forceSystemResetCmd[22] =
{
	// message header stuffs
	0xA0, 0xA1, 0x00, 0x0F, 0x01, 
	/* Start Mode
	 * 00 = Reserved
	 * 01 = System Reset, Hot Start
	 * 02 = System Reset, Warm Start
	 * 03 = System Reset, Cold Start
	 * 04 = Reserved
	 */
	0x03, 
	// UTC Year must be greater than 1980, 0x07E4 = 2020
	0x07, 0xE4, 
	// UTC Month
	0x01, 
	// UTC Day, 0x1B = 27
	0x1B, 
	// UTC Hour
	0x08, 
	// UTC Minute
	0x2E,
	// UTC Second
	0x03, 
	/* Latitude 
	 * Between – 9000 and 9000
	 * x > 0: North Hemisphere
	 * x < 0: South Hemisphere
	 */
	0x09, 0xC4, 
	/* Longitude
	 * Between – 18000 and 18000
	 * x > 0: East Hemisphere
	 * x < 0: West Hemisphere
	 */
	0x30, 0x70, 
	// altitude 1000->18300
	0x00, 0x64, 
	// end of message shit
	0x16, 0x0D, 0x0A
};

// Query revision information of loaded software (0x2)
uint8_t querySoftwareConfigCmd[9] = 
{
	0xA0, 0xA1, 0x00, 0x02, 0x02, 
	/* Software Type
	 * 00 = Reserved
	 * 01 = System Code
	 */
	0x00, 0x02, 0x0D, 0x0A
};

// Query CRC information of loaded software (0x3)
uint8_t queryCrcValueCmd[9] = 
{
	0xA0, 0xA1, 0x00, 0x02, 0x03, 
	/* Software Type
	 * 00 = Reserved
	 * 01 = System Code
	 */
	0x01, 0x03, 0x0D, 0x0A
};

// Set the system to factory default values (0x4)
uint8_t setSystemDefaultsCmd[9] = 
{
	0xA0, 0xA1, 0x00, 0x02, 0x04, 
	/* Type of factory reset
	 * 00 = Reserved
	 * 01 = Reboot after setting to factory defaults
	*/
	0x01, 
	0x04, 0x0D, 0x0A
};

/* Set up serial port property (0x5)
 * This is a request message which will configure the serial COM port, 
 * baud rate. This command is issued from the host to GPS receiver and GPS 
 * receiver should respond with an ACK or NACK. The payload length is 4 bytes.
 */
uint8_t setupSerialCmd[11] = 
{
	// Start of sequence
	0xA0, 0xA1, 
	// payload length
	0x00, 0x04,
	// Message ID
	0x05,
	// comm port:  Com 1 (only option on this device)
	0x00,
	/* Baud Rate
	 * 0 = 4800
	 * 1 = 9600
	 * 2 = 19200
	 * 3 = 38400
	 * 4 = 57600
	 * 5 = 115200
	 */
	 0x00, 
	 /* Attributes 
	  * 00 = update to SRAM
	  * 01 = update to both SRAM & FLASH
	  */
	 0x01, 
	// checksum
	0x05, 
	// End of sequence
	0x0D, 0x0A
};

/* Configure NMEA message interval (0x8)
 * This is a request message which will set NMEA message configuration. This 
 * command is issued from the host to GPS receiver and GPS receiver should 
 * respond with an ACK or NACK. The payload length is 9 bytes.
 * All intervals unless otherwise noted:  0 ~255, 0: disable
 */
uint8_t configureNmeaMessageIntervalsCmd[16] = 
{
	0xA0, 0xA1, 0x00, 0x09, 0x08,
	// GGA Interval
	0x00,
	// GSA Interval
	0x00, 
	// GSV Interval
	0x00, 
	// GLL Interval - decimal degrees = easy math
	0x01, 
	// RMC Interval
	0x00, 
	// VTG Interval
	0x00,
	// ZDA Interval
	0x00,
	/* Attributes
	 * 00 = update to SRAM
	 * 01 = update to SRAM and FLASH
	 */
	0x01, 
	// enf of message stuff
	0x08, 0x0D, 0x0A
};

// Configure and select output message type (0x9)
uint8_t configureMessageTypeCmd[9] = 
{
	0xA0, 0xA1, 0x00, 0x03, 0x09, 
	/* Type (resets to no output by default)
	 * 00 : No output
	 * 01 : NMEA message
	 * 02 : Binary Message
	 */
	0x01, 
	0x09, 0x0D, 0x0A
};

// Set the power mode of GPS system (0xC)
uint8_t setGpsPowerModeCmd[10] = 
{
	0xA0, 0xA1, 0x00, 0x03, 0x0C,
	/* Mode
	 * 00 = Normal (disable power saving i think) 
	 * 01 = Power Save (enable)
	 */
	0x01,
	/* Attributes
	 * 0: update to SRAM
	 * 1: update to both SRAM & FLASH
	 * 2: temporarily enabled
	 */
	0x01,
	0x0C, 0x0D, 0x0A
};

// Configure the position update rate of GPS system (0xE)
uint8_t configurePositionUpdateRateCmd[10] = 
{
	0xA0, 0xA1, 0x00, 0x03, 0x0E,
	/* Rate
	 * Value with 1, 2, 4, 5, 8, 10 or 20
	 * Note:  20 Supported after firmware kernel version
	 * 01: 1Hz update rate
	 * Note: value with 4 ~10 should work with baud rate 38400 or higher, value 
	 * with 20 should work with baud rate 115200
	 */
	 0x14,  // 20 in decimal
	/* Attributes
	 * 0: update to SRAM
	 * 1: update to both SRAM & FLASH
	 */
	0x01,
	0x0E, 0x0D, 0x0A
};

/* Query the position update rate of GPS system (0x10)
 * This is a request message which is issued from the host to GPS receiver to 
 * query position update rate. The GPS receiver should respond with an ACK 
 * along with information on software version when succeeded and should
 * respond with an NACK when failed. The payload length is 1 byte.
 */
uint8_t queryPositionUpdateRateCmd[8] = 
{
	0xA0, 0xA1, 0x00, 0x01, 0x10, 0x10, 0x0D, 0x0A
};
#pragma endregion AN0003_Input_System_Messages

#pragma region AN0003_Input_GPS_Messages
// Configure datum used for GPS position transformation (0x29)
uint8_t configureGpsDatumCmd[26] = 
{
	// message start stuff
	0xA0, 0xA1, 0x00, 0x13, 0x29,
	// index:  00 = WGS-84
	0x00,
	// Ellip Idx:  0x17 = 23 decimal = WGS-84
	0x17,
	// Delta X, Y, Z :  Zero for WGS-84
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	// Semi-major Axis:  WGS-84 = 6378137 decimal = 0x615299
	0x61, 0x52, 0x99, 
	// Inversed Flattening:  WGS-84 = 298.257223563 decimal = 00012A.41D96746D878F4D
	0x00, 0x01, 0x2A, 
	/* Attributes
	 * 0: update to SRAM
	 * 1: update to both SRAM & FLASH
	 */
	0x01,	 
	0xCE, // why does thi snot match the beginning of the message?
	0x0D, 0x0A
};

// Query datum used by the GPS receiver (0x2D)
uint8_t queryGpsDatumCmd[8] = 
{
	0xA0, 0xA1, 0x00, 0x01, 0x2D, 0x2D,	0x0D, 0x0A
};

// Get ephemeris used of firmware (0x30)
uint8_t queryEphemerisUsedCmd[9] = 
{
	0xA0, 0xA1, 0x00, 0x02, 0x30, 
	/* SV #
	 * 0: means all SVs
	 * 1~32 : mean for the particular SV
	 */
	0x00,
	0x30, 0x0D, 0x0A
};

// Configure the enable or disable of WAAS (0x37)
uint8_t configureWaasCmd[10] = 
{
	0xA0, 0xA1, 0x00, 0x03, 0x37,
	/* enable
	 * 0: disable
	 * 1: enable
	 */
	0x01,
	/* Attributes
	 * 0: update to SRAM
	 * 1: update to both SRAM & FLASH
	 */
	0x01,
	0x36, 0x0D, 0x0A
};

// Query WAAS status of GPS receiver (0x38)
uint8_t queryWaasCmd[8] = 
{
	0xA0, 0xA1, 0x00, 0x01, 0x38, 0x38, 0x0D, 0x0A
};

// Enable or disable position pinning of GPS receiver (0x39)
uint8_t configureGpsPinning[9] = 
{
	0xA0, 0xA1, 0x00, 0x02, 0x39,
	/* Position Pinning
	 * 0: disable (default)
	 * 1: enable
	 */
	 0x01,
	 0x39, 0x0D, 0x0A
};

// Query position pinning status of GPS receiver (0x3A)
uint8_t queryPositionPinningCmd[8] = 
{
	0xA0, 0xA1, 0x00, 0x01, 0x3A, 0x3A,0x0D, 0x0A
};

// Set position pinning parameters of GPS receiver (0x3B)'
uint8_t setPinningParametersCmd[18] = 
{
	0xA0, 0xA1, 0x00, 0x0B, 0x3B,
	// pinning speed
	0x00, 0x02,
	// pinning count
	0x00, 0x0A,
	// unpinning speed
	0x00, 0x08,
	// unpinning count
	0x00, 0x2D,
	// unpinning distance
	0x01, 0xF4,
	0xE3,0x0D, 0x0A
};

// Configure the navigation mode of GPS system (0x3C)
uint8_t configureNavModeCmd[10] = 
{
	0xA0, 0xA1, 0x00, 0x03, 0x3C,
	/* Nav Mode (no projectile...nothing fast enough)
	 * 0: car
	 * 1: pedestrian
	 */
	0x00,
	/* Attributes
	 * 0: update to SRAM
	 * 1: update to both SRAM & FLASH
	 */
	0x01,
	0x3C, 0x0D, 0x0A	
};

// Query the navigation mode of GPS receiver (0x3D)
uint8_t queryNavModeCmd[8] = 
{
	0xA0, 0xA1, 0x00, 0x01, 0x3D, 0x3D, 0x0D, 0x0A
};

// Set 1PPS mode to the GPS receiver (0x3E)
uint8_t set1PpsModeCmd[10] = 
{
	0xA0, 0xA1, 0x00, 0x03, 0x3E, 
	/* 1PPS Mode 
	 * 0: off
	 * 1: on when 3D fix
	 * 2: on when 1 SV
	 */
	0x01,
	/* Attributes
	 * 0: update to SRAM
	 * 1: update to both SRAM & FLASH
	 */
	0x01,
	0x3E, 0x0D, 0x0A
};

// Query 1PPS mode of the GPS receiver (0x3F)
uint8_t query1PpsModeCmd[8] = 
{
	0xA0, 0xA1, 0x00, 0x01, 0x3F, 0x3F, 0x0D, 0x0A
};
#pragma endregion AN0003_Input_GPS_Messages

#pragma region AN0008_Input_Messages
// Enable data read from the log buffer (0x1D)
uint8_t enableGpsReadBufferCmd[12] = 
{
	0xA0, 0xA1, 0x00, 0x05, 0x1D, 
	// Starting log Sector 
	0x00, 0x00, 
	// ending log sector
	0x00, 0x02, 
	0x1F, 0x0D, 0x0A
};

// Request Information of the Log Buffer Status (0x17)
uint8_t requestBufferDataCmd[8] = 
{
	0xA0, 0xA1, 0x00, 0x01, 0x17, 0x17, 0x0D, 0x0A
};

// Configuration Data Logging Criteria (0x18)
uint8_t configureBufferCmd[34] =
{
	0xA0, 0xA1, 0x00, 0x1B, 0x18, 
	// max time:  default 3600
	0x00, 0x00, 0x0E, 0x10,  
	// min time
	0x00, 0x00, 0x00, 0x05,
	// max distance
	0x00, 0x00, 0x00, 0x00,
	// min distance
	0x00, 0x00, 0x00, 0x00,
	// max speed
	0x00, 0x00, 0x00, 0x00,
	// min speed
	0x00, 0x00, 0x00, 0x00,
	/* datalog_enable
	 * 0: disable
	 * 1: enable
	 */
	 0x01,
	 // reserved
	 0x00,
	 0x02, 0x0D, 0x0A
};

// Clear Data Logging Buffer (0x19)
uint8_t clearBufferCmd[8] =
{
	0xA0, 0xA1, 0x00, 0x01, 0x19, 0x19, 0x0D, 0x0A
};
#pragma endregion AN0008_Input_Messages



void sendCommandToGps(uint8_t * whatever)
{
	for (uint8_t index = 0; index <= sizeof(whatever)/sizeof(whatever[0]); index++)
	{
		Serial.write(whatever[index]);
	}
}

#endif // SKYTRAQ_HPP


