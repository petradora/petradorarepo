#include "Wire.h"                                                     
#include <Adafruit_Sensor.h>
#include <Adafruit_LSM303_U.h>
#include <EEPROM.h>

/* Assign a unique ID to this sensor at the same time */
Adafruit_LSM303_Accel_Unified accel = Adafruit_LSM303_Accel_Unified(54321);


/** the current address in the EEPROM (i.e. which byte we're going to write to next) **/
int addr = 0;


void displaySensorDetails(void)
{
  sensor_t sensor;
  accel.getSensor(&sensor);
  Serial.println("------------------------------------");
  Serial.print  ("Sensor:       "); Serial.println(sensor.name);
  Serial.print  ("Driver Ver:   "); Serial.println(sensor.version);
  Serial.print  ("Unique ID:    "); Serial.println(sensor.sensor_id);
  Serial.print  ("Max Value:    "); Serial.print(sensor.max_value); Serial.println(" m/s^2");
  Serial.print  ("Min Value:    "); Serial.print(sensor.min_value); Serial.println(" m/s^2");
  Serial.print  ("Resolution:   "); Serial.print(sensor.resolution); Serial.println(" m/s^2");
  Serial.println("------------------------------------");
  Serial.println("");
  delay(500);
}

void addrUp()
{
    /***
    Advance to the next address, when at the end restart at the beginning.

    Larger AVR processors have larger EEPROM sizes, E.g:
    - Arduno Duemilanove: 512b EEPROM storage.
    - Arduino Uno:        1kb EEPROM storage.
    - Arduino Mega:       4kb EEPROM storage.

    Rather than hard-coding the length, you should use the pre-provided length function.
    This will make your code portable to all AVR processors.
  ***/
  addr = addr + 1;
  if (addr == EEPROM.length()) {
    addr = 0;
  } 
}

void setup(void)
{
#ifndef ESP8266
  while (!Serial);     // will pause Zero, Leonardo, etc until serial console opens
#endif
  Serial.begin(2000000);
  Serial.println("Accelerometer Test"); Serial.println("");

  /* Initialise the sensor */
  if(!accel.begin())
  {
    /* There was a problem detecting the ADXL345 ... check your connections */
    Serial.println("Ooops, no LSM303 detected ... Check your wiring!");
    while(!accel.begin());
  }

  /* Display some basic information on this sensor */
  displaySensorDetails();
}



void loop(void)
{
  /* Get a new sensor event */
  sensors_event_t event;
  accel.getEvent(&event);

  /* Display the results (acceleration is measured in m/s^2) */
//  Serial.print("X: "); Serial.print(event.acceleration.x); Serial.print("  ");
//  Serial.print("Y: "); Serial.print(event.acceleration.y); Serial.print("  ");
//  Serial.print("Z: "); Serial.print(event.acceleration.z); Serial.print("  ");Serial.println("m/s^2 ");

  /***
    Write the value to the appropriate byte of the EEPROM.
    these values will remain there when the board is
    turned off.
  ***/
  unsigned long start = micros();
  // Call to your function

  EEPROM.write(addr, accel.raw.x);
  addrUp();
  Serial.print("\tAddress:\t"); Serial.println(addr);
  EEPROM.write(addr, accel.raw.y);
  addrUp();
  Serial.print("\tAddress:\t"); Serial.println(addr);
  EEPROM.write(addr, accel.raw.z);
  addrUp();
  Serial.print("\tAddress:\t"); Serial.println(addr);

  if(addr == 1023)
  {
    // Compute the time it took
    unsigned long end = micros();
    unsigned long delta = end - start;
    Serial.print("Time to fill EEPROM:\t"); Serial.println(delta);
    exit(0);
  }


//  Serial.print("Wrote X Raw: "); Serial.print(accel.raw.x);
//  EEPROM.write(addr, accel.raw.x);
//  addrUp();
//  Serial.print("\tRead X Raw: "); Serial.print(EEPROM.read(addr)); Serial.print("\tAddress:\t"); Serial.println(addr);
//  Serial.print("Wrote Y Raw: "); Serial.print(accel.raw.y);
//  EEPROM.write(addr, accel.raw.y);
//  addrUp();
//  Serial.print("\tRead Y Raw: "); Serial.print(EEPROM.read(addr));Serial.print("\tAddress:\t"); Serial.println(addr);
//  Serial.print("Wrote Z Raw: "); Serial.print(accel.raw.z);
//  EEPROM.write(addr, accel.raw.z);
//  addrUp();
//  Serial.print("\tRead Z Raw: "); Serial.print(EEPROM.read(addr));Serial.print("\tAddress:\t"); Serial.println(addr);
//  delay(500);



  /* Note: You can also get the raw (non unified values) for */
  /* the last data sample as follows. The .getEvent call populates */
  /* the raw values used below. */
//  Serial.print("X Raw: "); Serial.print(accel.raw.x); Serial.print("  ");
//  Serial.print("Y Raw: "); Serial.print(accel.raw.y); Serial.print("  ");
//  Serial.print("Z Raw: "); Serial.print(accel.raw.z); Serial.println("");

  /* Delay before the next sample */
//  delay(500);
}
