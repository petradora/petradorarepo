/*
 * acc.cpp
 *
 *  Created on: Oct 27, 2020
 *      Author: HP
 */

#include <ConfigInits/acc.hpp>

uint32_t getADCValue[4];


/* adc init
* Description: In this application example the ADC0 is configured for a single
* sequencer sampling 3 channels in single ended mode. The data is then
* displayed on the serial console.
*
*                MSP432E401Y
*             ------------------
*         /|\|               |
*          | |               PE2|<-- AIN1
*          --|RST            PE1|<-- AIN2
*            |               PE0|<-- AIN3
*            |                  |
*            |                  |
*            |               PA0|<--U0RX
*            |               PA1|-->U0TX
*/
/* adc init
* Description: In this application example the ADC0 is configured for a single
* sequencer sampling 3 channels in single ended mode. The data is then
* displayed on the serial console.
*
*                MSP432E401Y
*             ------------------
*         /|\|               |
*          | |               PE2|<-- AIN1
*          --|RST            PE1|<-- AIN2
*            |               PE0|<-- AIN3
*            |                  |
*            |                  |
*            |               PA0|<--U0RX
*            |               PA1|-->U0TX
*/
void adcInit(void)
{
    // Enable the clock to GPIO Port E and wait for it to be ready
    MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
    while(!(MAP_SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOE)))
    {
    }

    UARTprintf("adcInit Port E ready \n\r");

    /* Configure PE0-PE3 as ADC input channel
     * PE3 = AIN0 = ACC_X
     * PE2 = AIN0 = ACC_Y
     * PE1 = AIN0 = ACC_Z
     */
    MAP_GPIOPinTypeADC(GPIO_PORTE_BASE, GPIO_PIN_3);
    MAP_GPIOPinTypeADC(GPIO_PORTE_BASE, GPIO_PIN_2);
    MAP_GPIOPinTypeADC(GPIO_PORTE_BASE, GPIO_PIN_1);

    UARTprintf("adcInit Port E - Pins 1-3 configured\n\r");

    /* Enable the clock to ADC-0 and wait for it to be ready */
    MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);
    while(!(MAP_SysCtlPeripheralReady(SYSCTL_PERIPH_ADC0)))
    {
    }
    UARTprintf("adcInit clock enabled to ADC0\n\r");


    /* Configure Sequencer 2 to sample the analog channel : AIN0-AIN3. The
     * end of conversion and interrupt generation is set for AIN3 */
    MAP_ADCSequenceStepConfigure(ADC0_BASE, 2, 0, ADC_CTL_CH0);
    MAP_ADCSequenceStepConfigure(ADC0_BASE, 2, 1, ADC_CTL_CH1);
    MAP_ADCSequenceStepConfigure(ADC0_BASE, 2, 2, ADC_CTL_CH2| ADC_CTL_IE |
                                 ADC_CTL_END);
//    MAP_ADCSequenceStepConfigure(ADC0_BASE, 2, 3, ADC_CTL_CH3 | ADC_CTL_IE |
//                                 ADC_CTL_END);
    UARTprintf("adcInit ADC0 sequencer 2 configured for AIN0-2\n\r");

    /* Enable sample sequence 2 with a processor signal trigger.  Sequencer 2
     * will do a single sample when the processor sends a signal to start the
     * conversion */
    MAP_ADCSequenceConfigure(ADC0_BASE, 2, ADC_TRIGGER_PROCESSOR, 0);
    UARTprintf("adcInit ADC0 sequencer 2 configured to processor trigger \n\r");

    /* Since sample sequence 2 is now configured, it must be enabled. */
    MAP_ADCSequenceEnable(ADC0_BASE, 2);
    UARTprintf("adcInit ADC0 sequencer 2 enabled\n\r");

    /* Clear the interrupt status flag.  This is done to make sure the
     * interrupt flag is cleared before we sample. */
    MAP_ADCIntClear(ADC0_BASE, 2);
    UARTprintf("adcInit ADC0 interrupt cleared\n\r");
}

// Accelerometer Init
void accInit(void)
{

    // TODO may not need this, since ADC alreay configured
    UARTprintf("accInit blah blah \n\r");
}

// Interrupt service routines
void accIsr(void)
{
    // testing adc settings
    /* Trigger the ADC conversion. */
    MAP_ADCProcessorTrigger(ADC0_BASE, 2);

    /* Wait for conversion to be completed. */
    while(!MAP_ADCIntStatus(ADC0_BASE, 2, false))
    {
    }

    /* Clear the ADC interrupt flag. */
    MAP_ADCIntClear(ADC0_BASE, 2);

    /* Read ADC Value. */
    MAP_ADCSequenceDataGet(ADC0_BASE, 2, getADCValue);

    /* Display the AIN0-AIN03 (PE3-PE0) digital value on the console. */
    UARTprintf("AIN0-3 = %4d %4d %4d\n\r", getADCValue[0],
               getADCValue[1], getADCValue[2]);
}


