/*
 * bmg250.hpp
 *
 *  Created on: Oct 28, 2020
 *      Author: HP
 */

#ifndef BMG250_HPP_
#define BMG250_HPP_

/********************************* I2C *************************/
/* Defines for I2C bus parameters */
#define SLAVE_ADDRESS   0X68        // default I2C address for BMG-250

// bmg250 registers
#define BMG250_CHIPID           0X00
#define BMG250_ERR_REG          0x02
#define BMG250_PMU_STATUS       0x03
#define BMG250_DATA_01          0X12
#define BMG250_DATA_02          0X13
#define BMG250_DATA_03          0X14
#define BMG250_DATA_04          0X15
#define BMG250_DATA_05          0X16
#define BMG250_DATA_06          0X17
#define BMG250_SENSEOR_TIME_01  0X18
#define BMG250_SENSEOR_TIME_02  0X19
#define BMG250_SENSEOR_TIME_03  0X1A
#define BMG250_SENSEOR_STATUS   0X1B
#define BMG250_INT_STATUS_01    0X1D
#define BMG250_TEMP_01          0X20
#define BMG250_TEMP_02          0X21
#define BMG250_FIFO_LENGTH_01   0X22
#define BMG250_FIFO_LENGTH_02   0X23
#define BMG250_FIFO_DATA        0X24
#define BMG250_GYR_CONF         0X42
#define BMG250_RANGE            0X43
#define BMG250_FIFO_DOWNS       0X45
#define BMG250_FIFO_CONFIG_01   0X46
#define BMG250_FIFO_CONFIG_02   0X47
#define BMG250_INT_EN           0X51
#define BMG250_INT_OUT_CTRL     0X53
#define BMG250_INT_IN_CTRL      0X54
#define BMG250_INT_MAP          0X56
#define BMG250_CONF             0X6A
#define BMG250_IF_CONF          0X6B
#define BMG250_PMU_TRIGGER      0X6C
#define BMG250_SELF_TEST        0X6D
#define BMG250_NV_CONF          0X70
#define BMG250_OFFSET_01        0X74
#define BMG250_OFFSET_02        0X75
#define BMG250_OFFSET_03        0X76
#define BMG250_OFFSET_04        0X77
#define BMG250_CMD              0X7E

// config writes
/* 9.1.5.5  Gyroscope Configuration
 * Filter mode � Normal
 * Output Data Rate (ODR) � 1600 Hz
 */
#define BMG250_CONFIG_WR        0X2C


// i2c config command strings
#define BMG250_FIFO_CONFG_CMD_01    0x80
#define BMG250_FIFO_CONFG_CMD_02    0x91
#define BMG250_INT_EN_CMD           0x30
#define BMG250_INT_OUT_CTRL_CMD     0xBB
#define BMG250_INT_IN_CTRL_CMD      0x00
#define BMG250_INT_MAP_CMD          0x82
#define BMG250_IF_CONF_CMD          0x00
#define BMG250_NV_CONF_CMD          0x06
#define BMG250_PMU_STATUS_CMD       0x04

#endif /* BMG250_HPP_ */
