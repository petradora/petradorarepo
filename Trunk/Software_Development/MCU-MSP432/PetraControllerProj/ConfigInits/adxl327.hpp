/*
 * adxl327.hpp
 *
 *  Created on: Oct 28, 2020
 *      Author: HP
 */

#ifndef ADXL327_HPP_
#define ADXL327_HPP_

/* Defines for I2C bus parameters */
#define SLAVE_ADDRESS   0x6A        // default I2C address for L3GD20H

/*=========================================================================
    I2C ADDRESS/BITS AND SETTINGS
    -----------------------------------------------------------------------*/
#define L3GD20_ADDRESS (0x6B)     //!< L3gD20 I2C address; 1101011 in binary
#define L3GD20_POLL_TIMEOUT (100) //!< Maximum number of read attempts
#define L3GD20_ID (0xD4)          //!< L3GD20 ID
#define L3GD20H_ID (0xD7)         //!< L3GD20H ID
// Sesitivity values from the mechanical characteristics in the datasheet.
#define GYRO_SENSITIVITY_250DPS (0.00875F) //!< Sensitivity at 250 dps
#define GYRO_SENSITIVITY_500DPS (0.0175F)  //!< Sensitivity at 500 dps
#define GYRO_SENSITIVITY_2000DPS (0.070F)  //!< Sensitivity at 2000 dps
/*=========================================================================*/

/*!
 * @brief Registers
 */
typedef enum {                        // DEFAULT    TYPE
  GYRO_REGISTER_WHO_AM_I = 0x0F,      // 11010100   r
  GYRO_REGISTER_CTRL_REG1 = 0x20,     // 00000111   rw
  GYRO_REGISTER_CTRL_REG2 = 0x21,     // 00000000   rw
  GYRO_REGISTER_CTRL_REG3 = 0x22,     // 00000000   rw
  GYRO_REGISTER_CTRL_REG4 = 0x23,     // 00000000   rw
  GYRO_REGISTER_CTRL_REG5 = 0x24,     // 00000000   rw
  GYRO_REGISTER_REFERENCE = 0x25,     // 00000000   rw
  GYRO_REGISTER_OUT_TEMP = 0x26,      //            r
  GYRO_REGISTER_STATUS_REG = 0x27,    //            r
  GYRO_REGISTER_OUT_X_L = 0x28,       //            r
  GYRO_REGISTER_OUT_X_H = 0x29,       //            r
  GYRO_REGISTER_OUT_Y_L = 0x2A,       //            r
  GYRO_REGISTER_OUT_Y_H = 0x2B,       //            r
  GYRO_REGISTER_OUT_Z_L = 0x2C,       //            r
  GYRO_REGISTER_OUT_Z_H = 0x2D,       //            r
  GYRO_REGISTER_FIFO_CTRL_REG = 0x2E, // 00000000   rw
  GYRO_REGISTER_FIFO_SRC_REG = 0x2F,  //            r
  GYRO_REGISTER_INT1_CFG = 0x30,      // 00000000   rw
  GYRO_REGISTER_INT1_SRC = 0x31,      //            r
  GYRO_REGISTER_TSH_XH = 0x32,        // 00000000   rw
  GYRO_REGISTER_TSH_XL = 0x33,        // 00000000   rw
  GYRO_REGISTER_TSH_YH = 0x34,        // 00000000   rw
  GYRO_REGISTER_TSH_YL = 0x35,        // 00000000   rw
  GYRO_REGISTER_TSH_ZH = 0x36,        // 00000000   rw
  GYRO_REGISTER_TSH_ZL = 0x37,        // 00000000   rw
  GYRO_REGISTER_INT1_DURATION = 0x38  // 00000000   rw
} gyroRegisters_t;


/*!
 * @brief Optional speed settings
 */
typedef enum {
  GYRO_RANGE_250DPS = 250,
  GYRO_RANGE_500DPS = 500,
  GYRO_RANGE_2000DPS = 2000
} gyroRange_t;

#endif /* ADXL327_HPP_ */
