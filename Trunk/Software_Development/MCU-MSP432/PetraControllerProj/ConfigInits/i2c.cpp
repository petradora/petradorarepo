/*
 * i2c.cpp
 *
 *  Created on: Oct 27, 2020
 *      Author: Michael Riley
 */

#include <ConfigInits/i2c.hpp>

using namespace std;

// externs
//extern std::chrono::milliseconds SysOpTimeStart;


uint32_t systemClock = 0;
//uint8_t sendData[I2C_NUM_DATA] = {0xA5, 0x36, 0x67, 0x44};
//uint8_t getData[I2C_NUM_DATA]  = {0x00, 0x00, 0x00, 0x00};
uint8_t setI2CState;
uint8_t dataIndex;

// gyroscope Init
void gyroInit(void)
{

    // change the below line for the correct gyroscope
    #define ADXL327

    #if defined(BMG250)
        // do stuff

    // need to wait 50 ms before attempting to communicate
    #elif defined(ADXL327)
        // do other stuff

    #endif
    // TODO may not need this since I2C already configured..
    // or at least test comms here
    UARTprintf("gyroInit blah blah \n\r");
}

/* i2c init
 *  * MSP432E4 Example Project for I2C-Master for Simple Write and Read.
 *
 * Description: This application example configures the I2C module for master
 * mode operation with standard speed. The use of the example requires another
 * MSP-EXP432E401Y board to be running the i2c_slavemode_simple_transfer
 * application. The master board sends a data to the slave and the slave
 * inverts the bits. The master board reads the data from the slave and
 * compares the read data from the slave with the inverted master data. If
 * there is an error in transmission the LED D2 is switched ON.
 *
 *                MSP432E401Y                      MSP432E401Y
 *             ------------------               ------------------
 *         /|\|      MASTER      |             |      SLAVE       |
 *          --|RST            PG0|<->I2C1SCL<->|PG0               |
 *            |               PG1|<->I2C1SDA<->|PG1               |
 *            |               PN0|-->LED D2    |                  |
 * Author:
*******************************************************************************/
void i2cInit(void)
{
    /* Enable clocks to GPIO Port G and configure pins as I2C */
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOG);
    while(!(SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOG)))
    {
    }

    UARTprintf("i2cInit clock enabled for Port G \n\r");

    GPIOPinConfigure(GPIO_PG0_I2C1SCL);
    GPIOPinConfigure(GPIO_PG1_I2C1SDA);
    GPIOPinTypeI2C(GPIO_PORTG_BASE, GPIO_PIN_1);
    GPIOPinTypeI2CSCL(GPIO_PORTG_BASE, GPIO_PIN_0);

    UARTprintf("i2cInit Port G pins configured for I2C \n\r");

    /* Since there are no board pull up's we shall enable the weak internal
     * pull up */
    // TODO I may not need to do this since we have pull ups in our design
    GPIOG->PUR |= (GPIO_PIN_1 | GPIO_PIN_0);

    UARTprintf("i2cInit weak internal pull ups enabled\n\r");

    /* Enable the clock to I2C-1 module and configure the I2C Master */
    SysCtlPeripheralEnable(SYSCTL_PERIPH_I2C1);
    while(!(SysCtlPeripheralReady(SYSCTL_PERIPH_I2C1)))
    {
    }

    UARTprintf("i2cInit clock enabled I2C1 \n\r");
    /* Configure the I2C Master in standard mode and enable interrupt for Data
     * completion, NAK and Stop condition on the bus */
    I2CMasterInitExpClk(I2C1_BASE, systemClock, false);
    I2CMasterIntEnableEx(I2C1_BASE, I2C_MASTER_INT_NACK |
                                        I2C_MASTER_INT_STOP |
                                        I2C_MASTER_INT_DATA);

    UARTprintf("i2cInit I2C configured for master mode\n\r");

    /* Initialize the state of the I2C Master */
    setI2CState = I2C_MASTER_IDLE;

    UARTprintf("i2cInit state of master set\n\r");

    /* Enable the interrupt generation from I2C-1 */
    IntEnable(INT_I2C1);
    UARTprintf("i2cInit interrupt cleared\n\r");
}

void i2cWrite(uint8_t anyReg, uint8_t cmd)
{
    // send the register we want to write to
    I2CMasterDataPut(I2C1_BASE, anyReg);
    // TODO i think i have to wait here but am not sure
    I2CMasterDataPut(I2C1_BASE, cmd);
}

uint8_t i2cRead(uint8_t anyReg)
{
    uint8_t gyroData = {0};

    // Send the register we want to read to the bmg250
    I2CMasterDataPut(I2C1_BASE, anyReg);

    // while data is available read and fill a buffer
    gyroData =  I2CMasterDataGet(I2C1_BASE);

    setI2CState = I2C_MASTER_IDLE;

    // return read data
    return gyroData;
}



GyroStartUpHealthEnumType startUpHealth(void)
{

//    uint8_t something = i2cRead(BMG250_ERR_REG);

    bitset<8> errorRegData = bitset<8> (i2cRead(BMG250_ERR_REG));

    // quickly check if entire word is zero and return if so
    if(!errorRegData.any())
    {
        return GyroStartUpHealthEnumType::noErr;
    }
    // fatal error
    else if (errorRegData.test(0))
    {
        return GyroStartUpHealthEnumType::fatalErr;
    }
    // determine if an error code is set
    else if(errorRegData.test(4) || errorRegData.test(3) ||
            errorRegData.test(2) || errorRegData.test(1) )
    {
        bitset<4> tempData;
        for (uint8_t index = 0; index <= 3; index++)
        {
            tempData[index] = errorRegData[index + 1];
        }

        if(tempData == 0x2)
        {
            return GyroStartUpHealthEnumType::gyrConfErr;
        }
        else
        {
            return GyroStartUpHealthEnumType::reserved;
        }

    }
    else if (errorRegData.test(6))
    {
        return GyroStartUpHealthEnumType::dropCmdErr;
    }
    else
    {
        return GyroStartUpHealthEnumType::reserved;
    }
}

GyroPwrModeEnumType getGyroPowerMode(void)
{
    bitset<8> PwrModeData = bitset<8> (i2cRead(BMG250_PMU_STATUS));
    switch(PwrModeData[2] + PwrModeData[3])
    {
        case 0: {return GyroPwrModeEnumType::suspend;}
        case 1: {return GyroPwrModeEnumType::normal;}
        case 2: {return GyroPwrModeEnumType::reserved;}
        case 3: {return GyroPwrModeEnumType::fastStartUp;}
    }
}


void I2C1_IRQHandler(void)
{
    uint32_t getIntStatus;
    uint32_t getERRStatus;

    /* Get the interrupt status and clear the same */
    getIntStatus = I2CMasterIntStatusEx(I2C1_BASE, true);
    I2CMasterIntClearEx(I2C1_BASE, getIntStatus);
    setI2CState = I2C_MASTER_IDLE;

//    /* Check if we have a Data Request */
//    if((getIntStatus & I2C_MASTER_INT_DATA) == I2C_MASTER_INT_DATA)
//    {
//        /* Process data interrupt  for the Transmit Path */
//        if((setI2CState == I2C_MASTER_TX) && (dataIndex < I2C_NUM_DATA-1))
//        {
//            I2CMasterDataPut(I2C1_BASE, sendData[dataIndex++]);
//            I2CMasterControl(I2C1_BASE, I2C_MASTER_CMD_BURST_SEND_CONT);
//        }
//        else if((setI2CState == I2C_MASTER_TX) && (dataIndex == I2C_NUM_DATA-1))
//        {
//            I2CMasterDataPut(I2C1_BASE, sendData[dataIndex++]);
//            I2CMasterControl(I2C1_BASE, I2C_MASTER_CMD_BURST_SEND_FINISH);
//        }
//
//        /* Process data interrupt  for the Receive Path */
//        if((setI2CState == I2C_MASTER_RX) && (dataIndex < I2C_NUM_DATA-2))
//        {
//            getData[dataIndex++] = I2CMasterDataGet(I2C1_BASE);
//            I2CMasterControl(I2C1_BASE, I2C_MASTER_CMD_BURST_RECEIVE_CONT);
//        }
//        else if((setI2CState == I2C_MASTER_RX) && (dataIndex == I2C_NUM_DATA-2))
//        {
//            getData[dataIndex++] = I2CMasterDataGet(I2C1_BASE);
//            I2CMasterControl(I2C1_BASE, I2C_MASTER_CMD_BURST_RECEIVE_FINISH);
//        }
//    }

    /* Check if we have a Stop condition on the bus */
//    if((getIntStatus & I2C_MASTER_INT_STOP) == I2C_MASTER_INT_STOP)
//    {
//        if(setI2CState == I2C_MASTER_TX)
//        {
//            setI2CState = I2C_MASTER_IDLE;
//        }
//        else if(setI2CState == I2C_MASTER_RX)
//        {
//            getData[dataIndex] = I2CMasterDataGet(I2C1_BASE);
//            setI2CState = I2C_MASTER_IDLE;
//        }
//    }
//
//    /* Check if we have an ADDR NAK, DATA NAK or ARB LOST condition on the I2C
//     * bus */
//    if((getIntStatus & I2C_MASTER_INT_NACK) == I2C_MASTER_INT_NACK)
//    {
//        /* Set the Error LED */
//        GPIOPinWrite(GPIO_PORTN_BASE, GPIO_PIN_0, GPIO_PIN_0);
//
//        getERRStatus = I2CMasterErr(I2C1_BASE);
//        if((getERRStatus & I2C_MASTER_ERR_ADDR_ACK) == I2C_MASTER_ERR_ADDR_ACK)
//        {
//            setI2CState = I2C_MASTER_ANAK;
//        }
//
//        if((getERRStatus & I2C_MASTER_ERR_DATA_ACK) == I2C_MASTER_ERR_DATA_ACK)
//        {
//            setI2CState = I2C_MASTER_DNAK;
//        }
//
//        if((getERRStatus & I2C_MASTER_ERR_ARB_LOST) == I2C_MASTER_ERR_ARB_LOST)
//        {
//            setI2CState = I2C_MASTER_ALST;
//        }
//    }
//    UARTprintf("i2cISR GetData:   \n\r");
//    for (uint8_t index = 0; index <= 3; index++)
//    {
//        UARTprintf("\n\r");
//    }
//    UARTprintf("i2cISR GetData:   \n\r");
}

///**************************************************************************/
///**
//    @brief  Abstracts away platform differences in Arduino wire library
//
//    @param  reg     The register to write to.
//    @param  value   The value to assign to 'reg'.
//*/
///**************************************************************************/
//void Adafruit_L3GD20_Unified::write8(byte reg, byte value) {
//  _i2c->beginTransmission(L3GD20_ADDRESS);
//#if ARDUINO >= 100
//  _i2c->write((uint8_t)reg);
//  _i2c->write((uint8_t)value);
//#else
//  _i2c->send(reg);
//  _i2c->send(value);
//#endif
//  _i2c->endTransmission();
//}


