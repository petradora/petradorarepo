/*
 * uart.hpp
 *
 *  Created on: Oct 27, 2020
 *      Author: Michael Riley
 */

#ifndef UART_HPP_
#define UART_HPP_

#include <stdint.h>             // explicit integer types
#include <tiUtils/uartstdio.h>
#include <tiUtils/pinout.h>

void ConfigureUART(void);

#endif /* UART_HPP_ */
