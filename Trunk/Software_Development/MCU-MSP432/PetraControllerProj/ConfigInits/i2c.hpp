/*
 * i2c.hpp
 *
 *  Created on: Oct 27, 2020
 *      Author: HP
 */

#ifndef I2C_HPP_
#define I2C_HPP_

// change the below line for the correct gyroscope
//#define ADXL327
#define BMG250
#if defined(BMG250)
    #include <ConfigInits/bmg250.hpp>
#elif defined(ADXL327)
    #include <ConfigInits/adxl327.hpp>

#endif

// c++ STL includes
#include <stdint.h>             // explicit integer types
#include <tiUtils/uartstdio.h>  // TODO remove this later
#include <tiUtils/pinout.h>
#include <bitset>

/* Defines for I2C State Machine */
#define I2C_MASTER_IDLE 0x0
#define I2C_MASTER_TX   0x1
#define I2C_MASTER_RX   0x2
#define I2C_MASTER_ANAK 0x3
#define I2C_MASTER_DNAK 0x4
#define I2C_MASTER_ALST 0x5
#define I2C_MASTER_UNKN 0x6

enum class GyroStartUpHealthEnumType
{
    noErr, reserved, gyrConfErr, dropCmdErr, fatalErr
};

enum class GyroPwrModeEnumType
{
    suspend, normal, reserved, fastStartUp
};

void i2cInit(void);
void MyI2C1_IRQHandler(void);
void gyroInit(void);
void i2cWrite(uint8_t, uint8_t);          // write to gyro
uint8_t i2cRead(uint8_t);                // read from gyro
GyroStartUpHealthEnumType startUpHealth(void);                      // get gyro health
GyroPwrModeEnumType getGyroPowerMode(void);

#endif /* I2C_HPP_ */
