/*
 * PetraContMain.cpp
 *
 *  Created on: Jul 29, 2020
 *      Author: Michael Riley
 */

// custom includes
#include <DataLog/DataLog.hpp>
#include <ConfigInits/acc.hpp>
//#include <ConfigInits/i2c.hpp>
#include <ConfigInits/uart.hpp>

// c++ STL includes
#include <stdint.h>             // explicit integer types
#include <chrono>               // time base conversions
#include <ctime>                // converting time to date string

// TI includes
//#include <stdbool.h>
//#include "ti/devices/msp432e4/driverlib/driverlib.h"

#include <tiUtils/uartstdio.h>
#include <tiUtils/pinout.h>

// SDFatFs includes
#include <ti/drivers/SDFatFS.h>
#include <ti/drivers/SD.h>

// namespaces
using namespace std;
using namespace chrono;

//*****************************************************************************
//
// System clock rate in Hz.
//
//*****************************************************************************
extern uint32_t g_ui32SysClock;

//std::chrono::steady_clock::time_point SysOpTimeStart = steady_clock::duration::zero() ;

steady_clock::time_point SysOpStartTime = std::chrono::steady_clock::now();


//extern uint32_t getADCValue[4];
//extern uint32_t systemClock;
//extern uint8_t getData[4];
//*****************************************************************************
//
// The error routine that is called if the driver library encounters an error.
//
//*****************************************************************************
#ifdef DEBUG
void
__error__(char *pcFilename, uint32_t ui32Line)
{
}
#endif


// clock init function
void cpuInit()
{
    // TODO set to 16MHz
    // Run from the PLL at 120 MHz.
    //
    g_ui32SysClock = SysCtlClockFreqSet((SYSCTL_XTAL_25MHZ |
                SYSCTL_OSC_MAIN | SYSCTL_USE_PLL |
                SYSCTL_CFG_VCO_480), 120000000);
}

// USB stack init
void usbInit()
{

    UARTprintf("usbInit blah blah \n\r");
}

// spi init
void spInit()
{
    UARTprintf("spInit blah blah \n\r");
}

// sd card init
void sdCardInit()
{
    // TODO hard point...make sure we can can comm with SD card
    // might brick it...need to research
    UARTprintf("sdCardInit blah blah \n\r");
}

// do shit
int main()
{
    // cpu init
    cpuInit();

    // get the system start time after cpu init configures the clock
//    SysOpTimeStart = duration_cast< std::chrono::milliseconds >
//        (
//            std::chrono::system_clock::now().time_since_epoch()
//        );

    /*
     * Initialize the UART.
     */
    ConfigureUART();

    UARTprintf("cpuInit clock set to 120MHz\n\r");

    UARTprintf("UART initialized and configured\n\r");

    // gpio interrupt shit
    /* Enable the clock to the GPIO Port B and wait for it to be ready */
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);


    while(!(SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOB)))
    {
    }

    /* Configure the GPIO PB2 as input with internal pull up enabled. Configure
     * the PB2 for a rising edge interrupt detection */
//    GPIOPinTypeGPIOInput(GPIO_PORTB_BASE, GPIO_PIN_2);
//    GPIOB->PUR |= GPIO_PIN_2;
//    GPIOIntTypeSet(GPIO_PORTB_BASE, GPIO_PIN_2, GPIO_RISING_EDGE);
//    GPIOIntEnable(GPIO_PORTB_BASE, GPIO_INT_PIN_2);

    GPIOPinTypeGPIOInput(GPIO_PORTB_BASE, GPIO_PIN_3);
    GPIOB->PUR |= GPIO_PIN_3;
    GPIOIntTypeSet(GPIO_PORTB_BASE, GPIO_PIN_3, GPIO_RISING_EDGE);
    GPIOIntEnable(GPIO_PORTB_BASE, GPIO_INT_PIN_3);

    IntEnable(INT_GPIOB);

    adcInit();
//    usbInit();
//    spInit();
//    accInit();
//      i2cInit();
//    gyroInit();
//    sdCardInit();

    // current date/time based on current system
//    time_t now = time(0);


    // convert now to string form
//    char* dt = ctime(&now);

    /*
     * Configure the device pins. Turn on USB
     *
     * PinoutSet(bool bEthernet, bool bUSB)
     */
    PinoutSet(false, true);

    //
    // Enable the GPIO pins for the LED D1 (PN1).
    //
    GPIOPinTypeGPIOOutput(GPIO_PORTN_BASE, GPIO_PIN_1);

    DataLog sensorData();
    DataLog stuff(5.69, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0);

    // both of these work fine
//    UARTprintf("Main uart Configured!\n\r");
//        UARTCharPut(UART0,'z');

    //
    // We are finished.  Hang around flashing D1.
    //
    while(1)
    {
        accIsr();


        /* Delay the next sampling */
        //SysCtlDelay(systemClock / 12);


//        // both of these work fine
//        UARTprintf("Hello, Kira!\n");
////        UARTCharPut(UART0,'z');
        //
        // Turn on D1.
        //
        LEDWrite(CLP_D1, 1);

        //
        // Delay for a bit.
        //
        SysCtlDelay(g_ui32SysClock / 10 / 1);

        //
        // Turn off D1.
        //
        LEDWrite(CLP_D1, 0);

        //
        // Delay for a bit.
        //
        SysCtlDelay(g_ui32SysClock / 10 / 3);
    }
    return 0;
}

