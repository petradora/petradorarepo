/*
 * DataLog.cpp
 *
 *  Created on: Jul 29, 2020
 *      Author: Michael Riley
 */

#include <DataLog/DataLog.hpp>

// Overloaded constructor
DataLog::DataLog (uint64_t ts, float acx, float acy, float acz,
                  float gx, float gy, float gz)
{
    timeStamp = ts;
    accX = acx;
    accY = acy;
    accZ = acz;
    gyroX = gx;
    gyroY = gy;
    gyroZ = gz;
}

// default constructor
DataLog::DataLog ()
{
    timeStamp = 0.0;
    accX = 0.0;
    accY = 0.0;
    accZ = 0.0;
    gyroX = 0.0;
    gyroY = 0.0;
    gyroZ = 0.0;
}

