/*
 * DATALOG.hpp
 *
 *  Created on: Jul 29, 2020
 *      Author: Mike Riley
 */

#ifndef DATALOG_HPP_
#define DATALOG_HPP_

#include <stdint.h>     // explicit integer types

class DataLog
{
public:
    DataLog (uint64_t ts, float acx, float acy, float acz,
             float gx, float gy, float gz);

    DataLog ();

private:
    uint64_t timeStamp = {0};
    float accX = {0};
    float accY = {0};
    float accZ = {0};
    float gyroX = {0};
    float gyroY = {0};
    float gyroZ = {0};
};
#endif /* DATALOG_HPP_ */
