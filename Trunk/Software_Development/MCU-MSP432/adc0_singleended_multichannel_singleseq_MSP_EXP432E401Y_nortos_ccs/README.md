# MSP432E4 Example project for ADC with multiple channel and single sequencer

In this application example the ADC0 is configured for a single sequencer 
 sampling 4 channels in single ended mode. The data is then displayed on the 
 serial console.