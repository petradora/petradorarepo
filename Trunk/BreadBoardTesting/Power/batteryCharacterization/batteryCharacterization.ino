// global variables
int batteryVoltage = 0;           // stores battery voltage
const int batteryVoltagePin = A0; // pin for reading battery voltage
const String aboutProgram = 
      "\n\rThis program runs for 30 minutes and measures the battery voltage \n\r" \
      "present on pin A0.  The goal of this program is to characterize the \n\r" \
      "discharge profile of the PETRA battery over time\n\r" \
       "Michael Riley - 16OCT20 - Have fun!\n\n\r";
const String programUsage = 
      "\n\rTools->Serial Monitor.  Select 9600 Baud rate\n\r" \
      "You may have to adjust the values in the scaleVoltage() function...\n\r" \
      "Connect a battery and load to pin A0 and wait 30 mins.\n\r" \
      "The Battery voltage after 30 mins will be the last output to the terminal\n\r";

// simply scaling battery voltage for human readable output                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
float scaleVoltage(int voltageRead)
{
  return voltageRead * (5.0 / 1023.0);
}

/* 
 *  Arduino board initialization
 *  
 *  this is where you configure your I/O pins
 */
void setup() 
{
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
    // initialize digital pin LED_BUILTIN as an output.
  pinMode(extLedPin, OUTPUT);

  // setup serial comms for uart
  Serial.begin(9600);
  while (!Serial) 
  {
    ; // wait for serial port to connect. Needed for native USB port only
  }

  // print welcome banner
  Serial.print("\n\n\rHello, welcome to the PETRA Battery Characterization Lab!!\n\r");
  delay(1000);
  Serial.print(aboutProgram);
  delay(5000);
  Serial.print(programUsage);
  delay(5000);
}

/*
 * Arduino 'main' function.  
 * 
 * This is where you do your work that is repetitive
 */
void loop() {
  // get the system loop start time
  unsigned long StartTime = millis();

  // read the batter voltage
  batteryVoltage = analogRead(batteryVoltagePin);
  
  // calculate how long we have been running
  unsigned long CurrentTime = millis();
  unsigned long ElapsedTime = CurrentTime - StartTime;

  // report to console
  Serial.print("Time:\t");
  // minutes
  Serial.print(ElapsedTime / 1000);
  Serial.print(":");
  // seconds
  Serial.print((ElapsedTime / 1000) / 3600);
  //battery voltage
  Serial.print(",\tBattery =\t");
  Serial.print(scaleVoltage(batteryVoltage));

  // exit test at 30 mins, there are 1.8e+6 ms in 30 mins
  if (ElapsedTime >= 1.8e+6)
  {
    // kill ourselves
    exit(0);
  }
  // Adjust LED delays
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second
}
