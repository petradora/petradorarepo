Output: Gerber X2 Files
Type  : GerberX2
From  : Project [CombinedSystem_p2_b_2layer.PrjPcb]
   Generated File[CombinedSystem_p2_b_2layer._Copper_Signal_Top.gbr]
   Generated File[CombinedSystem_p2_b_2layer._Copper_Signal_Bot.gbr]
   Generated File[CombinedSystem_p2_b_2layer._Pads_Bot.gbr]
   Generated File[CombinedSystem_p2_b_2layer._Pads_Top.gbr]
   Generated File[CombinedSystem_p2_b_2layer._Legend_Bot.gbr]
   Generated File[CombinedSystem_p2_b_2layer._Soldermask_Bot.gbr]
   Generated File[CombinedSystem_p2_b_2layer._Paste_Bot.gbr]
   Generated File[CombinedSystem_p2_b_2layer._Legend_Top.gbr]
   Generated File[CombinedSystem_p2_b_2layer._Paste_Top.gbr]
   Generated File[CombinedSystem_p2_b_2layer._Soldermask_Top.gbr]
   Generated File[CombinedSystem_p2_b_2layer._Keep-out.gbr]
   Generated File[CombinedSystem_p2_b_2layer._Mechanical_13.gbr]
   Generated File[CombinedSystem_p2_b_2layer._Mechanical_20.gbr]
   Generated File[CombinedSystem_p2_b_2layer._Mechanical_2.gbr]
   Generated File[CombinedSystem_p2_b_2layer._Mechanical_14.gbr]
   Generated File[CombinedSystem_p2_b_2layer._Mechanical_5.gbr]
   Generated File[CombinedSystem_p2_b_2layer._Mechanical_19.gbr]
   Generated File[CombinedSystem_p2_b_2layer._Mechanical_1.gbr]
   Generated File[CombinedSystem_p2_b_2layer._Mechanical_3.gbr]
   Generated File[CombinedSystem_p2_b_2layer._Mechanical_7.gbr]
   Generated File[CombinedSystem_p2_b_2layer._Mechanical_15.gbr]
   Generated File[CombinedSystem_p2_b_2layer._Mechanical_24.gbr]
   Generated File[CombinedSystem_p2_b_2layer._Mechanical_4.gbr]
   Generated File[CombinedSystem_p2_b_2layer._Mechanical_21.gbr]
   Generated File[CombinedSystem_p2_b_2layer._Mechanical_16.gbr]
   Generated File[CombinedSystem_p2_b_2layer._Profile.gbr]
   Generated File[CombinedSystem_p2_b_2layer._NPTH_Drill.gbr]
   Generated File[CombinedSystem_p2_b_2layer._PTH_Drill.gbr]
   Generated File[CombinedSystem_p2_b_2layer._Drawing_1.gbr]
   Generated File[CombinedSystem_p2_b_2layer._Drillmap_1.gbr]
   Generated File[CombinedSystem_p2_b_2layer..RUL]
   Generated File[CombinedSystem_p2_b_2layer..EXTREP]
   Generated File[CombinedSystem_p2_b_2layer..REP]


Files Generated   : 33
Documents Printed : 0

Finished Output Generation At 4:19:19 PM On 10/22/2020
