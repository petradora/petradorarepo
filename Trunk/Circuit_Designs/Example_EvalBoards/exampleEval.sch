<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.5.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="yes"/>
<layer number="103" name="fp3" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="yes"/>
<layer number="105" name="Beschreib" color="9" fill="1" visible="no" active="yes"/>
<layer number="106" name="BGA-Top" color="4" fill="1" visible="no" active="yes"/>
<layer number="107" name="BD-Top" color="5" fill="1" visible="no" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="no" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="no" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="113" name="ReferenceLS" color="7" fill="1" visible="no" active="no"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="no" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="no" active="no"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="yes"/>
<layer number="250" name="Descript" color="7" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="7" fill="1" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="no" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="SamacSys_Parts">
<packages>
<package name="DFN1600X1600X750-13N">
<wire x1="-8" y1="-8" x2="8" y2="-8" width="0.127" layer="21"/>
<wire x1="8" y1="-8" x2="8" y2="8" width="0.127" layer="51"/>
<wire x1="8" y1="8" x2="-8" y2="8" width="0.127" layer="21"/>
<wire x1="-8" y1="8" x2="-8" y2="-8" width="0.127" layer="51"/>
<wire x1="-9.5" y1="8.25" x2="9.5" y2="8.25" width="0.127" layer="39"/>
<wire x1="9.5" y1="8.25" x2="9.5" y2="-8.25" width="0.127" layer="39"/>
<wire x1="9.5" y1="-8.25" x2="-9.5" y2="-8.25" width="0.127" layer="39"/>
<wire x1="-9.5" y1="-8.25" x2="-9.5" y2="8.25" width="0.127" layer="39"/>
<text x="-9.504090625" y="8.503659375" size="1.27055" layer="25">&gt;NAME</text>
<text x="-9.513659375" y="-9.76401875" size="1.27183125" layer="27">&gt;VALUE</text>
<smd name="1" x="-8" y="6.75" dx="2" dy="1" layer="1"/>
<smd name="2" x="-8" y="5.25" dx="2" dy="1" layer="1"/>
<smd name="3" x="-8" y="3.75" dx="2" dy="1" layer="1"/>
<smd name="4" x="-8" y="2.25" dx="2" dy="1" layer="1"/>
<smd name="5" x="-8" y="0.75" dx="2" dy="1" layer="1"/>
<smd name="6" x="-8" y="-0.75" dx="2" dy="1" layer="1"/>
<smd name="7" x="-8" y="-2.25" dx="2" dy="1" layer="1"/>
<smd name="8" x="-8" y="-3.75" dx="2" dy="1" layer="1"/>
<smd name="9" x="-8" y="-5.25" dx="2" dy="1" layer="1"/>
<smd name="10" x="-8" y="-6.75" dx="2" dy="1" layer="1"/>
<smd name="13" x="8" y="6.75" dx="2" dy="1" layer="1"/>
<smd name="12" x="8" y="0.75" dx="2" dy="1" layer="1"/>
<smd name="11" x="8" y="-6.75" dx="2" dy="1" layer="1"/>
<hole x="0.5" y="0.85" drill="3"/>
</package>
<package name="5046222012">
<description>&lt;b&gt;504622-2012-2&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-1.575" y="-0.907" dx="0.485" dy="0.18" layer="1" rot="R90"/>
<smd name="2" x="-1.575" y="0.907" dx="0.485" dy="0.18" layer="1" rot="R90"/>
<smd name="3" x="-1.225" y="-0.907" dx="0.485" dy="0.18" layer="1" rot="R90"/>
<smd name="4" x="-1.225" y="0.907" dx="0.485" dy="0.18" layer="1" rot="R90"/>
<smd name="5" x="-0.875" y="-0.907" dx="0.485" dy="0.18" layer="1" rot="R90"/>
<smd name="6" x="-0.875" y="0.907" dx="0.485" dy="0.18" layer="1" rot="R90"/>
<smd name="7" x="-0.525" y="-0.907" dx="0.485" dy="0.18" layer="1" rot="R90"/>
<smd name="8" x="-0.525" y="0.907" dx="0.485" dy="0.18" layer="1" rot="R90"/>
<smd name="9" x="-0.175" y="-0.907" dx="0.485" dy="0.18" layer="1" rot="R90"/>
<smd name="10" x="-0.175" y="0.907" dx="0.485" dy="0.18" layer="1" rot="R90"/>
<smd name="11" x="0.175" y="-0.907" dx="0.485" dy="0.18" layer="1" rot="R90"/>
<smd name="12" x="0.175" y="0.907" dx="0.485" dy="0.18" layer="1" rot="R90"/>
<smd name="13" x="0.525" y="-0.907" dx="0.485" dy="0.18" layer="1" rot="R90"/>
<smd name="14" x="0.525" y="0.907" dx="0.485" dy="0.18" layer="1" rot="R90"/>
<smd name="15" x="0.875" y="-0.907" dx="0.485" dy="0.18" layer="1" rot="R90"/>
<smd name="16" x="0.875" y="0.907" dx="0.485" dy="0.18" layer="1" rot="R90"/>
<smd name="17" x="1.225" y="-0.907" dx="0.485" dy="0.18" layer="1" rot="R90"/>
<smd name="18" x="1.225" y="0.907" dx="0.485" dy="0.18" layer="1" rot="R90"/>
<smd name="19" x="1.575" y="-0.907" dx="0.485" dy="0.18" layer="1" rot="R90"/>
<smd name="20" x="1.575" y="0.907" dx="0.485" dy="0.18" layer="1" rot="R90"/>
<smd name="MP1" x="-1.975" y="-0.907" dx="0.485" dy="0.28" layer="1" rot="R90"/>
<smd name="MP2" x="-1.975" y="0.907" dx="0.485" dy="0.28" layer="1" rot="R90"/>
<smd name="MP3" x="1.975" y="-0.907" dx="0.485" dy="0.28" layer="1" rot="R90"/>
<smd name="MP4" x="1.975" y="0.907" dx="0.485" dy="0.28" layer="1" rot="R90"/>
<text x="0" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="2.395" y1="-0.63" x2="-2.395" y2="-0.63" width="0.2" layer="51"/>
<wire x1="-2.395" y1="-0.63" x2="-2.395" y2="0.63" width="0.2" layer="51"/>
<wire x1="-2.395" y1="0.63" x2="2.395" y2="0.63" width="0.2" layer="51"/>
<wire x1="2.395" y1="0.63" x2="2.395" y2="-0.63" width="0.2" layer="51"/>
<wire x1="3.395" y1="2.149" x2="-3.395" y2="2.149" width="0.1" layer="51"/>
<wire x1="-3.395" y1="2.149" x2="-3.395" y2="-2.149" width="0.1" layer="51"/>
<wire x1="-3.395" y1="-2.149" x2="3.395" y2="-2.149" width="0.1" layer="51"/>
<wire x1="3.395" y1="-2.149" x2="3.395" y2="2.149" width="0.1" layer="51"/>
<wire x1="-2.395" y1="0.25" x2="-2.395" y2="0.25" width="0.1" layer="21"/>
<wire x1="-2.395" y1="0.25" x2="-2.395" y2="-0.25" width="0.1" layer="21"/>
<wire x1="-2.395" y1="-0.25" x2="-2.395" y2="-0.25" width="0.1" layer="21"/>
<wire x1="-2.395" y1="-0.25" x2="-2.395" y2="0.25" width="0.1" layer="21"/>
<wire x1="2.395" y1="0.25" x2="2.395" y2="0.25" width="0.1" layer="21"/>
<wire x1="2.395" y1="0.25" x2="2.395" y2="-0.25" width="0.1" layer="21"/>
<wire x1="2.395" y1="-0.25" x2="2.395" y2="-0.25" width="0.1" layer="21"/>
<wire x1="2.395" y1="-0.25" x2="2.395" y2="0.25" width="0.1" layer="21"/>
</package>
<package name="SOP65P490X110-8N">
<description>&lt;b&gt;SOP65P490X110-8N&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-2.2098" y="0.9652" dx="1.3716" dy="0.4572" layer="1"/>
<smd name="2" x="-2.2098" y="0.3302" dx="1.3716" dy="0.4572" layer="1"/>
<smd name="3" x="-2.2098" y="-0.3302" dx="1.3716" dy="0.4572" layer="1"/>
<smd name="4" x="-2.2098" y="-0.9652" dx="1.3716" dy="0.4572" layer="1"/>
<smd name="5" x="2.2098" y="-0.9652" dx="1.3716" dy="0.4572" layer="1"/>
<smd name="6" x="2.2098" y="-0.3302" dx="1.3716" dy="0.4572" layer="1"/>
<smd name="7" x="2.2098" y="0.3302" dx="1.3716" dy="0.4572" layer="1"/>
<smd name="8" x="2.2098" y="0.9652" dx="1.3716" dy="0.4572" layer="1"/>
<text x="-4.9022" y="1.8034" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="-5.715" y="-3.937" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-1.3716" y1="-1.4986" x2="1.3716" y2="-1.4986" width="0.1524" layer="21"/>
<wire x1="1.3716" y1="1.4986" x2="0.3048" y2="1.4986" width="0.1524" layer="21"/>
<wire x1="0.3048" y1="1.4986" x2="-0.3048" y2="1.4986" width="0.1524" layer="21"/>
<wire x1="-0.3048" y1="1.4986" x2="-1.3716" y2="1.4986" width="0.1524" layer="21"/>
<wire x1="-1.4986" y1="0.7874" x2="-1.4986" y2="1.1684" width="0.1524" layer="51"/>
<wire x1="-1.4986" y1="1.1684" x2="-2.5146" y2="1.1684" width="0.1524" layer="51"/>
<wire x1="-2.5146" y1="1.1684" x2="-2.5146" y2="0.7874" width="0.1524" layer="51"/>
<wire x1="-2.5146" y1="0.7874" x2="-1.4986" y2="0.7874" width="0.1524" layer="51"/>
<wire x1="-1.4986" y1="0.127" x2="-1.4986" y2="0.5334" width="0.1524" layer="51"/>
<wire x1="-1.4986" y1="0.5334" x2="-2.5146" y2="0.5334" width="0.1524" layer="51"/>
<wire x1="-2.5146" y1="0.5334" x2="-2.5146" y2="0.127" width="0.1524" layer="51"/>
<wire x1="-2.5146" y1="0.127" x2="-1.4986" y2="0.127" width="0.1524" layer="51"/>
<wire x1="-1.4986" y1="-0.5334" x2="-1.4986" y2="-0.127" width="0.1524" layer="51"/>
<wire x1="-1.4986" y1="-0.127" x2="-2.5146" y2="-0.127" width="0.1524" layer="51"/>
<wire x1="-2.5146" y1="-0.127" x2="-2.5146" y2="-0.5334" width="0.1524" layer="51"/>
<wire x1="-2.5146" y1="-0.5334" x2="-1.4986" y2="-0.5334" width="0.1524" layer="51"/>
<wire x1="-1.4986" y1="-1.1684" x2="-1.4986" y2="-0.7874" width="0.1524" layer="51"/>
<wire x1="-1.4986" y1="-0.7874" x2="-2.5146" y2="-0.7874" width="0.1524" layer="51"/>
<wire x1="-2.5146" y1="-0.7874" x2="-2.5146" y2="-1.1684" width="0.1524" layer="51"/>
<wire x1="-2.5146" y1="-1.1684" x2="-1.4986" y2="-1.1684" width="0.1524" layer="51"/>
<wire x1="1.4986" y1="-0.7874" x2="1.4986" y2="-1.1684" width="0.1524" layer="51"/>
<wire x1="1.4986" y1="-1.1684" x2="2.5146" y2="-1.1684" width="0.1524" layer="51"/>
<wire x1="2.5146" y1="-1.1684" x2="2.5146" y2="-0.7874" width="0.1524" layer="51"/>
<wire x1="2.5146" y1="-0.7874" x2="1.4986" y2="-0.7874" width="0.1524" layer="51"/>
<wire x1="1.4986" y1="-0.127" x2="1.4986" y2="-0.5334" width="0.1524" layer="51"/>
<wire x1="1.4986" y1="-0.5334" x2="2.5146" y2="-0.5334" width="0.1524" layer="51"/>
<wire x1="2.5146" y1="-0.5334" x2="2.5146" y2="-0.127" width="0.1524" layer="51"/>
<wire x1="2.5146" y1="-0.127" x2="1.4986" y2="-0.127" width="0.1524" layer="51"/>
<wire x1="1.4986" y1="0.5334" x2="1.4986" y2="0.127" width="0.1524" layer="51"/>
<wire x1="1.4986" y1="0.127" x2="2.5146" y2="0.127" width="0.1524" layer="51"/>
<wire x1="2.5146" y1="0.127" x2="2.5146" y2="0.5334" width="0.1524" layer="51"/>
<wire x1="2.5146" y1="0.5334" x2="1.4986" y2="0.5334" width="0.1524" layer="51"/>
<wire x1="1.4986" y1="1.1684" x2="1.4986" y2="0.7874" width="0.1524" layer="51"/>
<wire x1="1.4986" y1="0.7874" x2="2.5146" y2="0.7874" width="0.1524" layer="51"/>
<wire x1="2.5146" y1="0.7874" x2="2.5146" y2="1.1684" width="0.1524" layer="51"/>
<wire x1="2.5146" y1="1.1684" x2="1.4986" y2="1.1684" width="0.1524" layer="51"/>
<wire x1="-1.4986" y1="1.4986" x2="-1.4986" y2="-1.4986" width="0.1524" layer="51"/>
<wire x1="-1.4986" y1="-1.4986" x2="1.4986" y2="-1.4986" width="0.1524" layer="51"/>
<wire x1="1.4986" y1="-1.4986" x2="1.4986" y2="1.4986" width="0.1524" layer="51"/>
<wire x1="1.4986" y1="1.4986" x2="0.3048" y2="1.4986" width="0.1524" layer="51"/>
<wire x1="0.3048" y1="1.4986" x2="-0.3048" y2="1.4986" width="0.1524" layer="51"/>
<wire x1="-0.3048" y1="1.4986" x2="-1.4986" y2="1.4986" width="0.1524" layer="51"/>
</package>
<package name="ESB33536">
<description>&lt;b&gt;ESB-33536-3&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-5.5" y="2.5" dx="2.3" dy="1.6" layer="1"/>
<smd name="2" x="-5.5" y="0" dx="2.3" dy="1.6" layer="1"/>
<smd name="3" x="-5.5" y="-2.5" dx="2.3" dy="1.6" layer="1"/>
<smd name="4" x="5.5" y="-2.5" dx="2.3" dy="1.6" layer="1"/>
<smd name="5" x="5.5" y="0" dx="2.3" dy="1.6" layer="1"/>
<smd name="6" x="5.5" y="2.5" dx="2.3" dy="1.6" layer="1"/>
<hole x="0" y="-1" drill="1.7"/>
<text x="-0.85" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="-0.85" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-4.925" y1="-4.3" x2="4.925" y2="-4.3" width="0.2" layer="51"/>
<wire x1="4.925" y1="-4.3" x2="4.925" y2="4.3" width="0.2" layer="51"/>
<wire x1="4.925" y1="4.3" x2="-4.925" y2="4.3" width="0.2" layer="51"/>
<wire x1="-4.925" y1="4.3" x2="-4.925" y2="-4.3" width="0.2" layer="51"/>
<wire x1="-4.925" y1="4" x2="-4.925" y2="4" width="0.1" layer="21"/>
<wire x1="-4.925" y1="4" x2="-4.925" y2="4.25" width="0.1" layer="21"/>
<wire x1="-4.925" y1="4.25" x2="-4.925" y2="4.25" width="0.1" layer="21"/>
<wire x1="-4.925" y1="4.25" x2="-4.925" y2="4" width="0.1" layer="21"/>
<wire x1="-4.925" y1="4.3" x2="4.925" y2="4.3" width="0.1" layer="21"/>
<wire x1="4.925" y1="4.3" x2="4.925" y2="4.3" width="0.1" layer="21"/>
<wire x1="4.925" y1="4.3" x2="-4.925" y2="4.3" width="0.1" layer="21"/>
<wire x1="-4.925" y1="4.3" x2="-4.925" y2="4.3" width="0.1" layer="21"/>
<wire x1="4.925" y1="4.3" x2="4.925" y2="4.3" width="0.1" layer="21"/>
<wire x1="4.925" y1="4.3" x2="4.925" y2="3.75" width="0.1" layer="21"/>
<wire x1="4.925" y1="3.75" x2="4.925" y2="3.75" width="0.1" layer="21"/>
<wire x1="4.925" y1="3.75" x2="4.925" y2="4.3" width="0.1" layer="21"/>
<wire x1="4.925" y1="-4" x2="4.925" y2="-4" width="0.1" layer="21"/>
<wire x1="4.925" y1="-4" x2="4.925" y2="-4.3" width="0.1" layer="21"/>
<wire x1="4.925" y1="-4.3" x2="4.925" y2="-4.3" width="0.1" layer="21"/>
<wire x1="4.925" y1="-4.3" x2="4.925" y2="-4" width="0.1" layer="21"/>
<wire x1="4.925" y1="-4.3" x2="-4.925" y2="-4.3" width="0.1" layer="21"/>
<wire x1="-4.925" y1="-4.3" x2="-4.925" y2="-4.3" width="0.1" layer="21"/>
<wire x1="-4.925" y1="-4.3" x2="4.925" y2="-4.3" width="0.1" layer="21"/>
<wire x1="4.925" y1="-4.3" x2="4.925" y2="-4.3" width="0.1" layer="21"/>
<wire x1="-4.925" y1="-4.25" x2="-4.925" y2="-4.25" width="0.1" layer="21"/>
<wire x1="-4.925" y1="-4.25" x2="-4.925" y2="-4" width="0.1" layer="21"/>
<wire x1="-4.925" y1="-4" x2="-4.925" y2="-4" width="0.1" layer="21"/>
<wire x1="-4.925" y1="-4" x2="-4.925" y2="-4.25" width="0.1" layer="21"/>
<wire x1="-9.35" y1="5.3" x2="7.65" y2="5.3" width="0.1" layer="51"/>
<wire x1="7.65" y1="5.3" x2="7.65" y2="-5.3" width="0.1" layer="51"/>
<wire x1="7.65" y1="-5.3" x2="-9.35" y2="-5.3" width="0.1" layer="51"/>
<wire x1="-9.35" y1="-5.3" x2="-9.35" y2="5.3" width="0.1" layer="51"/>
<wire x1="-8.3" y1="2.5" x2="-8.3" y2="2.5" width="0.2" layer="21"/>
<wire x1="-8.3" y1="2.5" x2="-8.3" y2="2.4" width="0.2" layer="21" curve="180"/>
<wire x1="-8.3" y1="2.4" x2="-8.3" y2="2.4" width="0.2" layer="21"/>
<wire x1="-8.3" y1="2.4" x2="-8.3" y2="2.5" width="0.2" layer="21" curve="180"/>
<wire x1="-8.3" y1="2.5" x2="-8.3" y2="2.5" width="0.2" layer="21"/>
<wire x1="-8.3" y1="2.5" x2="-8.3" y2="2.4" width="0.2" layer="21" curve="180"/>
</package>
</packages>
<symbols>
<symbol name="SIM39EA">
<wire x1="12.7" y1="10.16" x2="12.7" y2="-10.16" width="0.254" layer="94"/>
<wire x1="12.7" y1="-10.16" x2="-12.7" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-10.16" x2="-12.7" y2="10.16" width="0.254" layer="94"/>
<wire x1="-12.7" y1="10.16" x2="12.7" y2="10.16" width="0.254" layer="94"/>
<text x="-12.7151" y="10.1721" size="2.543009375" layer="95">&gt;NAME</text>
<text x="-12.7084" y="-12.7084" size="2.541690625" layer="96">&gt;VALUE</text>
<pin name="VCC" x="17.78" y="7.62" length="middle" direction="pwr" rot="R180"/>
<pin name="V_BACKUP" x="-17.78" y="-2.54" length="middle"/>
<pin name="GND" x="17.78" y="-7.62" length="middle" direction="pas" rot="R180"/>
<pin name="TXD" x="17.78" y="2.54" length="middle" direction="out" rot="R180"/>
<pin name="RXD" x="-17.78" y="2.54" length="middle" direction="in"/>
<pin name="TIMEMARK" x="17.78" y="0" length="middle" direction="out" rot="R180"/>
<pin name="3D-FIX" x="17.78" y="-2.54" length="middle" direction="out" rot="R180"/>
</symbol>
<symbol name="504622-2012">
<wire x1="5.08" y1="2.54" x2="20.32" y2="2.54" width="0.254" layer="94"/>
<wire x1="20.32" y1="-30.48" x2="20.32" y2="2.54" width="0.254" layer="94"/>
<wire x1="20.32" y1="-30.48" x2="5.08" y2="-30.48" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-30.48" width="0.254" layer="94"/>
<text x="21.59" y="7.62" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="21.59" y="5.08" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="1" x="0" y="-2.54" length="middle"/>
<pin name="2" x="25.4" y="-2.54" length="middle" rot="R180"/>
<pin name="3" x="0" y="-5.08" length="middle"/>
<pin name="4" x="25.4" y="-5.08" length="middle" rot="R180"/>
<pin name="5" x="0" y="-7.62" length="middle"/>
<pin name="6" x="25.4" y="-7.62" length="middle" rot="R180"/>
<pin name="7" x="0" y="-10.16" length="middle"/>
<pin name="8" x="25.4" y="-10.16" length="middle" rot="R180"/>
<pin name="9" x="0" y="-12.7" length="middle"/>
<pin name="10" x="25.4" y="-12.7" length="middle" rot="R180"/>
<pin name="11" x="0" y="-15.24" length="middle"/>
<pin name="12" x="25.4" y="-15.24" length="middle" rot="R180"/>
<pin name="13" x="0" y="-17.78" length="middle"/>
<pin name="14" x="25.4" y="-17.78" length="middle" rot="R180"/>
<pin name="15" x="0" y="-20.32" length="middle"/>
<pin name="16" x="25.4" y="-20.32" length="middle" rot="R180"/>
<pin name="17" x="0" y="-22.86" length="middle"/>
<pin name="18" x="25.4" y="-22.86" length="middle" rot="R180"/>
<pin name="19" x="0" y="-25.4" length="middle"/>
<pin name="20" x="25.4" y="-25.4" length="middle" rot="R180"/>
<pin name="MP1" x="0" y="0" length="middle"/>
<pin name="MP2" x="25.4" y="0" length="middle" rot="R180"/>
<pin name="MP3" x="0" y="-27.94" length="middle"/>
<pin name="MP4" x="25.4" y="-27.94" length="middle" rot="R180"/>
</symbol>
<symbol name="TMP431ADGKR">
<wire x1="5.08" y1="2.54" x2="38.1" y2="2.54" width="0.254" layer="94"/>
<wire x1="38.1" y1="-10.16" x2="38.1" y2="2.54" width="0.254" layer="94"/>
<wire x1="38.1" y1="-10.16" x2="5.08" y2="-10.16" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-10.16" width="0.254" layer="94"/>
<text x="39.37" y="7.62" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="39.37" y="5.08" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="V+" x="0" y="0" length="middle"/>
<pin name="DXP" x="0" y="-2.54" length="middle"/>
<pin name="DXN" x="0" y="-5.08" length="middle"/>
<pin name="~THERM" x="0" y="-7.62" length="middle"/>
<pin name="GND" x="43.18" y="-7.62" length="middle" rot="R180"/>
<pin name="~ALERT/~THERM2" x="43.18" y="-5.08" length="middle" rot="R180"/>
<pin name="SDA" x="43.18" y="-2.54" length="middle" rot="R180"/>
<pin name="SCL" x="43.18" y="0" length="middle" rot="R180"/>
</symbol>
<symbol name="ESB-33536">
<wire x1="5.08" y1="2.54" x2="27.94" y2="2.54" width="0.254" layer="94"/>
<wire x1="27.94" y1="-7.62" x2="27.94" y2="2.54" width="0.254" layer="94"/>
<wire x1="27.94" y1="-7.62" x2="5.08" y2="-7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-7.62" width="0.254" layer="94"/>
<text x="29.21" y="7.62" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="29.21" y="5.08" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="N.O._1" x="0" y="0" length="middle"/>
<pin name="C_1" x="0" y="-2.54" length="middle"/>
<pin name="N.C._1" x="0" y="-5.08" length="middle"/>
<pin name="N.C._2" x="33.02" y="0" length="middle" rot="R180"/>
<pin name="C_2" x="33.02" y="-2.54" length="middle" rot="R180"/>
<pin name="N.O._2" x="33.02" y="-5.08" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SIM39EA" prefix="U">
<description>Ic Gps Accompany With Patch Antenna</description>
<gates>
<gate name="G$1" symbol="SIM39EA" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DFN1600X1600X750-13N">
<connects>
<connect gate="G$1" pin="3D-FIX" pad="5"/>
<connect gate="G$1" pin="GND" pad="3 8 11 12 13"/>
<connect gate="G$1" pin="RXD" pad="10"/>
<connect gate="G$1" pin="TIMEMARK" pad="6"/>
<connect gate="G$1" pin="TXD" pad="9"/>
<connect gate="G$1" pin="VCC" pad="1"/>
<connect gate="G$1" pin="V_BACKUP" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="AVAILABILITY" value="Unavailable"/>
<attribute name="DESCRIPTION" value=" Ic Gps Accompany With Patch Antenna "/>
<attribute name="MF" value="Simcom"/>
<attribute name="MP" value="SIM39EA"/>
<attribute name="PACKAGE" value="None"/>
<attribute name="PRICE" value="None"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="504622-2012" prefix="J">
<description>&lt;b&gt;SlimStack Board-to-Board Connector, 0.35mm Pitch, SSB6 Standard Series, Plug, 0.60mm Mated Height, 2.00mm Mated Width,  Circuits&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="https://componentsearchengine.com/Datasheets/1/504622-2012.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="504622-2012" x="0" y="0"/>
</gates>
<devices>
<device name="" package="5046222012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
<connect gate="G$1" pin="15" pad="15"/>
<connect gate="G$1" pin="16" pad="16"/>
<connect gate="G$1" pin="17" pad="17"/>
<connect gate="G$1" pin="18" pad="18"/>
<connect gate="G$1" pin="19" pad="19"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="20" pad="20"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
<connect gate="G$1" pin="MP1" pad="MP1"/>
<connect gate="G$1" pin="MP2" pad="MP2"/>
<connect gate="G$1" pin="MP3" pad="MP3"/>
<connect gate="G$1" pin="MP4" pad="MP4"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="SlimStack Board-to-Board Connector, 0.35mm Pitch, SSB6 Standard Series, Plug, 0.60mm Mated Height, 2.00mm Mated Width,  Circuits" constant="no"/>
<attribute name="HEIGHT" value="0.48mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Molex" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="504622-2012" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="538-504622-2012" constant="no"/>
<attribute name="MOUSER_PRICE-STOCK" value="https://www.mouser.com/Search/Refine.aspx?Keyword=538-504622-2012" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TMP431ADGKR" prefix="IC">
<description>&lt;b&gt;Remote and Local Temperature Sensor with Automatic Beta, N-Factor and Series-R Correction&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://www.ti.com/lit/gpn/TMP431"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="TMP431ADGKR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOP65P490X110-8N">
<connects>
<connect gate="G$1" pin="DXN" pad="3"/>
<connect gate="G$1" pin="DXP" pad="2"/>
<connect gate="G$1" pin="GND" pad="5"/>
<connect gate="G$1" pin="SCL" pad="8"/>
<connect gate="G$1" pin="SDA" pad="7"/>
<connect gate="G$1" pin="V+" pad="1"/>
<connect gate="G$1" pin="~ALERT/~THERM2" pad="6"/>
<connect gate="G$1" pin="~THERM" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="Remote and Local Temperature Sensor with Automatic Beta, N-Factor and Series-R Correction" constant="no"/>
<attribute name="HEIGHT" value="mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Texas Instruments" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="TMP431ADGKR" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="595-TMP431ADGKR" constant="no"/>
<attribute name="MOUSER_PRICE-STOCK" value="https://www.mouser.com/Search/Refine.aspx?Keyword=595-TMP431ADGKR" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ESB-33536" prefix="S">
<description>&lt;b&gt;Pushbutton Switches SWITCH PUSHBUTTON&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="https://componentsearchengine.com/Datasheets/1/ESB-33536.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="ESB-33536" x="0" y="0"/>
</gates>
<devices>
<device name="" package="ESB33536">
<connects>
<connect gate="G$1" pin="C_1" pad="2"/>
<connect gate="G$1" pin="C_2" pad="5"/>
<connect gate="G$1" pin="N.C._1" pad="3"/>
<connect gate="G$1" pin="N.C._2" pad="4"/>
<connect gate="G$1" pin="N.O._1" pad="1"/>
<connect gate="G$1" pin="N.O._2" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="Pushbutton Switches SWITCH PUSHBUTTON" constant="no"/>
<attribute name="HEIGHT" value="12.5mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Panasonic" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="ESB-33536" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="667-ESB-33536" constant="no"/>
<attribute name="MOUSER_PRICE-STOCK" value="https://www.mouser.com/Search/Refine.aspx?Keyword=667-ESB-33536" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="adafruit" urn="urn:adsk.eagle:library:420">
<packages>
<package name="1X01" urn="urn:adsk.eagle:footprint:6240008/1" library_version="2">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.9304" shape="octagon"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="1X01-CLEANBIG" urn="urn:adsk.eagle:footprint:6240197/1" library_version="2">
<pad name="1" x="0" y="0" drill="1.016" diameter="1.778"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="1X1-BIGPOGO" urn="urn:adsk.eagle:footprint:6240310/1" library_version="2">
<pad name="P$1" x="0" y="0" drill="1.4" diameter="2.54" shape="long"/>
</package>
</packages>
<packages3d>
<package3d name="1X01" urn="urn:adsk.eagle:package:6240654/1" type="box" library_version="2">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<packageinstances>
<packageinstance name="1X01"/>
</packageinstances>
</package3d>
<package3d name="1X01-CLEANBIG" urn="urn:adsk.eagle:package:6240841/1" type="box" library_version="2">
<packageinstances>
<packageinstance name="1X01-CLEANBIG"/>
</packageinstances>
</package3d>
<package3d name="1X1-BIGPOGO" urn="urn:adsk.eagle:package:6240953/1" type="box" library_version="2">
<packageinstances>
<packageinstance name="1X1-BIGPOGO"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="PINHD1" urn="urn:adsk.eagle:symbol:6239504/1" library_version="2">
<wire x1="-6.35" y1="-2.54" x2="1.27" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.27" y2="2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="-6.35" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="2.54" x2="-6.35" y2="-2.54" width="0.4064" layer="94"/>
<text x="-6.35" y="3.175" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PINHD-1X1" urn="urn:adsk.eagle:component:6240995/1" prefix="JP" uservalue="yes" library_version="2">
<description>&lt;b&gt;Pin header 1x1 for 0.1" spacing&lt;/b&gt;
&lt;p&gt;
With round pins</description>
<gates>
<gate name="G$1" symbol="PINHD1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X01">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:6240654/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CB" package="1X01-CLEANBIG">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:6240841/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-BIGPOGO" package="1X1-BIGPOGO">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:6240953/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-lstb" urn="urn:adsk.eagle:library:162">
<description>&lt;b&gt;Pin Headers&lt;/b&gt;&lt;p&gt;
Naming:&lt;p&gt;
MA = male&lt;p&gt;
# contacts - # rows&lt;p&gt;
W = angled&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="MA06-2" urn="urn:adsk.eagle:footprint:8289/1" library_version="2">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-6.985" y1="2.54" x2="-5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="2.54" x2="-5.08" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.905" x2="-4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="2.54" x2="-3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="2.54" x2="-2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="2.54" x2="-7.62" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="2.54" x2="-0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0" y2="1.905" width="0.1524" layer="21"/>
<wire x1="0.635" y1="2.54" x2="1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.54" x2="2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.905" x2="3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="2.54" x2="4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="2.54" x2="5.08" y2="1.905" width="0.1524" layer="21"/>
<wire x1="0.635" y1="2.54" x2="0" y2="1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.905" x2="5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="5.715" y1="2.54" x2="6.985" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.985" y1="2.54" x2="7.62" y2="1.905" width="0.1524" layer="21"/>
<wire x1="7.62" y1="1.905" x2="7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-2.54" x2="-4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-2.54" x2="-5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-1.905" x2="-6.985" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-2.54" x2="-6.985" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="-0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="-1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-2.54" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-2.54" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-2.54" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-2.54" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="6.985" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-2.54" x2="5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-2.54" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<pad name="1" x="-6.35" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="3" x="-3.81" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="5" x="-1.27" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="7" x="1.27" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="9" x="3.81" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="11" x="6.35" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="2" x="-6.35" y="1.27" drill="1.016" shape="octagon"/>
<pad name="4" x="-3.81" y="1.27" drill="1.016" shape="octagon"/>
<pad name="6" x="-1.27" y="1.27" drill="1.016" shape="octagon"/>
<pad name="8" x="1.27" y="1.27" drill="1.016" shape="octagon"/>
<pad name="10" x="3.81" y="1.27" drill="1.016" shape="octagon"/>
<pad name="12" x="6.35" y="1.27" drill="1.016" shape="octagon"/>
<text x="-6.858" y="-4.191" size="1.27" layer="21" ratio="10">1</text>
<text x="-7.62" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="5.08" y="2.921" size="1.27" layer="21" ratio="10">12</text>
<text x="-2.54" y="-4.191" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.064" y1="-1.524" x2="-3.556" y2="-1.016" layer="51"/>
<rectangle x1="-6.604" y1="-1.524" x2="-6.096" y2="-1.016" layer="51"/>
<rectangle x1="-1.524" y1="-1.524" x2="-1.016" y2="-1.016" layer="51"/>
<rectangle x1="3.556" y1="-1.524" x2="4.064" y2="-1.016" layer="51"/>
<rectangle x1="1.016" y1="-1.524" x2="1.524" y2="-1.016" layer="51"/>
<rectangle x1="6.096" y1="-1.524" x2="6.604" y2="-1.016" layer="51"/>
<rectangle x1="-6.604" y1="1.016" x2="-6.096" y2="1.524" layer="51"/>
<rectangle x1="-4.064" y1="1.016" x2="-3.556" y2="1.524" layer="51"/>
<rectangle x1="-1.524" y1="1.016" x2="-1.016" y2="1.524" layer="51"/>
<rectangle x1="1.016" y1="1.016" x2="1.524" y2="1.524" layer="51"/>
<rectangle x1="3.556" y1="1.016" x2="4.064" y2="1.524" layer="51"/>
<rectangle x1="6.096" y1="1.016" x2="6.604" y2="1.524" layer="51"/>
</package>
</packages>
<packages3d>
<package3d name="MA06-2" urn="urn:adsk.eagle:package:8342/1" type="box" library_version="2">
<description>PIN HEADER</description>
<packageinstances>
<packageinstance name="MA06-2"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="MA06-2" urn="urn:adsk.eagle:symbol:8288/1" library_version="2">
<wire x1="3.81" y1="-10.16" x2="-3.81" y2="-10.16" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-7.62" x2="2.54" y2="-7.62" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-1.27" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-1.27" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-7.62" x2="-1.27" y2="-7.62" width="0.6096" layer="94"/>
<wire x1="-3.81" y1="7.62" x2="-3.81" y2="-10.16" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-10.16" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-1.27" y2="5.08" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-1.27" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.6096" layer="94"/>
<text x="-3.81" y="-12.7" size="1.778" layer="96">&gt;VALUE</text>
<text x="-3.81" y="8.382" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="-7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="-7.62" y="-7.62" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="4" x="-7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="6" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="7" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="9" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="11" x="7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="8" x="-7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="10" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="12" x="-7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MA06-2" urn="urn:adsk.eagle:component:8383/2" prefix="SV" uservalue="yes" library_version="2">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="MA06-2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MA06-2">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="10" pad="10"/>
<connect gate="1" pin="11" pad="11"/>
<connect gate="1" pin="12" pad="12"/>
<connect gate="1" pin="2" pad="2"/>
<connect gate="1" pin="3" pad="3"/>
<connect gate="1" pin="4" pad="4"/>
<connect gate="1" pin="5" pad="5"/>
<connect gate="1" pin="6" pad="6"/>
<connect gate="1" pin="7" pad="7"/>
<connect gate="1" pin="8" pad="8"/>
<connect gate="1" pin="9" pad="9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8342/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
<attribute name="POPULARITY" value="8" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U1" library="SamacSys_Parts" deviceset="SIM39EA" device="" value="some_gps_thingy"/>
<part name="J1" library="SamacSys_Parts" deviceset="504622-2012" device=""/>
<part name="IC1" library="SamacSys_Parts" deviceset="TMP431ADGKR" device="" value="some_smart_temp_sensor"/>
<part name="S1" library="SamacSys_Parts" deviceset="ESB-33536" device="" value="some_dumb_push_button"/>
<part name="JP1" library="adafruit" library_urn="urn:adsk.eagle:library:420" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:6240654/1"/>
<part name="JP2" library="adafruit" library_urn="urn:adsk.eagle:library:420" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:6240654/1"/>
<part name="JP3" library="adafruit" library_urn="urn:adsk.eagle:library:420" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:6240654/1"/>
<part name="JP4" library="adafruit" library_urn="urn:adsk.eagle:library:420" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:6240654/1"/>
<part name="JP5" library="adafruit" library_urn="urn:adsk.eagle:library:420" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:6240654/1"/>
<part name="JP6" library="adafruit" library_urn="urn:adsk.eagle:library:420" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:6240654/1"/>
<part name="JP7" library="adafruit" library_urn="urn:adsk.eagle:library:420" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:6240654/1"/>
<part name="JP8" library="adafruit" library_urn="urn:adsk.eagle:library:420" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:6240654/1"/>
<part name="SV1" library="con-lstb" library_urn="urn:adsk.eagle:library:162" deviceset="MA06-2" device="" package3d_urn="urn:adsk.eagle:package:8342/1"/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="U1" gate="G$1" x="76.2" y="114.3" smashed="yes">
<attribute name="VALUE" x="63.4916" y="126.9916" size="2.541690625" layer="96"/>
</instance>
<instance part="J1" gate="G$1" x="63.5" y="162.56" smashed="yes">
<attribute name="NAME" x="74.93" y="167.64" size="1.778" layer="95" align="center-left"/>
</instance>
<instance part="IC1" gate="G$1" x="55.88" y="91.44" smashed="yes">
<attribute name="VALUE" x="64.77" y="96.52" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="S1" gate="G$1" x="60.96" y="73.66" smashed="yes">
<attribute name="VALUE" x="67.31" y="78.74" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="JP1" gate="G$1" x="149.86" y="162.56" smashed="yes">
<attribute name="NAME" x="143.51" y="165.735" size="1.778" layer="95"/>
<attribute name="VALUE" x="143.51" y="157.48" size="1.778" layer="96"/>
</instance>
<instance part="JP2" gate="G$1" x="149.86" y="152.4" smashed="yes">
<attribute name="NAME" x="143.51" y="155.575" size="1.778" layer="95"/>
<attribute name="VALUE" x="143.51" y="147.32" size="1.778" layer="96"/>
</instance>
<instance part="JP3" gate="G$1" x="149.86" y="142.24" smashed="yes">
<attribute name="NAME" x="143.51" y="145.415" size="1.778" layer="95"/>
<attribute name="VALUE" x="143.51" y="137.16" size="1.778" layer="96"/>
</instance>
<instance part="JP4" gate="G$1" x="149.86" y="132.08" smashed="yes">
<attribute name="NAME" x="143.51" y="135.255" size="1.778" layer="95"/>
<attribute name="VALUE" x="143.51" y="127" size="1.778" layer="96"/>
</instance>
<instance part="JP5" gate="G$1" x="149.86" y="121.92" smashed="yes">
<attribute name="NAME" x="143.51" y="125.095" size="1.778" layer="95"/>
<attribute name="VALUE" x="143.51" y="116.84" size="1.778" layer="96"/>
</instance>
<instance part="JP6" gate="G$1" x="149.86" y="111.76" smashed="yes">
<attribute name="NAME" x="143.51" y="114.935" size="1.778" layer="95"/>
<attribute name="VALUE" x="143.51" y="106.68" size="1.778" layer="96"/>
</instance>
<instance part="JP7" gate="G$1" x="149.86" y="101.6" smashed="yes">
<attribute name="NAME" x="143.51" y="104.775" size="1.778" layer="95"/>
<attribute name="VALUE" x="143.51" y="96.52" size="1.778" layer="96"/>
</instance>
<instance part="JP8" gate="G$1" x="149.86" y="91.44" smashed="yes">
<attribute name="NAME" x="143.51" y="94.615" size="1.778" layer="95"/>
<attribute name="VALUE" x="143.51" y="86.36" size="1.778" layer="96"/>
</instance>
<instance part="SV1" gate="1" x="7.62" y="116.84" smashed="yes">
<attribute name="VALUE" x="3.81" y="104.14" size="1.778" layer="96"/>
<attribute name="NAME" x="3.81" y="125.222" size="1.778" layer="95"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="VBAT" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="VCC"/>
<wire x1="93.98" y1="121.92" x2="104.14" y2="121.92" width="0.1524" layer="91"/>
<label x="96.52" y="121.92" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="2"/>
<wire x1="88.9" y1="160.02" x2="106.68" y2="160.02" width="0.1524" layer="91"/>
<label x="101.6" y="160.02" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="JP2" gate="G$1" pin="1"/>
<wire x1="147.32" y1="152.4" x2="121.92" y2="152.4" width="0.1524" layer="91"/>
<label x="121.92" y="152.4" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="SV1" gate="1" pin="2"/>
<wire x1="0" y1="109.22" x2="-17.78" y2="109.22" width="0.1524" layer="91"/>
<label x="-17.78" y="109.22" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="V+"/>
<wire x1="55.88" y1="91.44" x2="45.72" y2="91.44" width="0.1524" layer="91"/>
<label x="45.72" y="91.44" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="S1" gate="G$1" pin="C_1"/>
<wire x1="60.96" y1="71.12" x2="50.8" y2="71.12" width="0.1524" layer="91"/>
<label x="50.8" y="71.12" size="1.778" layer="95"/>
</segment>
</net>
<net name="MC_UART_TX" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="TXD"/>
<wire x1="93.98" y1="116.84" x2="104.14" y2="116.84" width="0.1524" layer="91"/>
<label x="96.52" y="116.84" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPS_3D-FIX" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="3D-FIX"/>
<wire x1="93.98" y1="111.76" x2="104.14" y2="111.76" width="0.1524" layer="91"/>
<label x="96.52" y="111.76" size="1.778" layer="95"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="GND"/>
<wire x1="93.98" y1="106.68" x2="104.14" y2="106.68" width="0.1524" layer="91"/>
<label x="96.52" y="106.68" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="V_BACKUP"/>
<wire x1="58.42" y1="111.76" x2="48.26" y2="111.76" width="0.1524" layer="91"/>
<label x="50.8" y="111.76" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="TIMEMARK"/>
<wire x1="93.98" y1="114.3" x2="104.14" y2="114.3" width="0.1524" layer="91"/>
<label x="96.52" y="114.3" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="63.5" y1="160.02" x2="45.72" y2="160.02" width="0.1524" layer="91"/>
<label x="45.72" y="160.02" size="1.778" layer="95"/>
<pinref part="J1" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="SV1" gate="1" pin="1"/>
<wire x1="15.24" y1="109.22" x2="30.48" y2="109.22" width="0.1524" layer="91"/>
<label x="25.4" y="109.22" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="JP1" gate="G$1" pin="1"/>
<wire x1="147.32" y1="162.56" x2="121.92" y2="162.56" width="0.1524" layer="91"/>
<label x="121.92" y="162.56" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="GND"/>
<wire x1="99.06" y1="83.82" x2="109.22" y2="83.82" width="0.1524" layer="91"/>
<label x="104.14" y="83.82" size="1.778" layer="95"/>
</segment>
</net>
<net name="UART_MC_RX" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="RXD"/>
<wire x1="58.42" y1="116.84" x2="48.26" y2="116.84" width="0.1524" layer="91"/>
<label x="40.64" y="116.84" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="4"/>
<wire x1="88.9" y1="157.48" x2="106.68" y2="157.48" width="0.1524" layer="91"/>
<label x="91.44" y="157.48" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="SV1" gate="1" pin="7"/>
<wire x1="15.24" y1="116.84" x2="30.48" y2="116.84" width="0.1524" layer="91"/>
<label x="17.78" y="116.84" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="JP7" gate="G$1" pin="1"/>
<wire x1="147.32" y1="101.6" x2="121.92" y2="101.6" width="0.1524" layer="91"/>
<label x="121.92" y="101.6" size="1.778" layer="95"/>
</segment>
</net>
<net name="SPI_MC_MISO" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="16"/>
<wire x1="88.9" y1="142.24" x2="106.68" y2="142.24" width="0.1524" layer="91"/>
<label x="91.44" y="142.24" size="1.778" layer="95"/>
</segment>
</net>
<net name="SPI_MC_SCLK" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="14"/>
<wire x1="88.9" y1="144.78" x2="106.68" y2="144.78" width="0.1524" layer="91"/>
<label x="91.44" y="144.78" size="1.778" layer="95"/>
</segment>
</net>
<net name="SPI_MC_MOSI" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="18"/>
<wire x1="88.9" y1="139.7" x2="106.68" y2="139.7" width="0.1524" layer="91"/>
<label x="91.44" y="139.7" size="1.778" layer="95"/>
</segment>
</net>
<net name="I2C_MC_SCL" class="0">
<segment>
<wire x1="63.5" y1="149.86" x2="45.72" y2="149.86" width="0.1524" layer="91"/>
<label x="45.72" y="149.86" size="1.778" layer="95"/>
<pinref part="J1" gate="G$1" pin="9"/>
</segment>
<segment>
<pinref part="JP3" gate="G$1" pin="1"/>
<wire x1="147.32" y1="142.24" x2="121.92" y2="142.24" width="0.1524" layer="91"/>
<label x="121.92" y="142.24" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="SV1" gate="1" pin="3"/>
<wire x1="15.24" y1="111.76" x2="30.48" y2="111.76" width="0.1524" layer="91"/>
<label x="17.78" y="111.76" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="SCL"/>
<wire x1="99.06" y1="91.44" x2="109.22" y2="91.44" width="0.1524" layer="91"/>
<label x="101.6" y="91.44" size="1.778" layer="95"/>
</segment>
</net>
<net name="I2C_MC_SDA" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="10"/>
<wire x1="88.9" y1="149.86" x2="106.68" y2="149.86" width="0.1524" layer="91"/>
<label x="91.44" y="149.86" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="JP4" gate="G$1" pin="1"/>
<wire x1="147.32" y1="132.08" x2="121.92" y2="132.08" width="0.1524" layer="91"/>
<label x="121.92" y="132.08" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="SV1" gate="1" pin="4"/>
<wire x1="0" y1="111.76" x2="-17.78" y2="111.76" width="0.1524" layer="91"/>
<label x="-17.78" y="111.76" size="1.778" layer="95"/>
</segment>
</net>
<net name="UART_MC_TX" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="6"/>
<wire x1="88.9" y1="154.94" x2="106.68" y2="154.94" width="0.1524" layer="91"/>
<label x="91.44" y="154.94" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="JP6" gate="G$1" pin="1"/>
<wire x1="147.32" y1="111.76" x2="121.92" y2="111.76" width="0.1524" layer="91"/>
<label x="121.92" y="111.76" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="SV1" gate="1" pin="6"/>
<wire x1="0" y1="114.3" x2="-17.78" y2="114.3" width="0.1524" layer="91"/>
<label x="-17.78" y="114.3" size="1.778" layer="95"/>
</segment>
</net>
<net name="MC_CLK" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="12"/>
<wire x1="88.9" y1="147.32" x2="106.68" y2="147.32" width="0.1524" layer="91"/>
<label x="96.52" y="147.32" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="SV1" gate="1" pin="5"/>
<wire x1="15.24" y1="114.3" x2="30.48" y2="114.3" width="0.1524" layer="91"/>
<label x="22.86" y="114.3" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="JP5" gate="G$1" pin="1"/>
<wire x1="147.32" y1="121.92" x2="121.92" y2="121.92" width="0.1524" layer="91"/>
<label x="121.92" y="121.92" size="1.778" layer="95"/>
</segment>
</net>
<net name="SNS_1_X" class="0">
<segment>
<wire x1="63.5" y1="157.48" x2="45.72" y2="157.48" width="0.1524" layer="91"/>
<label x="45.72" y="157.48" size="1.778" layer="95"/>
<pinref part="J1" gate="G$1" pin="3"/>
</segment>
</net>
<net name="SNS_1_Y" class="0">
<segment>
<wire x1="63.5" y1="154.94" x2="45.72" y2="154.94" width="0.1524" layer="91"/>
<label x="45.72" y="154.94" size="1.778" layer="95"/>
<pinref part="J1" gate="G$1" pin="5"/>
</segment>
</net>
<net name="SNS_1_Z" class="0">
<segment>
<wire x1="63.5" y1="152.4" x2="45.72" y2="152.4" width="0.1524" layer="91"/>
<label x="45.72" y="152.4" size="1.778" layer="95"/>
<pinref part="J1" gate="G$1" pin="7"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<wire x1="63.5" y1="147.32" x2="45.72" y2="147.32" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="11"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<wire x1="63.5" y1="144.78" x2="45.72" y2="144.78" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="13"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<wire x1="63.5" y1="142.24" x2="45.72" y2="142.24" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="15"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<wire x1="63.5" y1="139.7" x2="45.72" y2="139.7" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="17"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<wire x1="63.5" y1="137.16" x2="45.72" y2="137.16" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="19"/>
</segment>
</net>
<net name="GYRO_12C_LSB" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="8"/>
<wire x1="88.9" y1="152.4" x2="106.68" y2="152.4" width="0.1524" layer="91"/>
<label x="91.44" y="152.4" size="1.778" layer="95"/>
</segment>
</net>
<net name="SPI_MC_SS" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="20"/>
<wire x1="88.9" y1="137.16" x2="106.68" y2="137.16" width="0.1524" layer="91"/>
<label x="93.98" y="137.16" size="1.778" layer="95"/>
</segment>
</net>
<net name="FUCK" class="0">
<segment>
<pinref part="S1" gate="G$1" pin="C_2"/>
<wire x1="93.98" y1="71.12" x2="101.6" y2="71.12" width="0.1524" layer="91"/>
<label x="96.52" y="71.12" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="JP8" gate="G$1" pin="1"/>
<wire x1="147.32" y1="91.44" x2="121.92" y2="91.44" width="0.1524" layer="91"/>
<label x="124.46" y="91.44" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="SV1" gate="1" pin="8"/>
<wire x1="0" y1="116.84" x2="-17.78" y2="116.84" width="0.1524" layer="91"/>
<label x="-17.78" y="116.84" size="1.778" layer="95"/>
</segment>
</net>
<net name="I2C_MC_SCA" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="SDA"/>
<wire x1="109.22" y1="88.9" x2="99.06" y2="88.9" width="0.1524" layer="91"/>
<label x="101.6" y="88.9" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="SV1" gate="1" pin="9"/>
<wire x1="15.24" y1="119.38" x2="30.48" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="SV1" gate="1" pin="10"/>
<wire x1="0" y1="119.38" x2="-17.78" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
