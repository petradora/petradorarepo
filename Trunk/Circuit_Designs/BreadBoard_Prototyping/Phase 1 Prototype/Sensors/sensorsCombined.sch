<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.6.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="yes" active="yes"/>
<layer number="101" name="Patch_Top" color="7" fill="1" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="fp3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="Beschreib" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="BGA-Top" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="BD-Top" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="yes" active="yes"/>
<layer number="114" name="GND" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="7" fill="1" visible="yes" active="yes"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="yes" active="yes"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="134" name="silk_top" color="7" fill="1" visible="yes" active="yes"/>
<layer number="135" name="silk_bottom" color="7" fill="1" visible="yes" active="yes"/>
<layer number="136" name="silktop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="137" name="silkbottom" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="145" name="DrillLegend_01-16" color="7" fill="1" visible="yes" active="yes"/>
<layer number="146" name="DrillLegend_01-20" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="166" name="AntennaArea" color="7" fill="1" visible="yes" active="yes"/>
<layer number="168" name="4mmHeightArea" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="218" name="218bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="219" name="219bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="220" name="220bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="221" name="221bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="222" name="222bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="223" name="223bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="224" name="224bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="7" fill="1" visible="yes" active="yes"/>
<layer number="251" name="SMDround" color="7" fill="1" visible="yes" active="yes"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="SamacSys_Parts" urn="urn:adsk.eagle:library:19142403">
<packages>
<package name="SOIC127P600X175-8N" urn="urn:adsk.eagle:footprint:19142484/1" library_version="7">
<description>&lt;b&gt;D (R-PDSO-G8)&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-2.712" y="1.905" dx="1.525" dy="0.65" layer="1"/>
<smd name="2" x="-2.712" y="0.635" dx="1.525" dy="0.65" layer="1"/>
<smd name="3" x="-2.712" y="-0.635" dx="1.525" dy="0.65" layer="1"/>
<smd name="4" x="-2.712" y="-1.905" dx="1.525" dy="0.65" layer="1"/>
<smd name="5" x="2.712" y="-1.905" dx="1.525" dy="0.65" layer="1"/>
<smd name="6" x="2.712" y="-0.635" dx="1.525" dy="0.65" layer="1"/>
<smd name="7" x="2.712" y="0.635" dx="1.525" dy="0.65" layer="1"/>
<smd name="8" x="2.712" y="1.905" dx="1.525" dy="0.65" layer="1"/>
<text x="0" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-3.725" y1="2.75" x2="3.725" y2="2.75" width="0.05" layer="51"/>
<wire x1="3.725" y1="2.75" x2="3.725" y2="-2.75" width="0.05" layer="51"/>
<wire x1="3.725" y1="-2.75" x2="-3.725" y2="-2.75" width="0.05" layer="51"/>
<wire x1="-3.725" y1="-2.75" x2="-3.725" y2="2.75" width="0.05" layer="51"/>
<wire x1="-1.95" y1="2.45" x2="1.95" y2="2.45" width="0.1" layer="51"/>
<wire x1="1.95" y1="2.45" x2="1.95" y2="-2.45" width="0.1" layer="51"/>
<wire x1="1.95" y1="-2.45" x2="-1.95" y2="-2.45" width="0.1" layer="51"/>
<wire x1="-1.95" y1="-2.45" x2="-1.95" y2="2.45" width="0.1" layer="51"/>
<wire x1="-1.95" y1="1.18" x2="-0.68" y2="2.45" width="0.1" layer="51"/>
<wire x1="-1.6" y1="2.45" x2="1.6" y2="2.45" width="0.2" layer="21"/>
<wire x1="1.6" y1="2.45" x2="1.6" y2="-2.45" width="0.2" layer="21"/>
<wire x1="1.6" y1="-2.45" x2="-1.6" y2="-2.45" width="0.2" layer="21"/>
<wire x1="-1.6" y1="-2.45" x2="-1.6" y2="2.45" width="0.2" layer="21"/>
<wire x1="-3.475" y1="2.58" x2="-1.95" y2="2.58" width="0.2" layer="21"/>
</package>
<package name="SOP100P1210X475-24N" library_version="7">
<description>&lt;b&gt;SOIC-24&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-5.425" y="5.5" dx="2.2" dy="0.75" layer="1"/>
<smd name="2" x="-5.425" y="4.5" dx="2.2" dy="0.75" layer="1"/>
<smd name="3" x="-5.425" y="3.5" dx="2.2" dy="0.75" layer="1"/>
<smd name="4" x="-5.425" y="2.5" dx="2.2" dy="0.75" layer="1"/>
<smd name="5" x="-5.425" y="1.5" dx="2.2" dy="0.75" layer="1"/>
<smd name="6" x="-5.425" y="0.5" dx="2.2" dy="0.75" layer="1"/>
<smd name="7" x="-5.425" y="-0.5" dx="2.2" dy="0.75" layer="1"/>
<smd name="8" x="-5.425" y="-1.5" dx="2.2" dy="0.75" layer="1"/>
<smd name="9" x="-5.425" y="-2.5" dx="2.2" dy="0.75" layer="1"/>
<smd name="10" x="-5.425" y="-3.5" dx="2.2" dy="0.75" layer="1"/>
<smd name="11" x="-5.425" y="-4.5" dx="2.2" dy="0.75" layer="1"/>
<smd name="12" x="-5.425" y="-5.5" dx="2.2" dy="0.75" layer="1"/>
<smd name="13" x="5.425" y="-5.5" dx="2.2" dy="0.75" layer="1"/>
<smd name="14" x="5.425" y="-4.5" dx="2.2" dy="0.75" layer="1"/>
<smd name="15" x="5.425" y="-3.5" dx="2.2" dy="0.75" layer="1"/>
<smd name="16" x="5.425" y="-2.5" dx="2.2" dy="0.75" layer="1"/>
<smd name="17" x="5.425" y="-1.5" dx="2.2" dy="0.75" layer="1"/>
<smd name="18" x="5.425" y="-0.5" dx="2.2" dy="0.75" layer="1"/>
<smd name="19" x="5.425" y="0.5" dx="2.2" dy="0.75" layer="1"/>
<smd name="20" x="5.425" y="1.5" dx="2.2" dy="0.75" layer="1"/>
<smd name="21" x="5.425" y="2.5" dx="2.2" dy="0.75" layer="1"/>
<smd name="22" x="5.425" y="3.5" dx="2.2" dy="0.75" layer="1"/>
<smd name="23" x="5.425" y="4.5" dx="2.2" dy="0.75" layer="1"/>
<smd name="24" x="5.425" y="5.5" dx="2.2" dy="0.75" layer="1"/>
<text x="0" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-6.775" y1="7.8" x2="6.775" y2="7.8" width="0.05" layer="51"/>
<wire x1="6.775" y1="7.8" x2="6.775" y2="-7.8" width="0.05" layer="51"/>
<wire x1="6.775" y1="-7.8" x2="-6.775" y2="-7.8" width="0.05" layer="51"/>
<wire x1="-6.775" y1="-7.8" x2="-6.775" y2="7.8" width="0.05" layer="51"/>
<wire x1="-3.55" y1="7.5" x2="3.55" y2="7.5" width="0.1" layer="51"/>
<wire x1="3.55" y1="7.5" x2="3.55" y2="-7.5" width="0.1" layer="51"/>
<wire x1="3.55" y1="-7.5" x2="-3.55" y2="-7.5" width="0.1" layer="51"/>
<wire x1="-3.55" y1="-7.5" x2="-3.55" y2="7.5" width="0.1" layer="51"/>
<wire x1="-3.55" y1="6.5" x2="-2.55" y2="7.5" width="0.1" layer="51"/>
<wire x1="-3.975" y1="7.5" x2="3.975" y2="7.5" width="0.2" layer="21"/>
<wire x1="3.975" y1="7.5" x2="3.975" y2="-7.5" width="0.2" layer="21"/>
<wire x1="3.975" y1="-7.5" x2="-3.975" y2="-7.5" width="0.2" layer="21"/>
<wire x1="-3.975" y1="-7.5" x2="-3.975" y2="7.5" width="0.2" layer="21"/>
<wire x1="-6.525" y1="6.225" x2="-4.325" y2="6.225" width="0.2" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="AIS1120SXTR" library_version="7">
<wire x1="5.08" y1="2.54" x2="22.86" y2="2.54" width="0.254" layer="94"/>
<wire x1="22.86" y1="-10.16" x2="22.86" y2="2.54" width="0.254" layer="94"/>
<wire x1="22.86" y1="-10.16" x2="5.08" y2="-10.16" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-10.16" width="0.254" layer="94"/>
<text x="24.13" y="7.62" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="24.13" y="5.08" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="SCL" x="0" y="0" length="middle"/>
<pin name="SDI" x="0" y="-2.54" length="middle"/>
<pin name="SDO" x="0" y="-5.08" length="middle"/>
<pin name="CS" x="0" y="-7.62" length="middle"/>
<pin name="MP" x="27.94" y="0" length="middle" rot="R180"/>
<pin name="VDD" x="27.94" y="-2.54" length="middle" rot="R180"/>
<pin name="VREG" x="27.94" y="-5.08" length="middle" rot="R180"/>
<pin name="GND" x="27.94" y="-7.62" length="middle" rot="R180"/>
</symbol>
<symbol name="SCR2100-D08-05" library_version="7">
<wire x1="5.08" y1="2.54" x2="38.1" y2="2.54" width="0.254" layer="94"/>
<wire x1="38.1" y1="-30.48" x2="38.1" y2="2.54" width="0.254" layer="94"/>
<wire x1="38.1" y1="-30.48" x2="5.08" y2="-30.48" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-30.48" width="0.254" layer="94"/>
<text x="39.37" y="7.62" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="39.37" y="5.08" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="HEAT_1" x="0" y="0" length="middle"/>
<pin name="RESERVED_1" x="0" y="-2.54" length="middle"/>
<pin name="EXTRESN" x="0" y="-5.08" length="middle"/>
<pin name="SCK" x="0" y="-7.62" length="middle"/>
<pin name="MISO" x="0" y="-10.16" length="middle"/>
<pin name="VBOOST" x="0" y="-12.7" length="middle"/>
<pin name="LBOOST" x="0" y="-15.24" length="middle"/>
<pin name="DVSS" x="0" y="-17.78" length="middle"/>
<pin name="DVDD" x="0" y="-20.32" length="middle"/>
<pin name="D_EXTC" x="0" y="-22.86" length="middle"/>
<pin name="RESERVED_2" x="0" y="-25.4" length="middle"/>
<pin name="HEAT_2" x="0" y="-27.94" length="middle"/>
<pin name="HEAT_4" x="43.18" y="0" length="middle" rot="R180"/>
<pin name="RESERVED_6" x="43.18" y="-2.54" length="middle" rot="R180"/>
<pin name="RESERVED_5" x="43.18" y="-5.08" length="middle" rot="R180"/>
<pin name="RESERVED_4" x="43.18" y="-7.62" length="middle" rot="R180"/>
<pin name="RESERVED_3" x="43.18" y="-10.16" length="middle" rot="R180"/>
<pin name="MOSI" x="43.18" y="-12.7" length="middle" rot="R180"/>
<pin name="CSB" x="43.18" y="-15.24" length="middle" rot="R180"/>
<pin name="AVSS" x="43.18" y="-17.78" length="middle" rot="R180"/>
<pin name="AVSS_REF" x="43.18" y="-20.32" length="middle" rot="R180"/>
<pin name="A_EXTC" x="43.18" y="-22.86" length="middle" rot="R180"/>
<pin name="AVDD" x="43.18" y="-25.4" length="middle" rot="R180"/>
<pin name="HEAT_3" x="43.18" y="-27.94" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="AIS1120SXTR" prefix="IC" library_version="7">
<description>&lt;b&gt;Accelerometers MEMS acceleration sensor: single-axis for central airbag applications&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="https://www.st.com/resource/en/datasheet/ais1120sx.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="AIS1120SXTR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOIC127P600X175-8N">
<connects>
<connect gate="G$1" pin="CS" pad="4"/>
<connect gate="G$1" pin="GND" pad="5"/>
<connect gate="G$1" pin="MP" pad="8"/>
<connect gate="G$1" pin="SCL" pad="1"/>
<connect gate="G$1" pin="SDI" pad="2"/>
<connect gate="G$1" pin="SDO" pad="3"/>
<connect gate="G$1" pin="VDD" pad="7"/>
<connect gate="G$1" pin="VREG" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="Accelerometers MEMS acceleration sensor: single-axis for central airbag applications" constant="no"/>
<attribute name="HEIGHT" value="1.75mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="STMicroelectronics" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="AIS1120SXTR" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="511-AIS1120SXTR" constant="no"/>
<attribute name="MOUSER_PRICE-STOCK" value="https://www.mouser.co.uk/ProductDetail/STMicroelectronics/AIS1120SXTR?qs=BA62vJVifGoAj%2Fv2Aa%252BKHA%3D%3D" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SCR2100-D08-05" prefix="IC" library_version="7">
<description>&lt;b&gt;Murata SCR2100-D08-05, Accelerometer &amp; Gyroscope, +/-125/s, 3  3.6 V, SPI, SMD 24-Pin&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="https://www.murata.com/~/media/webrenewal/products/sensor/gyro/scc2000/scr2100-d08 datasheet 82177700b0.ashx?la=en"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="SCR2100-D08-05" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOP100P1210X475-24N">
<connects>
<connect gate="G$1" pin="AVDD" pad="14"/>
<connect gate="G$1" pin="AVSS" pad="17"/>
<connect gate="G$1" pin="AVSS_REF" pad="16"/>
<connect gate="G$1" pin="A_EXTC" pad="15"/>
<connect gate="G$1" pin="CSB" pad="18"/>
<connect gate="G$1" pin="DVDD" pad="9"/>
<connect gate="G$1" pin="DVSS" pad="8"/>
<connect gate="G$1" pin="D_EXTC" pad="10"/>
<connect gate="G$1" pin="EXTRESN" pad="3"/>
<connect gate="G$1" pin="HEAT_1" pad="1"/>
<connect gate="G$1" pin="HEAT_2" pad="12"/>
<connect gate="G$1" pin="HEAT_3" pad="13"/>
<connect gate="G$1" pin="HEAT_4" pad="24"/>
<connect gate="G$1" pin="LBOOST" pad="7"/>
<connect gate="G$1" pin="MISO" pad="5"/>
<connect gate="G$1" pin="MOSI" pad="19"/>
<connect gate="G$1" pin="RESERVED_1" pad="2"/>
<connect gate="G$1" pin="RESERVED_2" pad="11"/>
<connect gate="G$1" pin="RESERVED_3" pad="20"/>
<connect gate="G$1" pin="RESERVED_4" pad="21"/>
<connect gate="G$1" pin="RESERVED_5" pad="22"/>
<connect gate="G$1" pin="RESERVED_6" pad="23"/>
<connect gate="G$1" pin="SCK" pad="4"/>
<connect gate="G$1" pin="VBOOST" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="Murata SCR2100-D08-05, Accelerometer &amp; Gyroscope, +/-125/s, 3  3.6 V, SPI, SMD 24-Pin" constant="no"/>
<attribute name="HEIGHT" value="4.75mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Murata Electronics" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="SCR2100-D08-05" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="81-SCR2100-D08-05" constant="no"/>
<attribute name="MOUSER_PRICE-STOCK" value="https://www.mouser.co.uk/ProductDetail/Murata-Electronics/SCR2100-D08-05?qs=u1o5GQxYRwqwX72FamU5oQ%3D%3D" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="DE6E3KJ472MB3B">
<packages>
<package name="CAP_DE6E3KJ472MB3B">
<wire x1="6" y1="-3.5" x2="-6" y2="-3.5" width="0.127" layer="21"/>
<wire x1="-6" y1="-3.5" x2="-6" y2="3.5" width="0.127" layer="51"/>
<wire x1="-6" y1="3.5" x2="6" y2="3.5" width="0.127" layer="51"/>
<wire x1="6" y1="3.5" x2="6" y2="-3.5" width="0.127" layer="51"/>
<wire x1="6" y1="-3.5" x2="-6" y2="-3.5" width="0.127" layer="51"/>
<wire x1="-6.25" y1="-3.75" x2="-6.25" y2="3.75" width="0.05" layer="39"/>
<wire x1="-6.25" y1="3.75" x2="6.25" y2="3.75" width="0.05" layer="39"/>
<wire x1="6.25" y1="3.75" x2="6.25" y2="-3.75" width="0.05" layer="39"/>
<wire x1="6.25" y1="-3.75" x2="-6.25" y2="-3.75" width="0.05" layer="39"/>
<text x="-6.25" y="3.8" size="1.27" layer="25">&gt;NAME</text>
<text x="-6.25" y="-5.15" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="6" y1="3.5" x2="-6" y2="3.5" width="0.127" layer="21"/>
<wire x1="-6" y1="3.5" x2="-6" y2="-3.5" width="0.127" layer="21"/>
<wire x1="6" y1="3.5" x2="6" y2="-3.5" width="0.127" layer="21"/>
<pad name="1" x="-3.75" y="0" drill="0.85" diameter="1.28"/>
<pad name="2" x="3.75" y="0" drill="0.85" diameter="1.28"/>
</package>
</packages>
<symbols>
<symbol name="DE6E3KJ472MB3B">
<text x="0" y="3.81093125" size="1.77843125" layer="95">&gt;NAME</text>
<text x="0" y="-5.08848125" size="1.78096875" layer="96">&gt;VALUE</text>
<rectangle x1="0" y1="-1.906859375" x2="0.635" y2="1.905" layer="94"/>
<rectangle x1="1.90685" y1="-1.90685" x2="2.54" y2="1.905" layer="94"/>
<pin name="2" x="5.08" y="0" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="1" x="-2.54" y="0" visible="pad" length="short" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="DE6E3KJ472MB3B" prefix="C">
<description>CAP CER 4700PF 300VAC RADIAL &lt;a href="https://snapeda.com/parts/DE6E3KJ472MB3B/Murata/view-part/?ref=eda"&gt;Check prices&lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="DE6E3KJ472MB3B" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CAP_DE6E3KJ472MB3B">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="AVAILABILITY" value="Warning"/>
<attribute name="DESCRIPTION" value=" Lead capacitor E(JIS) with Capacitance 4700pF, Rated Voltage 300Vac(r.m.s.) "/>
<attribute name="MF" value="Murata"/>
<attribute name="MOUSER-PURCHASE-URL" value="https://snapeda.com/shop?store=Mouser&amp;id=1664984"/>
<attribute name="MP" value="DE6E3KJ472MB3B"/>
<attribute name="PACKAGE" value="NONSTANDARD "/>
<attribute name="PRICE" value="None"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="TO229P994X240-3N">
<packages>
<package name="TO229P994X240-3N">
<rectangle x1="1.665" y1="-0.655" x2="3.195" y2="0.655" layer="31"/>
<rectangle x1="-0.445" y1="-0.655" x2="1.085" y2="0.655" layer="31"/>
<rectangle x1="3.775" y1="-0.655" x2="5.305" y2="0.655" layer="31"/>
<rectangle x1="-0.445" y1="1.235" x2="1.085" y2="2.545" layer="31"/>
<rectangle x1="1.665" y1="1.235" x2="3.195" y2="2.545" layer="31"/>
<rectangle x1="3.775" y1="1.235" x2="5.305" y2="2.545" layer="31"/>
<rectangle x1="-0.445" y1="-2.545" x2="1.085" y2="-1.235" layer="31"/>
<rectangle x1="1.665" y1="-2.545" x2="3.195" y2="-1.235" layer="31"/>
<rectangle x1="3.775" y1="-2.545" x2="5.305" y2="-1.235" layer="31"/>
<wire x1="4.055" y1="3.2825" x2="4.055" y2="-3.2825" width="0.127" layer="51"/>
<wire x1="4.055" y1="-3.2825" x2="-2.045" y2="-3.2825" width="0.127" layer="51"/>
<wire x1="-2.045" y1="-3.2825" x2="-2.045" y2="3.2825" width="0.127" layer="51"/>
<wire x1="-2.045" y1="3.2825" x2="4.055" y2="3.2825" width="0.127" layer="51"/>
<wire x1="-2.045" y1="3.2825" x2="4.055" y2="3.2825" width="0.127" layer="21"/>
<wire x1="-2.045" y1="-3.2825" x2="-2.045" y2="3.2825" width="0.127" layer="21"/>
<wire x1="4.055" y1="-3.2825" x2="-2.045" y2="-3.2825" width="0.127" layer="21"/>
<wire x1="5.845" y1="3.5325" x2="-5.845" y2="3.5325" width="0.05" layer="39"/>
<wire x1="-5.845" y1="3.5325" x2="-5.845" y2="-3.5325" width="0.05" layer="39"/>
<wire x1="-5.845" y1="-3.5325" x2="5.845" y2="-3.5325" width="0.05" layer="39"/>
<wire x1="5.845" y1="-3.5325" x2="5.845" y2="3.5325" width="0.05" layer="39"/>
<circle x="-6.095" y="2.29" radius="0.1" width="0.2" layer="21"/>
<circle x="-6.095" y="2.29" radius="0.1" width="0.2" layer="51"/>
<text x="-4" y="4" size="1.27" layer="25">&gt;NAME</text>
<text x="-4" y="-5" size="1.27" layer="27">&gt;VALUE</text>
<smd name="1" x="-4.19" y="2.29" dx="2.81" dy="0.98" layer="1"/>
<smd name="3" x="-4.19" y="-2.29" dx="2.81" dy="0.98" layer="1"/>
<smd name="2" x="2.43" y="0" dx="5.67" dy="6.33" layer="1" rot="R90" cream="no"/>
</package>
</packages>
<symbols>
<symbol name="FDD9411L-F085">
<pin name="SOURCE" x="-7.62" y="0" length="middle"/>
<pin name="DRAIN" x="-7.62" y="2.54" length="middle"/>
<pin name="GATE" x="-7.62" y="5.08" length="middle"/>
<wire x1="-2.54" y1="7.62" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="10.16" y2="-2.54" width="0.254" layer="94"/>
<wire x1="10.16" y1="-2.54" x2="10.16" y2="7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="7.62" x2="-2.54" y2="7.62" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="FDD9411L-F085" uservalue="yes">
<gates>
<gate name="G$1" symbol="FDD9411L-F085" x="-2.54" y="-2.54"/>
</gates>
<devices>
<device name="" package="TO229P994X240-3N">
<connects>
<connect gate="G$1" pin="DRAIN" pad="2"/>
<connect gate="G$1" pin="GATE" pad="1"/>
<connect gate="G$1" pin="SOURCE" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="pinhead" urn="urn:adsk.eagle:library:325">
<description>&lt;b&gt;Pin Header Connectors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="1X01" urn="urn:adsk.eagle:footprint:22382/1" library_version="4">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" shape="octagon"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
</packages>
<packages3d>
<package3d name="1X01" urn="urn:adsk.eagle:package:22485/2" type="model" library_version="4">
<description>PIN HEADER</description>
<packageinstances>
<packageinstance name="1X01"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="PINHD1" urn="urn:adsk.eagle:symbol:22381/1" library_version="4">
<wire x1="-6.35" y1="-2.54" x2="1.27" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.27" y2="2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="-6.35" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="2.54" x2="-6.35" y2="-2.54" width="0.4064" layer="94"/>
<text x="-6.35" y="3.175" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PINHD-1X1" urn="urn:adsk.eagle:component:22540/3" prefix="JP" uservalue="yes" library_version="4">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="PINHD1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X01">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:22485/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="64" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="ACC1" library="SamacSys_Parts" library_urn="urn:adsk.eagle:library:19142403" deviceset="AIS1120SXTR" device=""/>
<part name="C1" library="DE6E3KJ472MB3B" deviceset="DE6E3KJ472MB3B" device=""/>
<part name="C2" library="DE6E3KJ472MB3B" deviceset="DE6E3KJ472MB3B" device=""/>
<part name="ACC2" library="SamacSys_Parts" library_urn="urn:adsk.eagle:library:19142403" deviceset="AIS1120SXTR" device=""/>
<part name="U$2" library="TO229P994X240-3N" deviceset="FDD9411L-F085" device="" value="Acc Power Fet"/>
<part name="U$1" library="TO229P994X240-3N" deviceset="FDD9411L-F085" device="" value="Gyro Power Fet"/>
<part name="C3" library="DE6E3KJ472MB3B" deviceset="DE6E3KJ472MB3B" device=""/>
<part name="IC1" library="SamacSys_Parts" library_urn="urn:adsk.eagle:library:19142403" deviceset="SCR2100-D08-05" device=""/>
<part name="IC2" library="SamacSys_Parts" library_urn="urn:adsk.eagle:library:19142403" deviceset="SCR2100-D08-05" device=""/>
<part name="C4" library="DE6E3KJ472MB3B" deviceset="DE6E3KJ472MB3B" device=""/>
<part name="C6" library="DE6E3KJ472MB3B" deviceset="DE6E3KJ472MB3B" device=""/>
<part name="JP6" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP1" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP2" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP3" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP4" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP5" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP7" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP8" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP9" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP10" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP11" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP12" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP13" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP14" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP15" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
</parts>
<sheets>
<sheet>
<plain>
<text x="-40.64" y="101.6" size="12.7" layer="97">Accelerometers</text>
<text x="149.86" y="99.06" size="12.7" layer="97">Test Pins</text>
<text x="132.08" y="20.32" size="12.7" layer="97">Power Switching</text>
<text x="-25.4" y="43.18" size="12.7" layer="97">Gyroscope</text>
</plain>
<instances>
<instance part="ACC1" gate="G$1" x="-2.54" y="91.44" smashed="yes">
<attribute name="NAME" x="3.81" y="96.52" size="1.778" layer="95" align="center-left"/>
</instance>
<instance part="C1" gate="G$1" x="38.1" y="78.74" smashed="yes" rot="R180">
<attribute name="NAME" x="38.1" y="77.46906875" size="1.77843125" layer="95" rot="MR180"/>
</instance>
<instance part="C2" gate="G$1" x="55.88" y="78.74" smashed="yes" rot="R180">
<attribute name="NAME" x="55.88" y="74.92906875" size="1.77843125" layer="95" rot="R180"/>
</instance>
<instance part="ACC2" gate="G$1" x="-2.54" y="66.04" smashed="yes" rot="MR180">
<attribute name="NAME" x="3.81" y="60.96" size="1.778" layer="95" rot="MR180" align="center-left"/>
</instance>
<instance part="U$2" gate="G$1" x="154.94" y="5.08" smashed="yes" rot="MR0"/>
<instance part="U$1" gate="G$1" x="223.52" y="5.08" smashed="yes"/>
<instance part="C3" gate="G$1" x="185.42" y="0" smashed="yes" rot="R270">
<attribute name="NAME" x="189.23093125" y="0" size="1.77843125" layer="95" rot="R270"/>
</instance>
<instance part="IC1" gate="G$1" x="-5.08" y="30.48" smashed="yes"/>
<instance part="IC2" gate="G$1" x="-5.08" y="-38.1" smashed="yes"/>
<instance part="C4" gate="G$1" x="2.54" y="-25.4" smashed="yes" rot="R90">
<attribute name="NAME" x="-1.27093125" y="-25.4" size="1.77843125" layer="95" rot="R90"/>
</instance>
<instance part="C6" gate="G$1" x="25.4" y="-10.16" smashed="yes" rot="R270">
<attribute name="NAME" x="29.21093125" y="-10.16" size="1.77843125" layer="95" rot="R270"/>
</instance>
<instance part="JP6" gate="G$1" x="208.28" y="60.96" smashed="yes">
<attribute name="NAME" x="209.55" y="61.595" size="1.778" layer="95"/>
<attribute name="VALUE" x="201.93" y="55.88" size="1.778" layer="96"/>
</instance>
<instance part="JP1" gate="G$1" x="208.28" y="86.36" smashed="yes">
<attribute name="NAME" x="209.55" y="86.995" size="1.778" layer="95"/>
<attribute name="VALUE" x="201.93" y="81.28" size="1.778" layer="96"/>
</instance>
<instance part="JP2" gate="G$1" x="208.28" y="81.28" smashed="yes">
<attribute name="NAME" x="209.55" y="81.915" size="1.778" layer="95"/>
<attribute name="VALUE" x="201.93" y="76.2" size="1.778" layer="96"/>
</instance>
<instance part="JP3" gate="G$1" x="208.28" y="76.2" smashed="yes">
<attribute name="NAME" x="209.55" y="76.835" size="1.778" layer="95"/>
<attribute name="VALUE" x="201.93" y="71.12" size="1.778" layer="96"/>
</instance>
<instance part="JP4" gate="G$1" x="208.28" y="71.12" smashed="yes">
<attribute name="NAME" x="209.55" y="71.755" size="1.778" layer="95"/>
<attribute name="VALUE" x="201.93" y="66.04" size="1.778" layer="96"/>
</instance>
<instance part="JP5" gate="G$1" x="208.28" y="66.04" smashed="yes">
<attribute name="NAME" x="209.55" y="66.675" size="1.778" layer="95"/>
<attribute name="VALUE" x="201.93" y="60.96" size="1.778" layer="96"/>
</instance>
<instance part="JP7" gate="G$1" x="208.28" y="55.88" smashed="yes">
<attribute name="NAME" x="209.55" y="56.515" size="1.778" layer="95"/>
<attribute name="VALUE" x="201.93" y="50.8" size="1.778" layer="96"/>
</instance>
<instance part="JP8" gate="G$1" x="208.28" y="50.8" smashed="yes">
<attribute name="NAME" x="209.55" y="51.435" size="1.778" layer="95"/>
<attribute name="VALUE" x="201.93" y="45.72" size="1.778" layer="96"/>
</instance>
<instance part="JP9" gate="G$1" x="208.28" y="45.72" smashed="yes">
<attribute name="NAME" x="209.55" y="46.355" size="1.778" layer="95"/>
<attribute name="VALUE" x="201.93" y="40.64" size="1.778" layer="96"/>
</instance>
<instance part="JP10" gate="G$1" x="228.6" y="86.36" smashed="yes" rot="MR0">
<attribute name="NAME" x="227.33" y="86.995" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="234.95" y="81.28" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="JP11" gate="G$1" x="228.6" y="81.28" smashed="yes" rot="MR0">
<attribute name="NAME" x="227.33" y="81.915" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="234.95" y="76.2" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="JP12" gate="G$1" x="228.6" y="76.2" smashed="yes" rot="MR0">
<attribute name="NAME" x="227.33" y="76.835" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="234.95" y="71.12" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="JP13" gate="G$1" x="228.6" y="71.12" smashed="yes" rot="MR0">
<attribute name="NAME" x="227.33" y="71.755" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="234.95" y="66.04" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="JP14" gate="G$1" x="228.6" y="66.04" smashed="yes" rot="MR0">
<attribute name="NAME" x="227.33" y="66.675" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="234.95" y="60.96" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="JP15" gate="G$1" x="228.6" y="60.96" smashed="yes" rot="MR0">
<attribute name="NAME" x="227.33" y="61.595" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="234.95" y="55.88" size="1.778" layer="96" rot="MR0"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="ACC1" gate="G$1" pin="MP"/>
<wire x1="25.4" y1="91.44" x2="40.64" y2="91.44" width="0.1524" layer="91"/>
<label x="35.56" y="91.44" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="ACC2" gate="G$1" pin="MP"/>
<wire x1="25.4" y1="66.04" x2="40.64" y2="66.04" width="0.1524" layer="91"/>
<label x="35.56" y="66.04" size="1.778" layer="95" rot="MR180"/>
</segment>
<segment>
<wire x1="205.74" y1="86.36" x2="180.34" y2="86.36" width="0.1524" layer="91"/>
<label x="180.34" y="86.36" size="1.778" layer="95"/>
<pinref part="JP1" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="ACC2" gate="G$1" pin="GND"/>
<wire x1="25.4" y1="73.66" x2="33.02" y2="73.66" width="0.1524" layer="91"/>
<label x="27.94" y="73.66" size="1.778" layer="95" rot="MR180"/>
<pinref part="ACC1" gate="G$1" pin="GND"/>
<wire x1="25.4" y1="83.82" x2="33.02" y2="83.82" width="0.1524" layer="91"/>
<wire x1="33.02" y1="83.82" x2="33.02" y2="78.74" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="33.02" y1="78.74" x2="33.02" y2="73.66" width="0.1524" layer="91"/>
<junction x="33.02" y="78.74"/>
</segment>
<segment>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="50.8" y1="78.74" x2="45.72" y2="78.74" width="0.1524" layer="91"/>
<label x="45.72" y="78.74" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="C3" gate="G$1" pin="2"/>
<wire x1="185.42" y1="-5.08" x2="185.42" y2="-17.78" width="0.1524" layer="91"/>
<label x="185.42" y="-12.7" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="HEAT_2"/>
<wire x1="-12.7" y1="2.54" x2="-5.08" y2="2.54" width="0.1524" layer="91"/>
<label x="-12.7" y="2.54" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="HEAT_1"/>
<wire x1="-5.08" y1="30.48" x2="-12.7" y2="30.48" width="0.1524" layer="91"/>
<label x="-12.7" y="30.48" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="DVSS"/>
<wire x1="-5.08" y1="12.7" x2="-12.7" y2="12.7" width="0.1524" layer="91"/>
<label x="-12.7" y="12.7" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="HEAT_4"/>
<wire x1="38.1" y1="30.48" x2="45.72" y2="30.48" width="0.1524" layer="91"/>
<label x="40.64" y="30.48" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="AVSS"/>
<wire x1="38.1" y1="12.7" x2="45.72" y2="12.7" width="0.1524" layer="91"/>
<label x="40.64" y="12.7" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="AVSS_REF"/>
<wire x1="45.72" y1="10.16" x2="38.1" y2="10.16" width="0.1524" layer="91"/>
<label x="40.64" y="10.16" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="HEAT_3"/>
<wire x1="38.1" y1="2.54" x2="45.72" y2="2.54" width="0.1524" layer="91"/>
<label x="40.64" y="2.54" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="HEAT_2"/>
<wire x1="-12.7" y1="-66.04" x2="-5.08" y2="-66.04" width="0.1524" layer="91"/>
<label x="-12.7" y="-66.04" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="HEAT_1"/>
<wire x1="-5.08" y1="-38.1" x2="-12.7" y2="-38.1" width="0.1524" layer="91"/>
<label x="-12.7" y="-38.1" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="DVSS"/>
<wire x1="-5.08" y1="-55.88" x2="-12.7" y2="-55.88" width="0.1524" layer="91"/>
<label x="-12.7" y="-55.88" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="HEAT_4"/>
<wire x1="38.1" y1="-38.1" x2="45.72" y2="-38.1" width="0.1524" layer="91"/>
<label x="40.64" y="-38.1" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="AVSS"/>
<wire x1="38.1" y1="-55.88" x2="45.72" y2="-55.88" width="0.1524" layer="91"/>
<label x="40.64" y="-55.88" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="AVSS_REF"/>
<wire x1="45.72" y1="-58.42" x2="38.1" y2="-58.42" width="0.1524" layer="91"/>
<label x="40.64" y="-58.42" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="HEAT_3"/>
<wire x1="38.1" y1="-66.04" x2="45.72" y2="-66.04" width="0.1524" layer="91"/>
<label x="40.64" y="-66.04" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="C4" gate="G$1" pin="2"/>
<pinref part="C6" gate="G$1" pin="2"/>
<wire x1="2.54" y1="-20.32" x2="25.4" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="25.4" y1="-20.32" x2="25.4" y2="-15.24" width="0.1524" layer="91"/>
<label x="12.7" y="-20.32" size="1.778" layer="95"/>
</segment>
</net>
<net name="VBAT" class="0">
<segment>
<label x="162.56" y="5.08" size="1.778" layer="95"/>
<pinref part="U$2" gate="G$1" pin="SOURCE"/>
<wire x1="162.56" y1="5.08" x2="185.42" y2="5.08" width="0.1524" layer="91"/>
<label x="193.04" y="5.08" size="1.778" layer="95"/>
<wire x1="215.9" y1="5.08" x2="185.42" y2="5.08" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="SOURCE"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="185.42" y1="5.08" x2="185.42" y2="2.54" width="0.1524" layer="91"/>
<junction x="185.42" y="5.08"/>
</segment>
<segment>
<wire x1="205.74" y1="81.28" x2="180.34" y2="81.28" width="0.1524" layer="91"/>
<label x="180.34" y="81.28" size="1.778" layer="95"/>
<pinref part="JP2" gate="G$1" pin="1"/>
</segment>
</net>
<net name="ACC_PWR_SW" class="0">
<segment>
<wire x1="180.34" y1="10.16" x2="162.56" y2="10.16" width="0.1524" layer="91"/>
<label x="162.56" y="10.16" size="1.778" layer="95"/>
<pinref part="U$2" gate="G$1" pin="GATE"/>
</segment>
<segment>
<wire x1="205.74" y1="50.8" x2="180.34" y2="50.8" width="0.1524" layer="91"/>
<label x="180.34" y="50.8" size="1.778" layer="95"/>
<pinref part="JP8" gate="G$1" pin="1"/>
</segment>
</net>
<net name="ACC_PWR_FET" class="0">
<segment>
<wire x1="180.34" y1="7.62" x2="162.56" y2="7.62" width="0.1524" layer="91"/>
<label x="162.56" y="7.62" size="1.778" layer="95"/>
<pinref part="U$2" gate="G$1" pin="DRAIN"/>
</segment>
<segment>
<pinref part="ACC2" gate="G$1" pin="VDD"/>
<wire x1="25.4" y1="68.58" x2="58.42" y2="68.58" width="0.1524" layer="91"/>
<pinref part="ACC1" gate="G$1" pin="VDD"/>
<wire x1="25.4" y1="88.9" x2="58.42" y2="88.9" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="58.42" y1="88.9" x2="58.42" y2="78.74" width="0.1524" layer="91"/>
<wire x1="58.42" y1="68.58" x2="58.42" y2="78.74" width="0.1524" layer="91"/>
<junction x="58.42" y="78.74"/>
<wire x1="58.42" y1="78.74" x2="68.58" y2="78.74" width="0.1524" layer="91"/>
<label x="60.96" y="78.74" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="205.74" y1="45.72" x2="180.34" y2="45.72" width="0.1524" layer="91"/>
<label x="180.34" y="45.72" size="1.778" layer="95"/>
<pinref part="JP9" gate="G$1" pin="1"/>
</segment>
</net>
<net name="VREG" class="0">
<segment>
<pinref part="ACC1" gate="G$1" pin="VREG"/>
<wire x1="40.64" y1="86.36" x2="25.4" y2="86.36" width="0.1524" layer="91"/>
<pinref part="ACC2" gate="G$1" pin="VREG"/>
<wire x1="40.64" y1="71.12" x2="25.4" y2="71.12" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="40.64" y1="71.12" x2="40.64" y2="78.74" width="0.1524" layer="91"/>
<wire x1="40.64" y1="86.36" x2="40.64" y2="78.74" width="0.1524" layer="91"/>
<junction x="40.64" y="78.74"/>
<label x="33.02" y="86.36" size="1.778" layer="95"/>
</segment>
</net>
<net name="GYRO_PWR_SW" class="0">
<segment>
<wire x1="193.04" y1="10.16" x2="215.9" y2="10.16" width="0.1524" layer="91"/>
<label x="193.04" y="10.16" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="GATE"/>
</segment>
<segment>
<wire x1="261.62" y1="81.28" x2="231.14" y2="81.28" width="0.1524" layer="91"/>
<label x="243.84" y="81.28" size="1.778" layer="95"/>
<pinref part="JP11" gate="G$1" pin="1"/>
</segment>
</net>
<net name="GYRO_PWR_FET" class="0">
<segment>
<wire x1="193.04" y1="7.62" x2="215.9" y2="7.62" width="0.1524" layer="91"/>
<label x="193.04" y="7.62" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="DRAIN"/>
</segment>
<segment>
<wire x1="266.7" y1="86.36" x2="231.14" y2="86.36" width="0.1524" layer="91"/>
<label x="243.84" y="86.36" size="1.778" layer="95"/>
<pinref part="JP10" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="DVDD"/>
<wire x1="-5.08" y1="10.16" x2="-38.1" y2="10.16" width="0.1524" layer="91"/>
<label x="-27.94" y="10.16" size="1.778" layer="95"/>
<pinref part="IC2" gate="G$1" pin="DVDD"/>
<wire x1="-38.1" y1="10.16" x2="-38.1" y2="-27.94" width="0.1524" layer="91"/>
<wire x1="-38.1" y1="-27.94" x2="-38.1" y2="-58.42" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="-58.42" x2="-38.1" y2="-58.42" width="0.1524" layer="91"/>
<label x="-27.94" y="-58.42" size="1.778" layer="95"/>
<pinref part="IC1" gate="G$1" pin="AVDD"/>
<wire x1="38.1" y1="5.08" x2="71.12" y2="5.08" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="AVDD"/>
<wire x1="38.1" y1="-63.5" x2="71.12" y2="-63.5" width="0.1524" layer="91"/>
<wire x1="71.12" y1="-63.5" x2="71.12" y2="-27.94" width="0.1524" layer="91"/>
<label x="43.18" y="-63.5" size="1.778" layer="95"/>
<label x="43.18" y="5.08" size="1.778" layer="95"/>
<wire x1="71.12" y1="-27.94" x2="71.12" y2="5.08" width="0.1524" layer="91"/>
<wire x1="-38.1" y1="-27.94" x2="2.54" y2="-27.94" width="0.1524" layer="91"/>
<junction x="-38.1" y="-27.94"/>
<junction x="71.12" y="-27.94"/>
<label x="-25.4" y="-27.94" size="1.778" layer="95"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="2.54" y1="-27.94" x2="71.12" y2="-27.94" width="0.1524" layer="91"/>
<junction x="2.54" y="-27.94"/>
</segment>
</net>
<net name="SPI_MISO" class="0">
<segment>
<pinref part="ACC1" gate="G$1" pin="SDO"/>
<wire x1="-2.54" y1="86.36" x2="-17.78" y2="86.36" width="0.1524" layer="91"/>
<label x="-20.32" y="86.36" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="ACC2" gate="G$1" pin="SDO"/>
<wire x1="-2.54" y1="71.12" x2="-17.78" y2="71.12" width="0.1524" layer="91"/>
<label x="-20.32" y="73.66" size="1.778" layer="95" rot="MR180"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="MISO"/>
<wire x1="-5.08" y1="20.32" x2="-27.94" y2="20.32" width="0.1524" layer="91"/>
<label x="-27.94" y="20.32" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="MISO"/>
<wire x1="-5.08" y1="-48.26" x2="-27.94" y2="-48.26" width="0.1524" layer="91"/>
<label x="-27.94" y="-48.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="JP3" gate="G$1" pin="1"/>
<wire x1="205.74" y1="76.2" x2="180.34" y2="76.2" width="0.1524" layer="91"/>
<label x="180.34" y="76.2" size="1.778" layer="95"/>
</segment>
</net>
<net name="SPI_MOSI" class="0">
<segment>
<wire x1="205.74" y1="71.12" x2="180.34" y2="71.12" width="0.1524" layer="91"/>
<label x="180.34" y="71.12" size="1.778" layer="95"/>
<pinref part="JP4" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="ACC1" gate="G$1" pin="SDI"/>
<wire x1="-2.54" y1="88.9" x2="-17.78" y2="88.9" width="0.1524" layer="91"/>
<label x="-20.32" y="88.9" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="MOSI"/>
<wire x1="38.1" y1="17.78" x2="60.96" y2="17.78" width="0.1524" layer="91"/>
<label x="43.18" y="17.78" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="MOSI"/>
<wire x1="38.1" y1="-50.8" x2="60.96" y2="-50.8" width="0.1524" layer="91"/>
<label x="43.18" y="-50.8" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="ACC2" gate="G$1" pin="SDI"/>
<wire x1="-2.54" y1="68.58" x2="-17.78" y2="68.58" width="0.1524" layer="91"/>
<label x="-20.32" y="71.12" size="1.778" layer="95" rot="MR180"/>
</segment>
</net>
<net name="SPI_SCK" class="0">
<segment>
<wire x1="205.74" y1="66.04" x2="180.34" y2="66.04" width="0.1524" layer="91"/>
<label x="180.34" y="66.04" size="1.778" layer="95"/>
<pinref part="JP5" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="ACC1" gate="G$1" pin="SCL"/>
<wire x1="-2.54" y1="91.44" x2="-17.78" y2="91.44" width="0.1524" layer="91"/>
<label x="-20.32" y="91.44" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="ACC2" gate="G$1" pin="SCL"/>
<wire x1="-2.54" y1="66.04" x2="-17.78" y2="66.04" width="0.1524" layer="91"/>
<label x="-20.32" y="68.58" size="1.778" layer="95" rot="MR180"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="SCK"/>
<wire x1="-5.08" y1="22.86" x2="-27.94" y2="22.86" width="0.1524" layer="91"/>
<label x="-27.94" y="22.86" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="SCK"/>
<wire x1="-5.08" y1="-45.72" x2="-27.94" y2="-45.72" width="0.1524" layer="91"/>
<label x="-27.94" y="-45.72" size="1.778" layer="95"/>
</segment>
</net>
<net name="SPI_ACC_CS1" class="0">
<segment>
<wire x1="205.74" y1="60.96" x2="180.34" y2="60.96" width="0.1524" layer="91"/>
<label x="180.34" y="60.96" size="1.778" layer="95"/>
<pinref part="JP6" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="ACC1" gate="G$1" pin="CS"/>
<wire x1="-2.54" y1="83.82" x2="-17.78" y2="83.82" width="0.1524" layer="91"/>
<label x="-20.32" y="83.82" size="1.778" layer="95"/>
</segment>
</net>
<net name="SPI_ACC_CS2" class="0">
<segment>
<wire x1="205.74" y1="55.88" x2="180.34" y2="55.88" width="0.1524" layer="91"/>
<label x="180.34" y="55.88" size="1.778" layer="95"/>
<pinref part="JP7" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="ACC2" gate="G$1" pin="CS"/>
<wire x1="-2.54" y1="73.66" x2="-17.78" y2="73.66" width="0.1524" layer="91"/>
<label x="-20.32" y="76.2" size="1.778" layer="95" rot="MR180"/>
</segment>
</net>
<net name="SPI_GYRO_CSB1" class="0">
<segment>
<wire x1="231.14" y1="76.2" x2="264.16" y2="76.2" width="0.1524" layer="91"/>
<label x="243.84" y="76.2" size="1.778" layer="95"/>
<pinref part="JP12" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="CSB"/>
<wire x1="38.1" y1="15.24" x2="60.96" y2="15.24" width="0.1524" layer="91"/>
<label x="43.18" y="15.24" size="1.778" layer="95"/>
</segment>
</net>
<net name="A_EXTC" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="A_EXTC"/>
<wire x1="38.1" y1="7.62" x2="66.04" y2="7.62" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="A_EXTC"/>
<wire x1="38.1" y1="-60.96" x2="66.04" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="66.04" y1="7.62" x2="66.04" y2="-7.62" width="0.1524" layer="91"/>
<label x="55.88" y="7.62" size="1.778" layer="95"/>
<pinref part="IC2" gate="G$1" pin="D_EXTC"/>
<wire x1="66.04" y1="-7.62" x2="66.04" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="-60.96" x2="-33.02" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="-33.02" y1="-60.96" x2="-33.02" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="D_EXTC"/>
<wire x1="-33.02" y1="-7.62" x2="-33.02" y2="7.62" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="7.62" x2="-33.02" y2="7.62" width="0.1524" layer="91"/>
<label x="-27.94" y="7.62" size="1.778" layer="95"/>
<wire x1="66.04" y1="-7.62" x2="25.4" y2="-7.62" width="0.1524" layer="91"/>
<junction x="66.04" y="-7.62"/>
<junction x="-33.02" y="-7.62"/>
<label x="30.48" y="-7.62" size="1.778" layer="95"/>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="25.4" y1="-7.62" x2="-33.02" y2="-7.62" width="0.1524" layer="91"/>
<junction x="25.4" y="-7.62"/>
</segment>
</net>
<net name="SPI_GYRO_CSB2" class="0">
<segment>
<wire x1="264.16" y1="71.12" x2="231.14" y2="71.12" width="0.1524" layer="91"/>
<label x="243.84" y="71.12" size="1.778" layer="95"/>
<pinref part="JP13" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="CSB"/>
<wire x1="38.1" y1="-53.34" x2="60.96" y2="-53.34" width="0.1524" layer="91"/>
<label x="43.18" y="-53.34" size="1.778" layer="95"/>
</segment>
</net>
<net name="GYRO1_EXT_RSET" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="EXTRESN"/>
<wire x1="-5.08" y1="25.4" x2="-27.94" y2="25.4" width="0.1524" layer="91"/>
<label x="-27.94" y="25.4" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="264.16" y1="66.04" x2="231.14" y2="66.04" width="0.1524" layer="91"/>
<label x="243.84" y="66.04" size="1.778" layer="95"/>
<pinref part="JP14" gate="G$1" pin="1"/>
</segment>
</net>
<net name="GYRO2_EXT_RSET" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="EXTRESN"/>
<wire x1="-5.08" y1="-43.18" x2="-27.94" y2="-43.18" width="0.1524" layer="91"/>
<label x="-27.94" y="-43.18" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="264.16" y1="60.96" x2="231.14" y2="60.96" width="0.1524" layer="91"/>
<label x="243.84" y="60.96" size="1.778" layer="95"/>
<pinref part="JP15" gate="G$1" pin="1"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="113,1,204.168,83.82,JP1,,,,,"/>
<approved hash="113,1,204.168,73.66,JP2,,,,,"/>
<approved hash="113,1,204.168,63.5,JP3,,,,,"/>
<approved hash="113,1,204.168,53.34,JP4,,,,,"/>
<approved hash="113,1,204.168,43.18,JP5,,,,,"/>
</errors>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
