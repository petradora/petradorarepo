<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.5.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="SamacSys_Parts">
<description>&lt;b&gt;https://componentsearchengine.com&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="QFN50P300X300X150-17N">
<description>&lt;b&gt;cp-16-28&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-1.45" y="0.75" dx="0.85" dy="0.3" layer="1"/>
<smd name="2" x="-1.45" y="0.25" dx="0.85" dy="0.3" layer="1"/>
<smd name="3" x="-1.45" y="-0.25" dx="0.85" dy="0.3" layer="1"/>
<smd name="4" x="-1.45" y="-0.75" dx="0.85" dy="0.3" layer="1"/>
<smd name="5" x="-0.75" y="-1.45" dx="0.85" dy="0.3" layer="1" rot="R90"/>
<smd name="6" x="-0.25" y="-1.45" dx="0.85" dy="0.3" layer="1" rot="R90"/>
<smd name="7" x="0.25" y="-1.45" dx="0.85" dy="0.3" layer="1" rot="R90"/>
<smd name="8" x="0.75" y="-1.45" dx="0.85" dy="0.3" layer="1" rot="R90"/>
<smd name="9" x="1.45" y="-0.75" dx="0.85" dy="0.3" layer="1"/>
<smd name="10" x="1.45" y="-0.25" dx="0.85" dy="0.3" layer="1"/>
<smd name="11" x="1.45" y="0.25" dx="0.85" dy="0.3" layer="1"/>
<smd name="12" x="1.45" y="0.75" dx="0.85" dy="0.3" layer="1"/>
<smd name="13" x="0.75" y="1.45" dx="0.85" dy="0.3" layer="1" rot="R90"/>
<smd name="14" x="0.25" y="1.45" dx="0.85" dy="0.3" layer="1" rot="R90"/>
<smd name="15" x="-0.25" y="1.45" dx="0.85" dy="0.3" layer="1" rot="R90"/>
<smd name="16" x="-0.75" y="1.45" dx="0.85" dy="0.3" layer="1" rot="R90"/>
<smd name="17" x="0" y="0" dx="1.7" dy="1.7" layer="1" rot="R90"/>
<text x="0" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-2.125" y1="2.125" x2="2.125" y2="2.125" width="0.05" layer="51"/>
<wire x1="2.125" y1="2.125" x2="2.125" y2="-2.125" width="0.05" layer="51"/>
<wire x1="2.125" y1="-2.125" x2="-2.125" y2="-2.125" width="0.05" layer="51"/>
<wire x1="-2.125" y1="-2.125" x2="-2.125" y2="2.125" width="0.05" layer="51"/>
<wire x1="-1.5" y1="1.5" x2="1.5" y2="1.5" width="0.1" layer="51"/>
<wire x1="1.5" y1="1.5" x2="1.5" y2="-1.5" width="0.1" layer="51"/>
<wire x1="1.5" y1="-1.5" x2="-1.5" y2="-1.5" width="0.1" layer="51"/>
<wire x1="-1.5" y1="-1.5" x2="-1.5" y2="1.5" width="0.1" layer="51"/>
<wire x1="-1.5" y1="1" x2="-1" y2="1.5" width="0.1" layer="51"/>
<circle x="-1.875" y="1.5" radius="0.125" width="0.25" layer="25"/>
</package>
</packages>
<symbols>
<symbol name="ADXL377BCPZ-RL7">
<wire x1="5.08" y1="20.32" x2="25.4" y2="20.32" width="0.254" layer="94"/>
<wire x1="25.4" y1="-20.32" x2="25.4" y2="20.32" width="0.254" layer="94"/>
<wire x1="25.4" y1="-20.32" x2="5.08" y2="-20.32" width="0.254" layer="94"/>
<wire x1="5.08" y1="20.32" x2="5.08" y2="-20.32" width="0.254" layer="94"/>
<text x="26.67" y="25.4" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="26.67" y="22.86" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="RES_1" x="0" y="0" length="middle"/>
<pin name="ST" x="0" y="-2.54" length="middle"/>
<pin name="RES_2" x="0" y="-5.08" length="middle"/>
<pin name="YOUT" x="0" y="-7.62" length="middle"/>
<pin name="XOUT" x="10.16" y="-25.4" length="middle" rot="R90"/>
<pin name="GND_1" x="12.7" y="-25.4" length="middle" rot="R90"/>
<pin name="GND_2" x="15.24" y="-25.4" length="middle" rot="R90"/>
<pin name="NC_1" x="17.78" y="-25.4" length="middle" rot="R90"/>
<pin name="NC_5" x="30.48" y="0" length="middle" rot="R180"/>
<pin name="NC_4" x="30.48" y="-2.54" length="middle" rot="R180"/>
<pin name="NC_3" x="30.48" y="-5.08" length="middle" rot="R180"/>
<pin name="NC_2" x="30.48" y="-7.62" length="middle" rot="R180"/>
<pin name="THERMAL_PAD" x="10.16" y="25.4" length="middle" rot="R270"/>
<pin name="ZOUT" x="12.7" y="25.4" length="middle" rot="R270"/>
<pin name="VS_2" x="15.24" y="25.4" length="middle" rot="R270"/>
<pin name="VS_1" x="17.78" y="25.4" length="middle" rot="R270"/>
<pin name="NC_6" x="20.32" y="25.4" length="middle" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ADXL377BCPZ-RL7" prefix="AC">
<description>&lt;b&gt;Accelerometers 3-Axis High g Analog MEMS&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="https://componentsearchengine.com/Datasheets/1/ADXL377BCPZ-RL7.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="ADXL377BCPZ-RL7" x="0" y="0"/>
</gates>
<devices>
<device name="" package="QFN50P300X300X150-17N">
<connects>
<connect gate="G$1" pin="GND_1" pad="6"/>
<connect gate="G$1" pin="GND_2" pad="7"/>
<connect gate="G$1" pin="NC_1" pad="8"/>
<connect gate="G$1" pin="NC_2" pad="9"/>
<connect gate="G$1" pin="NC_3" pad="10"/>
<connect gate="G$1" pin="NC_4" pad="11"/>
<connect gate="G$1" pin="NC_5" pad="12"/>
<connect gate="G$1" pin="NC_6" pad="13"/>
<connect gate="G$1" pin="RES_1" pad="1"/>
<connect gate="G$1" pin="RES_2" pad="3"/>
<connect gate="G$1" pin="ST" pad="2"/>
<connect gate="G$1" pin="THERMAL_PAD" pad="17"/>
<connect gate="G$1" pin="VS_1" pad="14"/>
<connect gate="G$1" pin="VS_2" pad="15"/>
<connect gate="G$1" pin="XOUT" pad="5"/>
<connect gate="G$1" pin="YOUT" pad="4"/>
<connect gate="G$1" pin="ZOUT" pad="16"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="Accelerometers 3-Axis High g Analog MEMS" constant="no"/>
<attribute name="HEIGHT" value="1.5mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Analog Devices" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="ADXL377BCPZ-RL7" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="584-ADXL377BCPZ-R7" constant="no"/>
<attribute name="MOUSER_PRICE-STOCK" value="https://www.mouser.com/Search/Refine.aspx?Keyword=584-ADXL377BCPZ-R7" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="AC1" library="SamacSys_Parts" deviceset="ADXL377BCPZ-RL7" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="AC1" gate="G$1" x="43.18" y="127" smashed="yes">
<attribute name="NAME" x="69.85" y="152.4" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="69.85" y="149.86" size="1.778" layer="96" align="center-left"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="SNS_1_Z" class="0">
<segment>
<pinref part="AC1" gate="G$1" pin="ZOUT"/>
<wire x1="55.88" y1="152.4" x2="55.88" y2="160.02" width="0.1524" layer="91"/>
<label x="55.88" y="162.56" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="SNS_1_X" class="0">
<segment>
<pinref part="AC1" gate="G$1" pin="XOUT"/>
<wire x1="53.34" y1="101.6" x2="53.34" y2="88.9" width="0.1524" layer="91"/>
<label x="53.34" y="71.12" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="SNS_1_Y" class="0">
<segment>
<pinref part="AC1" gate="G$1" pin="YOUT"/>
<wire x1="43.18" y1="119.38" x2="33.02" y2="119.38" width="0.1524" layer="91"/>
<label x="15.24" y="119.38" size="1.778" layer="95"/>
</segment>
</net>
<net name="ACC_ST" class="0">
<segment>
<pinref part="AC1" gate="G$1" pin="ST"/>
<wire x1="43.18" y1="124.46" x2="33.02" y2="124.46" width="0.1524" layer="91"/>
<label x="15.24" y="124.46" size="1.778" layer="95"/>
</segment>
</net>
<net name="VCC_3.3" class="0">
<segment>
<pinref part="AC1" gate="G$1" pin="VS_1"/>
<wire x1="60.96" y1="152.4" x2="60.96" y2="160.02" width="0.1524" layer="91"/>
<label x="60.96" y="162.56" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="AC1" gate="G$1" pin="VS_2"/>
<wire x1="58.42" y1="152.4" x2="58.42" y2="160.02" width="0.1524" layer="91"/>
<label x="58.42" y="162.56" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="AC1" gate="G$1" pin="GND_1"/>
<wire x1="55.88" y1="101.6" x2="55.88" y2="88.9" width="0.1524" layer="91"/>
<label x="55.88" y="71.12" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="AC1" gate="G$1" pin="GND_2"/>
<wire x1="58.42" y1="101.6" x2="58.42" y2="88.9" width="0.1524" layer="91"/>
<label x="58.42" y="71.12" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="AC1" gate="G$1" pin="RES_1"/>
<wire x1="43.18" y1="127" x2="33.02" y2="127" width="0.1524" layer="91"/>
<label x="15.24" y="127" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="AC1" gate="G$1" pin="RES_2"/>
<wire x1="43.18" y1="121.92" x2="33.02" y2="121.92" width="0.1524" layer="91"/>
<label x="15.24" y="121.92" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
