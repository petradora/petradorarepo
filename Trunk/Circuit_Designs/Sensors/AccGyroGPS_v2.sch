<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.5.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="SamacSys_Parts">
<packages>
<package name="DFN1600X1600X750-13N">
<wire x1="-8" y1="-8" x2="8" y2="-8" width="0.127" layer="21"/>
<wire x1="8" y1="-8" x2="8" y2="8" width="0.127" layer="51"/>
<wire x1="8" y1="8" x2="-8" y2="8" width="0.127" layer="21"/>
<wire x1="-8" y1="8" x2="-8" y2="-8" width="0.127" layer="51"/>
<wire x1="-9.5" y1="8.25" x2="9.5" y2="8.25" width="0.127" layer="39"/>
<wire x1="9.5" y1="8.25" x2="9.5" y2="-8.25" width="0.127" layer="39"/>
<wire x1="9.5" y1="-8.25" x2="-9.5" y2="-8.25" width="0.127" layer="39"/>
<wire x1="-9.5" y1="-8.25" x2="-9.5" y2="8.25" width="0.127" layer="39"/>
<text x="-9.504090625" y="8.503659375" size="1.27055" layer="25">&gt;NAME</text>
<text x="-9.513659375" y="-9.76401875" size="1.27183125" layer="27">&gt;VALUE</text>
<smd name="1" x="-8" y="6.75" dx="2" dy="1" layer="1"/>
<smd name="2" x="-8" y="5.25" dx="2" dy="1" layer="1"/>
<smd name="3" x="-8" y="3.75" dx="2" dy="1" layer="1"/>
<smd name="4" x="-8" y="2.25" dx="2" dy="1" layer="1"/>
<smd name="5" x="-8" y="0.75" dx="2" dy="1" layer="1"/>
<smd name="6" x="-8" y="-0.75" dx="2" dy="1" layer="1"/>
<smd name="7" x="-8" y="-2.25" dx="2" dy="1" layer="1"/>
<smd name="8" x="-8" y="-3.75" dx="2" dy="1" layer="1"/>
<smd name="9" x="-8" y="-5.25" dx="2" dy="1" layer="1"/>
<smd name="10" x="-8" y="-6.75" dx="2" dy="1" layer="1"/>
<smd name="13" x="8" y="6.75" dx="2" dy="1" layer="1"/>
<smd name="12" x="8" y="0.75" dx="2" dy="1" layer="1"/>
<smd name="11" x="8" y="-6.75" dx="2" dy="1" layer="1"/>
<hole x="0.5" y="0.85" drill="3"/>
</package>
<package name="CAPC0603X33N">
<description>&lt;b&gt;GRM03_0.03 L=0.6mm W=0.3mm T=0.3mm&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-0.33" y="0" dx="0.46" dy="0.42" layer="1"/>
<smd name="2" x="0.33" y="0" dx="0.46" dy="0.42" layer="1"/>
<text x="0" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-0.71" y1="0.36" x2="0.71" y2="0.36" width="0.05" layer="51"/>
<wire x1="0.71" y1="0.36" x2="0.71" y2="-0.36" width="0.05" layer="51"/>
<wire x1="0.71" y1="-0.36" x2="-0.71" y2="-0.36" width="0.05" layer="51"/>
<wire x1="-0.71" y1="-0.36" x2="-0.71" y2="0.36" width="0.05" layer="51"/>
<wire x1="-0.3" y1="0.15" x2="0.3" y2="0.15" width="0.1" layer="51"/>
<wire x1="0.3" y1="0.15" x2="0.3" y2="-0.15" width="0.1" layer="51"/>
<wire x1="0.3" y1="-0.15" x2="-0.3" y2="-0.15" width="0.1" layer="51"/>
<wire x1="-0.3" y1="-0.15" x2="-0.3" y2="0.15" width="0.1" layer="51"/>
</package>
<package name="QFN50P300X300X150-17N">
<description>&lt;b&gt;cp-16-28&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-1.45" y="0.75" dx="0.85" dy="0.3" layer="1"/>
<smd name="2" x="-1.45" y="0.25" dx="0.85" dy="0.3" layer="1"/>
<smd name="3" x="-1.45" y="-0.25" dx="0.85" dy="0.3" layer="1"/>
<smd name="4" x="-1.45" y="-0.75" dx="0.85" dy="0.3" layer="1"/>
<smd name="5" x="-0.75" y="-1.45" dx="0.85" dy="0.3" layer="1" rot="R90"/>
<smd name="6" x="-0.25" y="-1.45" dx="0.85" dy="0.3" layer="1" rot="R90"/>
<smd name="7" x="0.25" y="-1.45" dx="0.85" dy="0.3" layer="1" rot="R90"/>
<smd name="8" x="0.75" y="-1.45" dx="0.85" dy="0.3" layer="1" rot="R90"/>
<smd name="9" x="1.45" y="-0.75" dx="0.85" dy="0.3" layer="1"/>
<smd name="10" x="1.45" y="-0.25" dx="0.85" dy="0.3" layer="1"/>
<smd name="11" x="1.45" y="0.25" dx="0.85" dy="0.3" layer="1"/>
<smd name="12" x="1.45" y="0.75" dx="0.85" dy="0.3" layer="1"/>
<smd name="13" x="0.75" y="1.45" dx="0.85" dy="0.3" layer="1" rot="R90"/>
<smd name="14" x="0.25" y="1.45" dx="0.85" dy="0.3" layer="1" rot="R90"/>
<smd name="15" x="-0.25" y="1.45" dx="0.85" dy="0.3" layer="1" rot="R90"/>
<smd name="16" x="-0.75" y="1.45" dx="0.85" dy="0.3" layer="1" rot="R90"/>
<smd name="17" x="0" y="0" dx="1.7" dy="1.7" layer="1" rot="R90"/>
<text x="0" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-2.125" y1="2.125" x2="2.125" y2="2.125" width="0.05" layer="51"/>
<wire x1="2.125" y1="2.125" x2="2.125" y2="-2.125" width="0.05" layer="51"/>
<wire x1="2.125" y1="-2.125" x2="-2.125" y2="-2.125" width="0.05" layer="51"/>
<wire x1="-2.125" y1="-2.125" x2="-2.125" y2="2.125" width="0.05" layer="51"/>
<wire x1="-1.5" y1="1.5" x2="1.5" y2="1.5" width="0.1" layer="51"/>
<wire x1="1.5" y1="1.5" x2="1.5" y2="-1.5" width="0.1" layer="51"/>
<wire x1="1.5" y1="-1.5" x2="-1.5" y2="-1.5" width="0.1" layer="51"/>
<wire x1="-1.5" y1="-1.5" x2="-1.5" y2="1.5" width="0.1" layer="51"/>
<wire x1="-1.5" y1="1" x2="-1" y2="1.5" width="0.1" layer="51"/>
<circle x="-1.875" y="1.5" radius="0.125" width="0.25" layer="25"/>
</package>
<package name="5046222012">
<description>&lt;b&gt;504622-2012-2&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-1.575" y="-0.907" dx="0.485" dy="0.18" layer="1" rot="R90"/>
<smd name="2" x="-1.575" y="0.907" dx="0.485" dy="0.18" layer="1" rot="R90"/>
<smd name="3" x="-1.225" y="-0.907" dx="0.485" dy="0.18" layer="1" rot="R90"/>
<smd name="4" x="-1.225" y="0.907" dx="0.485" dy="0.18" layer="1" rot="R90"/>
<smd name="5" x="-0.875" y="-0.907" dx="0.485" dy="0.18" layer="1" rot="R90"/>
<smd name="6" x="-0.875" y="0.907" dx="0.485" dy="0.18" layer="1" rot="R90"/>
<smd name="7" x="-0.525" y="-0.907" dx="0.485" dy="0.18" layer="1" rot="R90"/>
<smd name="8" x="-0.525" y="0.907" dx="0.485" dy="0.18" layer="1" rot="R90"/>
<smd name="9" x="-0.175" y="-0.907" dx="0.485" dy="0.18" layer="1" rot="R90"/>
<smd name="10" x="-0.175" y="0.907" dx="0.485" dy="0.18" layer="1" rot="R90"/>
<smd name="11" x="0.175" y="-0.907" dx="0.485" dy="0.18" layer="1" rot="R90"/>
<smd name="12" x="0.175" y="0.907" dx="0.485" dy="0.18" layer="1" rot="R90"/>
<smd name="13" x="0.525" y="-0.907" dx="0.485" dy="0.18" layer="1" rot="R90"/>
<smd name="14" x="0.525" y="0.907" dx="0.485" dy="0.18" layer="1" rot="R90"/>
<smd name="15" x="0.875" y="-0.907" dx="0.485" dy="0.18" layer="1" rot="R90"/>
<smd name="16" x="0.875" y="0.907" dx="0.485" dy="0.18" layer="1" rot="R90"/>
<smd name="17" x="1.225" y="-0.907" dx="0.485" dy="0.18" layer="1" rot="R90"/>
<smd name="18" x="1.225" y="0.907" dx="0.485" dy="0.18" layer="1" rot="R90"/>
<smd name="19" x="1.575" y="-0.907" dx="0.485" dy="0.18" layer="1" rot="R90"/>
<smd name="20" x="1.575" y="0.907" dx="0.485" dy="0.18" layer="1" rot="R90"/>
<smd name="MP1" x="-1.975" y="-0.907" dx="0.485" dy="0.28" layer="1" rot="R90"/>
<smd name="MP2" x="-1.975" y="0.907" dx="0.485" dy="0.28" layer="1" rot="R90"/>
<smd name="MP3" x="1.975" y="-0.907" dx="0.485" dy="0.28" layer="1" rot="R90"/>
<smd name="MP4" x="1.975" y="0.907" dx="0.485" dy="0.28" layer="1" rot="R90"/>
<text x="0" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="2.395" y1="-0.63" x2="-2.395" y2="-0.63" width="0.2" layer="51"/>
<wire x1="-2.395" y1="-0.63" x2="-2.395" y2="0.63" width="0.2" layer="51"/>
<wire x1="-2.395" y1="0.63" x2="2.395" y2="0.63" width="0.2" layer="51"/>
<wire x1="2.395" y1="0.63" x2="2.395" y2="-0.63" width="0.2" layer="51"/>
<wire x1="3.395" y1="2.149" x2="-3.395" y2="2.149" width="0.1" layer="51"/>
<wire x1="-3.395" y1="2.149" x2="-3.395" y2="-2.149" width="0.1" layer="51"/>
<wire x1="-3.395" y1="-2.149" x2="3.395" y2="-2.149" width="0.1" layer="51"/>
<wire x1="3.395" y1="-2.149" x2="3.395" y2="2.149" width="0.1" layer="51"/>
<wire x1="-2.395" y1="0.25" x2="-2.395" y2="0.25" width="0.1" layer="21"/>
<wire x1="-2.395" y1="0.25" x2="-2.395" y2="-0.25" width="0.1" layer="21"/>
<wire x1="-2.395" y1="-0.25" x2="-2.395" y2="-0.25" width="0.1" layer="21"/>
<wire x1="-2.395" y1="-0.25" x2="-2.395" y2="0.25" width="0.1" layer="21"/>
<wire x1="2.395" y1="0.25" x2="2.395" y2="0.25" width="0.1" layer="21"/>
<wire x1="2.395" y1="0.25" x2="2.395" y2="-0.25" width="0.1" layer="21"/>
<wire x1="2.395" y1="-0.25" x2="2.395" y2="-0.25" width="0.1" layer="21"/>
<wire x1="2.395" y1="-0.25" x2="2.395" y2="0.25" width="0.1" layer="21"/>
</package>
<package name="XDCR_BMG250">
<wire x1="-1.5" y1="1.25" x2="1.5" y2="1.25" width="0.127" layer="51"/>
<wire x1="1.5" y1="1.25" x2="1.5" y2="-1.25" width="0.127" layer="51"/>
<wire x1="1.5" y1="-1.25" x2="-1.5" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-1.5" y1="-1.25" x2="-1.5" y2="1.25" width="0.127" layer="51"/>
<wire x1="-0.94" y1="1.25" x2="-1.5" y2="1.25" width="0.127" layer="21"/>
<wire x1="0.94" y1="1.25" x2="1.5" y2="1.25" width="0.127" layer="21"/>
<wire x1="0.94" y1="-1.25" x2="1.5" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-0.94" y1="-1.25" x2="-1.5" y2="-1.25" width="0.127" layer="21"/>
<circle x="-2.2" y="0.9" radius="0.1" width="0.2" layer="21"/>
<circle x="-2.2" y="0.9" radius="0.1" width="0.2" layer="51"/>
<wire x1="-1.85" y1="1.6" x2="1.85" y2="1.6" width="0.05" layer="39"/>
<wire x1="1.85" y1="1.6" x2="1.85" y2="-1.6" width="0.05" layer="39"/>
<wire x1="1.85" y1="-1.6" x2="-1.85" y2="-1.6" width="0.05" layer="39"/>
<wire x1="-1.85" y1="-1.6" x2="-1.85" y2="1.6" width="0.05" layer="39"/>
<text x="-2.413" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.413" y="-3.048" size="1.27" layer="27">&gt;VALUE</text>
<smd name="2" x="-1.2625" y="0.25" dx="0.675" dy="0.25" layer="1"/>
<smd name="1" x="-1.2625" y="0.75" dx="0.675" dy="0.25" layer="1"/>
<smd name="3" x="-1.2625" y="-0.25" dx="0.675" dy="0.25" layer="1"/>
<smd name="4" x="-1.2625" y="-0.75" dx="0.675" dy="0.25" layer="1"/>
<smd name="11" x="1.2625" y="0.75" dx="0.675" dy="0.25" layer="1"/>
<smd name="10" x="1.2625" y="0.25" dx="0.675" dy="0.25" layer="1"/>
<smd name="9" x="1.2625" y="-0.25" dx="0.675" dy="0.25" layer="1"/>
<smd name="8" x="1.2625" y="-0.75" dx="0.675" dy="0.25" layer="1"/>
<smd name="14" x="-0.5" y="1.0125" dx="0.675" dy="0.25" layer="1" rot="R90"/>
<smd name="13" x="0" y="1.0125" dx="0.675" dy="0.25" layer="1" rot="R90"/>
<smd name="12" x="0.5" y="1.0125" dx="0.675" dy="0.25" layer="1" rot="R90"/>
<smd name="5" x="-0.5" y="-1.0125" dx="0.675" dy="0.25" layer="1" rot="R90"/>
<smd name="6" x="0" y="-1.0125" dx="0.675" dy="0.25" layer="1" rot="R90"/>
<smd name="7" x="0.5" y="-1.0125" dx="0.675" dy="0.25" layer="1" rot="R90"/>
</package>
</packages>
<symbols>
<symbol name="SIM39EA">
<wire x1="12.7" y1="10.16" x2="12.7" y2="-10.16" width="0.254" layer="94"/>
<wire x1="12.7" y1="-10.16" x2="-12.7" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-10.16" x2="-12.7" y2="10.16" width="0.254" layer="94"/>
<wire x1="-12.7" y1="10.16" x2="12.7" y2="10.16" width="0.254" layer="94"/>
<text x="-12.7151" y="10.1721" size="2.543009375" layer="95">&gt;NAME</text>
<text x="-12.7084" y="-12.7084" size="2.541690625" layer="96">&gt;VALUE</text>
<pin name="VCC" x="17.78" y="7.62" length="middle" direction="pwr" rot="R180"/>
<pin name="V_BACKUP" x="-17.78" y="-2.54" length="middle"/>
<pin name="GND" x="17.78" y="-7.62" length="middle" direction="pas" rot="R180"/>
<pin name="TXD" x="17.78" y="2.54" length="middle" direction="out" rot="R180"/>
<pin name="RXD" x="-17.78" y="2.54" length="middle" direction="in"/>
<pin name="TIMEMARK" x="17.78" y="0" length="middle" direction="out" rot="R180"/>
<pin name="3D-FIX" x="17.78" y="-2.54" length="middle" direction="out" rot="R180"/>
</symbol>
<symbol name="GRM033R61C104KE14D">
<wire x1="5.588" y1="2.54" x2="5.588" y2="-2.54" width="0.254" layer="94"/>
<wire x1="7.112" y1="2.54" x2="7.112" y2="-2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="5.588" y2="0" width="0.254" layer="94"/>
<wire x1="7.112" y1="0" x2="7.62" y2="0" width="0.254" layer="94"/>
<text x="8.89" y="6.35" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="8.89" y="3.81" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="1" x="0" y="0" visible="pad" length="middle"/>
<pin name="2" x="12.7" y="0" visible="pad" length="middle" rot="R180"/>
</symbol>
<symbol name="ADXL377BCPZ-RL7">
<wire x1="5.08" y1="20.32" x2="25.4" y2="20.32" width="0.254" layer="94"/>
<wire x1="25.4" y1="-20.32" x2="25.4" y2="20.32" width="0.254" layer="94"/>
<wire x1="25.4" y1="-20.32" x2="5.08" y2="-20.32" width="0.254" layer="94"/>
<wire x1="5.08" y1="20.32" x2="5.08" y2="-20.32" width="0.254" layer="94"/>
<text x="26.67" y="25.4" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="26.67" y="22.86" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="RES_1" x="0" y="0" length="middle"/>
<pin name="ST" x="0" y="-2.54" length="middle"/>
<pin name="RES_2" x="0" y="-5.08" length="middle"/>
<pin name="YOUT" x="0" y="-7.62" length="middle"/>
<pin name="XOUT" x="10.16" y="-25.4" length="middle" rot="R90"/>
<pin name="GND_1" x="12.7" y="-25.4" length="middle" rot="R90"/>
<pin name="GND_2" x="15.24" y="-25.4" length="middle" rot="R90"/>
<pin name="NC_1" x="17.78" y="-25.4" length="middle" rot="R90"/>
<pin name="NC_5" x="30.48" y="0" length="middle" rot="R180"/>
<pin name="NC_4" x="30.48" y="-2.54" length="middle" rot="R180"/>
<pin name="NC_3" x="30.48" y="-5.08" length="middle" rot="R180"/>
<pin name="NC_2" x="30.48" y="-7.62" length="middle" rot="R180"/>
<pin name="THERMAL_PAD" x="10.16" y="25.4" length="middle" rot="R270"/>
<pin name="ZOUT" x="12.7" y="25.4" length="middle" rot="R270"/>
<pin name="VS_2" x="15.24" y="25.4" length="middle" rot="R270"/>
<pin name="VS_1" x="17.78" y="25.4" length="middle" rot="R270"/>
<pin name="NC_6" x="20.32" y="25.4" length="middle" rot="R270"/>
</symbol>
<symbol name="504622-2012">
<wire x1="5.08" y1="2.54" x2="20.32" y2="2.54" width="0.254" layer="94"/>
<wire x1="20.32" y1="-30.48" x2="20.32" y2="2.54" width="0.254" layer="94"/>
<wire x1="20.32" y1="-30.48" x2="5.08" y2="-30.48" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-30.48" width="0.254" layer="94"/>
<text x="21.59" y="7.62" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="21.59" y="5.08" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="1" x="0" y="-2.54" length="middle"/>
<pin name="2" x="25.4" y="-2.54" length="middle" rot="R180"/>
<pin name="3" x="0" y="-5.08" length="middle"/>
<pin name="4" x="25.4" y="-5.08" length="middle" rot="R180"/>
<pin name="5" x="0" y="-7.62" length="middle"/>
<pin name="6" x="25.4" y="-7.62" length="middle" rot="R180"/>
<pin name="7" x="0" y="-10.16" length="middle"/>
<pin name="8" x="25.4" y="-10.16" length="middle" rot="R180"/>
<pin name="9" x="0" y="-12.7" length="middle"/>
<pin name="10" x="25.4" y="-12.7" length="middle" rot="R180"/>
<pin name="11" x="0" y="-15.24" length="middle"/>
<pin name="12" x="25.4" y="-15.24" length="middle" rot="R180"/>
<pin name="13" x="0" y="-17.78" length="middle"/>
<pin name="14" x="25.4" y="-17.78" length="middle" rot="R180"/>
<pin name="15" x="0" y="-20.32" length="middle"/>
<pin name="16" x="25.4" y="-20.32" length="middle" rot="R180"/>
<pin name="17" x="0" y="-22.86" length="middle"/>
<pin name="18" x="25.4" y="-22.86" length="middle" rot="R180"/>
<pin name="19" x="0" y="-25.4" length="middle"/>
<pin name="20" x="25.4" y="-25.4" length="middle" rot="R180"/>
<pin name="MP1" x="0" y="0" length="middle"/>
<pin name="MP2" x="25.4" y="0" length="middle" rot="R180"/>
<pin name="MP3" x="0" y="-27.94" length="middle"/>
<pin name="MP4" x="25.4" y="-27.94" length="middle" rot="R180"/>
</symbol>
<symbol name="BMG250">
<wire x1="-10.16" y1="20.32" x2="10.16" y2="20.32" width="0.254" layer="94"/>
<wire x1="10.16" y1="20.32" x2="10.16" y2="-20.32" width="0.254" layer="94"/>
<wire x1="10.16" y1="-20.32" x2="-10.16" y2="-20.32" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-20.32" x2="-10.16" y2="20.32" width="0.254" layer="94"/>
<text x="-10.16" y="21.082" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="-22.86" size="1.778" layer="96">&gt;VALUE</text>
<pin name="SDO" x="-15.24" y="-10.16" length="middle"/>
<pin name="ASDX" x="-15.24" y="7.62" length="middle"/>
<pin name="ASCX" x="-15.24" y="10.16" length="middle" direction="in"/>
<pin name="INT1" x="15.24" y="-2.54" length="middle" rot="R180"/>
<pin name="VDDIO" x="15.24" y="15.24" length="middle" direction="pwr" rot="R180"/>
<pin name="GNDIO" x="15.24" y="-15.24" length="middle" direction="pwr" rot="R180"/>
<pin name="GND" x="15.24" y="-17.78" length="middle" direction="pwr" rot="R180"/>
<pin name="VDD" x="15.24" y="17.78" length="middle" direction="pwr" rot="R180"/>
<pin name="OSCB" x="-15.24" y="5.08" length="middle"/>
<pin name="OSCO" x="-15.24" y="2.54" length="middle"/>
<pin name="CSB" x="-15.24" y="-2.54" length="middle" direction="in"/>
<pin name="INT2" x="15.24" y="-5.08" length="middle" rot="R180"/>
<pin name="SCX" x="-15.24" y="-5.08" length="middle" direction="in"/>
<pin name="SDX" x="-15.24" y="-7.62" length="middle"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SIM39EA" prefix="U">
<description>Ic Gps Accompany With Patch Antenna</description>
<gates>
<gate name="G$1" symbol="SIM39EA" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DFN1600X1600X750-13N">
<connects>
<connect gate="G$1" pin="3D-FIX" pad="5"/>
<connect gate="G$1" pin="GND" pad="3 8 11 12 13"/>
<connect gate="G$1" pin="RXD" pad="10"/>
<connect gate="G$1" pin="TIMEMARK" pad="6"/>
<connect gate="G$1" pin="TXD" pad="9"/>
<connect gate="G$1" pin="VCC" pad="1"/>
<connect gate="G$1" pin="V_BACKUP" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="AVAILABILITY" value="Unavailable"/>
<attribute name="DESCRIPTION" value=" Ic Gps Accompany With Patch Antenna "/>
<attribute name="MF" value="Simcom"/>
<attribute name="MP" value="SIM39EA"/>
<attribute name="PACKAGE" value="None"/>
<attribute name="PRICE" value="None"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GRM033R61C104KE14D" prefix="C">
<description>&lt;b&gt;0201 .1uF&lt;p&gt;Multilayer Ceramic Capacitors MLCC - SMD/SMT 16volts X5R 10%&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="https://componentsearchengine.com/Datasheets/1/GRM033R61C104KE14D.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="GRM033R61C104KE14D" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CAPC0603X33N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="Multilayer Ceramic Capacitors MLCC - SMD/SMT 0201 0.1uF 16volts X5R 10%" constant="no"/>
<attribute name="HEIGHT" value="0.33mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Murata Electronics" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="GRM033R61C104KE14D" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="81-GRM033R61C104KE4D" constant="no"/>
<attribute name="MOUSER_PRICE-STOCK" value="https://www.mouser.com/Search/Refine.aspx?Keyword=81-GRM033R61C104KE4D" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ADXL377BCPZ-RL7" prefix="AC">
<description>&lt;b&gt;Accelerometers 3-Axis High g Analog MEMS&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="https://componentsearchengine.com/Datasheets/1/ADXL377BCPZ-RL7.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="ADXL377BCPZ-RL7" x="0" y="0"/>
</gates>
<devices>
<device name="" package="QFN50P300X300X150-17N">
<connects>
<connect gate="G$1" pin="GND_1" pad="6"/>
<connect gate="G$1" pin="GND_2" pad="7"/>
<connect gate="G$1" pin="NC_1" pad="8"/>
<connect gate="G$1" pin="NC_2" pad="9"/>
<connect gate="G$1" pin="NC_3" pad="10"/>
<connect gate="G$1" pin="NC_4" pad="11"/>
<connect gate="G$1" pin="NC_5" pad="12"/>
<connect gate="G$1" pin="NC_6" pad="13"/>
<connect gate="G$1" pin="RES_1" pad="1"/>
<connect gate="G$1" pin="RES_2" pad="3"/>
<connect gate="G$1" pin="ST" pad="2"/>
<connect gate="G$1" pin="THERMAL_PAD" pad="17"/>
<connect gate="G$1" pin="VS_1" pad="14"/>
<connect gate="G$1" pin="VS_2" pad="15"/>
<connect gate="G$1" pin="XOUT" pad="5"/>
<connect gate="G$1" pin="YOUT" pad="4"/>
<connect gate="G$1" pin="ZOUT" pad="16"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="Accelerometers 3-Axis High g Analog MEMS" constant="no"/>
<attribute name="HEIGHT" value="1.5mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Analog Devices" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="ADXL377BCPZ-RL7" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="584-ADXL377BCPZ-R7" constant="no"/>
<attribute name="MOUSER_PRICE-STOCK" value="https://www.mouser.com/Search/Refine.aspx?Keyword=584-ADXL377BCPZ-R7" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="504622-2012" prefix="J">
<description>&lt;b&gt;SlimStack Board-to-Board Connector, 0.35mm Pitch, SSB6 Standard Series, Plug, 0.60mm Mated Height, 2.00mm Mated Width,  Circuits&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="https://componentsearchengine.com/Datasheets/1/504622-2012.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="504622-2012" x="0" y="0"/>
</gates>
<devices>
<device name="" package="5046222012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
<connect gate="G$1" pin="15" pad="15"/>
<connect gate="G$1" pin="16" pad="16"/>
<connect gate="G$1" pin="17" pad="17"/>
<connect gate="G$1" pin="18" pad="18"/>
<connect gate="G$1" pin="19" pad="19"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="20" pad="20"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
<connect gate="G$1" pin="MP1" pad="MP1"/>
<connect gate="G$1" pin="MP2" pad="MP2"/>
<connect gate="G$1" pin="MP3" pad="MP3"/>
<connect gate="G$1" pin="MP4" pad="MP4"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="SlimStack Board-to-Board Connector, 0.35mm Pitch, SSB6 Standard Series, Plug, 0.60mm Mated Height, 2.00mm Mated Width,  Circuits" constant="no"/>
<attribute name="HEIGHT" value="0.48mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Molex" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="504622-2012" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="538-504622-2012" constant="no"/>
<attribute name="MOUSER_PRICE-STOCK" value="https://www.mouser.com/Search/Refine.aspx?Keyword=538-504622-2012" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BMG250" prefix="U">
<description>BMG250 Series 3.6 V 3200 Hz 850 µA Low Noise Low Power Triaxial Gyroscope-LGA-14</description>
<gates>
<gate name="G$1" symbol="BMG250" x="0" y="0"/>
</gates>
<devices>
<device name="" package="XDCR_BMG250">
<connects>
<connect gate="G$1" pin="ASCX" pad="3"/>
<connect gate="G$1" pin="ASDX" pad="2"/>
<connect gate="G$1" pin="CSB" pad="12"/>
<connect gate="G$1" pin="GND" pad="7"/>
<connect gate="G$1" pin="GNDIO" pad="6"/>
<connect gate="G$1" pin="INT1" pad="4"/>
<connect gate="G$1" pin="INT2" pad="9"/>
<connect gate="G$1" pin="OSCB" pad="10"/>
<connect gate="G$1" pin="OSCO" pad="11"/>
<connect gate="G$1" pin="SCX" pad="13"/>
<connect gate="G$1" pin="SDO" pad="1"/>
<connect gate="G$1" pin="SDX" pad="14"/>
<connect gate="G$1" pin="VDD" pad="8"/>
<connect gate="G$1" pin="VDDIO" pad="5"/>
</connects>
<technologies>
<technology name="">
<attribute name="AVAILABILITY" value="Unavailable"/>
<attribute name="DESCRIPTION" value=" Gyroscope X _Pitch_, Y _Roll_, Z _Yaw_ ±125, 250, 500, 1000, 2000 25Hz ~ 3200Hz I²C, SPI "/>
<attribute name="MF" value="Bosch"/>
<attribute name="MP" value="BMG250"/>
<attribute name="PACKAGE" value="LGA-14 Bosch Tools"/>
<attribute name="PRICE" value="None"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U1" library="SamacSys_Parts" deviceset="SIM39EA" device=""/>
<part name="AC1" library="SamacSys_Parts" deviceset="ADXL377BCPZ-RL7" device="" value="ADXL377"/>
<part name="J1" library="SamacSys_Parts" deviceset="504622-2012" device=""/>
<part name="U3" library="SamacSys_Parts" deviceset="BMG250" device=""/>
<part name="C4" library="SamacSys_Parts" deviceset="GRM033R61C104KE14D" device=""/>
<part name="C6" library="SamacSys_Parts" deviceset="GRM033R61C104KE14D" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="U1" gate="G$1" x="88.9" y="119.38" smashed="yes">
<attribute name="NAME" x="76.1849" y="129.5521" size="2.543009375" layer="95"/>
<attribute name="VALUE" x="76.1916" y="106.6716" size="2.541690625" layer="96"/>
</instance>
<instance part="AC1" gate="G$1" x="129.54" y="66.04" smashed="yes">
<attribute name="NAME" x="156.21" y="91.44" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="156.21" y="88.9" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="J1" gate="G$1" x="-12.7" y="142.24" smashed="yes">
<attribute name="NAME" x="-1.27" y="147.32" size="1.778" layer="95" align="center-left"/>
</instance>
<instance part="U3" gate="G$1" x="43.18" y="60.96" smashed="yes">
<attribute name="NAME" x="33.02" y="82.042" size="1.778" layer="95"/>
<attribute name="VALUE" x="33.02" y="38.1" size="1.778" layer="96"/>
</instance>
<instance part="C4" gate="G$1" x="17.78" y="96.52" smashed="yes">
<attribute name="NAME" x="16.51" y="102.87" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="-6.35" y="100.33" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="C6" gate="G$1" x="76.2" y="71.12" smashed="yes" rot="R180">
<attribute name="NAME" x="77.47" y="64.77" size="1.778" layer="95" rot="R180" align="center-left"/>
<attribute name="VALUE" x="100.33" y="67.31" size="1.778" layer="96" rot="R180" align="center-left"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="VBAT" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="VCC"/>
<wire x1="106.68" y1="127" x2="111.76" y2="127" width="0.1524" layer="91"/>
<label x="111.76" y="127" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="2"/>
<wire x1="12.7" y1="139.7" x2="30.48" y2="139.7" width="0.1524" layer="91"/>
<label x="25.4" y="139.7" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="C4" gate="G$1" pin="2"/>
<wire x1="30.48" y1="96.52" x2="30.48" y2="104.14" width="0.1524" layer="91"/>
<label x="30.48" y="104.14" size="1.778" layer="95"/>
<pinref part="U3" gate="G$1" pin="CSB"/>
<wire x1="27.94" y1="58.42" x2="17.78" y2="58.42" width="0.1524" layer="91"/>
<wire x1="17.78" y1="58.42" x2="17.78" y2="86.36" width="0.1524" layer="91"/>
<wire x1="17.78" y1="86.36" x2="30.48" y2="86.36" width="0.1524" layer="91"/>
<wire x1="30.48" y1="86.36" x2="58.42" y2="86.36" width="0.1524" layer="91"/>
<wire x1="58.42" y1="86.36" x2="58.42" y2="78.74" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="VDD"/>
<wire x1="30.48" y1="96.52" x2="30.48" y2="86.36" width="0.1524" layer="91"/>
<junction x="30.48" y="96.52"/>
<junction x="30.48" y="86.36"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="VDDIO"/>
<wire x1="58.42" y1="76.2" x2="58.42" y2="71.12" width="0.1524" layer="91"/>
<wire x1="58.42" y1="71.12" x2="63.5" y2="71.12" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="2"/>
<junction x="63.5" y="71.12"/>
<wire x1="63.5" y1="71.12" x2="63.5" y2="63.5" width="0.1524" layer="91"/>
<label x="63.5" y="63.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="MC_UART_RX" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="TXD"/>
<wire x1="106.68" y1="121.92" x2="111.76" y2="121.92" width="0.1524" layer="91"/>
<label x="111.76" y="121.92" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPS_3D-FIX" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="3D-FIX"/>
<wire x1="106.68" y1="116.84" x2="111.76" y2="116.84" width="0.1524" layer="91"/>
<label x="111.76" y="116.84" size="1.778" layer="95"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="GND"/>
<wire x1="106.68" y1="111.76" x2="111.76" y2="111.76" width="0.1524" layer="91"/>
<label x="111.76" y="111.76" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="V_BACKUP"/>
<wire x1="71.12" y1="116.84" x2="66.04" y2="116.84" width="0.1524" layer="91"/>
<label x="60.96" y="116.84" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="TIMEMARK"/>
<wire x1="106.68" y1="119.38" x2="111.76" y2="119.38" width="0.1524" layer="91"/>
<label x="111.76" y="119.38" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="AC1" gate="G$1" pin="GND_1"/>
<wire x1="142.24" y1="40.64" x2="142.24" y2="27.94" width="0.1524" layer="91"/>
<label x="142.24" y="15.24" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="AC1" gate="G$1" pin="GND_2"/>
<wire x1="144.78" y1="40.64" x2="144.78" y2="27.94" width="0.1524" layer="91"/>
<label x="144.78" y="15.24" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="AC1" gate="G$1" pin="RES_1"/>
<wire x1="129.54" y1="66.04" x2="119.38" y2="66.04" width="0.1524" layer="91"/>
<label x="106.68" y="66.04" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="AC1" gate="G$1" pin="RES_2"/>
<wire x1="129.54" y1="60.96" x2="119.38" y2="60.96" width="0.1524" layer="91"/>
<label x="106.68" y="60.96" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-12.7" y1="139.7" x2="-30.48" y2="139.7" width="0.1524" layer="91"/>
<label x="-30.48" y="139.7" size="1.778" layer="95"/>
<pinref part="J1" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="60.96" y1="43.18" x2="60.96" y2="45.72" width="0.1524" layer="91"/>
<label x="73.66" y="43.18" size="1.778" layer="95"/>
<pinref part="U3" gate="G$1" pin="GNDIO"/>
<wire x1="58.42" y1="45.72" x2="60.96" y2="45.72" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="GND"/>
<wire x1="58.42" y1="43.18" x2="60.96" y2="43.18" width="0.1524" layer="91"/>
<wire x1="60.96" y1="43.18" x2="76.2" y2="43.18" width="0.1524" layer="91"/>
<junction x="60.96" y="43.18"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="ASCX"/>
<wire x1="27.94" y1="71.12" x2="20.32" y2="71.12" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="ASDX"/>
<wire x1="20.32" y1="71.12" x2="20.32" y2="68.58" width="0.1524" layer="91"/>
<wire x1="20.32" y1="68.58" x2="27.94" y2="68.58" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="OSCO"/>
<wire x1="27.94" y1="63.5" x2="20.32" y2="63.5" width="0.1524" layer="91"/>
<wire x1="20.32" y1="63.5" x2="20.32" y2="66.04" width="0.1524" layer="91"/>
<junction x="20.32" y="68.58"/>
<pinref part="U3" gate="G$1" pin="OSCB"/>
<wire x1="20.32" y1="66.04" x2="20.32" y2="68.58" width="0.1524" layer="91"/>
<wire x1="27.94" y1="66.04" x2="20.32" y2="66.04" width="0.1524" layer="91"/>
<junction x="20.32" y="66.04"/>
<label x="20.32" y="71.12" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="17.78" y1="96.52" x2="12.7" y2="96.52" width="0.1524" layer="91"/>
<label x="7.62" y="96.52" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="76.2" y1="71.12" x2="81.28" y2="71.12" width="0.1524" layer="91"/>
<label x="78.74" y="71.12" size="1.778" layer="95"/>
</segment>
</net>
<net name="MC_UART_TX" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="RXD"/>
<wire x1="71.12" y1="121.92" x2="66.04" y2="121.92" width="0.1524" layer="91"/>
<label x="48.26" y="121.92" size="1.778" layer="95"/>
</segment>
</net>
<net name="GYRO_I2C_LSB" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="SDO"/>
<wire x1="27.94" y1="50.8" x2="7.62" y2="50.8" width="0.1524" layer="91"/>
<label x="7.62" y="50.8" size="1.778" layer="95"/>
</segment>
</net>
<net name="I2C_MC_SCL" class="0">
<segment>
<wire x1="-12.7" y1="129.54" x2="-30.48" y2="129.54" width="0.1524" layer="91"/>
<label x="-30.48" y="129.54" size="1.778" layer="95"/>
<pinref part="J1" gate="G$1" pin="9"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="SCX"/>
<wire x1="27.94" y1="55.88" x2="7.62" y2="55.88" width="0.1524" layer="91"/>
<label x="7.62" y="55.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="I2C_MC_SDA" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="10"/>
<wire x1="12.7" y1="129.54" x2="30.48" y2="129.54" width="0.1524" layer="91"/>
<label x="15.24" y="129.54" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="SDX"/>
<wire x1="27.94" y1="53.34" x2="7.62" y2="53.34" width="0.1524" layer="91"/>
<label x="7.62" y="53.34" size="1.778" layer="95"/>
</segment>
</net>
<net name="INT" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="INT1"/>
<wire x1="58.42" y1="58.42" x2="68.58" y2="58.42" width="0.1524" layer="91"/>
<label x="66.04" y="58.42" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="INT2"/>
<wire x1="58.42" y1="55.88" x2="68.58" y2="55.88" width="0.1524" layer="91"/>
<label x="66.04" y="55.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="SNS_1_Z" class="0">
<segment>
<pinref part="AC1" gate="G$1" pin="ZOUT"/>
<wire x1="142.24" y1="91.44" x2="142.24" y2="99.06" width="0.1524" layer="91"/>
<label x="142.24" y="101.6" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="-12.7" y1="132.08" x2="-30.48" y2="132.08" width="0.1524" layer="91"/>
<label x="-30.48" y="132.08" size="1.778" layer="95"/>
<pinref part="J1" gate="G$1" pin="7"/>
</segment>
</net>
<net name="SNS_1_X" class="0">
<segment>
<pinref part="AC1" gate="G$1" pin="XOUT"/>
<wire x1="139.7" y1="40.64" x2="139.7" y2="27.94" width="0.1524" layer="91"/>
<label x="139.7" y="15.24" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="-12.7" y1="137.16" x2="-30.48" y2="137.16" width="0.1524" layer="91"/>
<label x="-30.48" y="137.16" size="1.778" layer="95"/>
<pinref part="J1" gate="G$1" pin="3"/>
</segment>
</net>
<net name="SNS_1_Y" class="0">
<segment>
<pinref part="AC1" gate="G$1" pin="YOUT"/>
<wire x1="129.54" y1="58.42" x2="119.38" y2="58.42" width="0.1524" layer="91"/>
<label x="106.68" y="58.42" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-12.7" y1="134.62" x2="-30.48" y2="134.62" width="0.1524" layer="91"/>
<label x="-30.48" y="134.62" size="1.778" layer="95"/>
<pinref part="J1" gate="G$1" pin="5"/>
</segment>
</net>
<net name="ACC_ST" class="0">
<segment>
<pinref part="AC1" gate="G$1" pin="ST"/>
<wire x1="129.54" y1="63.5" x2="119.38" y2="63.5" width="0.1524" layer="91"/>
<label x="106.68" y="63.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="VCC_3.3" class="0">
<segment>
<pinref part="AC1" gate="G$1" pin="VS_1"/>
<wire x1="147.32" y1="91.44" x2="147.32" y2="99.06" width="0.1524" layer="91"/>
<label x="147.32" y="101.6" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="AC1" gate="G$1" pin="VS_2"/>
<wire x1="144.78" y1="91.44" x2="144.78" y2="99.06" width="0.1524" layer="91"/>
<label x="144.78" y="101.6" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="UART_MC_RX" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="4"/>
<wire x1="12.7" y1="137.16" x2="30.48" y2="137.16" width="0.1524" layer="91"/>
<label x="15.24" y="137.16" size="1.778" layer="95"/>
</segment>
</net>
<net name="UART_MC_TX" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="6"/>
<wire x1="12.7" y1="134.62" x2="30.48" y2="134.62" width="0.1524" layer="91"/>
<label x="15.24" y="134.62" size="1.778" layer="95"/>
</segment>
</net>
<net name="MC_CLK" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="12"/>
<wire x1="12.7" y1="127" x2="30.48" y2="127" width="0.1524" layer="91"/>
<label x="20.32" y="127" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<wire x1="-12.7" y1="127" x2="-30.48" y2="127" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="11"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<wire x1="-12.7" y1="124.46" x2="-30.48" y2="124.46" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="13"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<wire x1="-12.7" y1="121.92" x2="-30.48" y2="121.92" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="15"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<wire x1="-12.7" y1="119.38" x2="-30.48" y2="119.38" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="17"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<wire x1="-12.7" y1="116.84" x2="-30.48" y2="116.84" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="19"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="14"/>
<wire x1="12.7" y1="124.46" x2="30.48" y2="124.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="16"/>
<wire x1="12.7" y1="121.92" x2="30.48" y2="121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="18"/>
<wire x1="12.7" y1="119.38" x2="30.48" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="20"/>
<wire x1="12.7" y1="116.84" x2="30.48" y2="116.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GYRO_12C_LSB" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="8"/>
<wire x1="12.7" y1="132.08" x2="30.48" y2="132.08" width="0.1524" layer="91"/>
<label x="15.24" y="132.08" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
