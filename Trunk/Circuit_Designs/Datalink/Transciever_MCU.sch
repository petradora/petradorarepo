<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.5.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="SamacSys_Parts">
<packages>
<package name="INDC1006X60N">
<description>&lt;b&gt;0402 (1005) T=0.50.1mm&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-0.55" y="0" dx="0.7" dy="0.7" layer="1"/>
<smd name="2" x="0.55" y="0" dx="0.7" dy="0.7" layer="1"/>
<text x="0" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-1.15" y1="0.6" x2="1.15" y2="0.6" width="0.05" layer="51"/>
<wire x1="1.15" y1="0.6" x2="1.15" y2="-0.6" width="0.05" layer="51"/>
<wire x1="1.15" y1="-0.6" x2="-1.15" y2="-0.6" width="0.05" layer="51"/>
<wire x1="-1.15" y1="-0.6" x2="-1.15" y2="0.6" width="0.05" layer="51"/>
<wire x1="-0.5" y1="0.3" x2="0.5" y2="0.3" width="0.1" layer="51"/>
<wire x1="0.5" y1="0.3" x2="0.5" y2="-0.3" width="0.1" layer="51"/>
<wire x1="0.5" y1="-0.3" x2="-0.5" y2="-0.3" width="0.1" layer="51"/>
<wire x1="-0.5" y1="-0.3" x2="-0.5" y2="0.3" width="0.1" layer="51"/>
<wire x1="0" y1="0.2" x2="0" y2="-0.2" width="0.2" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="LQG15WZ82NG02D">
<wire x1="5.08" y1="0" x2="7.62" y2="0" width="0.254" layer="94" curve="-175.4"/>
<wire x1="7.62" y1="0" x2="10.16" y2="0" width="0.254" layer="94" curve="-175.4"/>
<wire x1="10.16" y1="0" x2="12.7" y2="0" width="0.254" layer="94" curve="-175.4"/>
<wire x1="12.7" y1="0" x2="15.24" y2="0" width="0.254" layer="94" curve="-175.4"/>
<text x="16.51" y="6.35" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="16.51" y="3.81" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="1" x="0" y="0" visible="pad" length="middle"/>
<pin name="2" x="20.32" y="0" visible="pad" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="LQG15WZ82NG02D" prefix="L">
<description>&lt;b&gt;LQG15WZ_02 Series Inductor 82nH +/-2% 0402 (1005)&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="https://componentsearchengine.com/Datasheets/1/LQG15WZ82NG02D.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="LQG15WZ82NG02D" x="0" y="0"/>
</gates>
<devices>
<device name="" package="INDC1006X60N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="LQG15WZ_02 Series Inductor 82nH +/-2% 0402 (1005)" constant="no"/>
<attribute name="HEIGHT" value="0.6mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Murata Electronics" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="LQG15WZ82NG02D" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="81-LQG15WZ82NG02D" constant="no"/>
<attribute name="MOUSER_PRICE-STOCK" value="https://www.mouser.com/Search/Refine.aspx?Keyword=81-LQG15WZ82NG02D" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="capacitor-wima" urn="urn:adsk.eagle:library:116">
<description>&lt;b&gt;WIMA Capacitors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="C10B4" urn="urn:adsk.eagle:footprint:5353/1" library_version="2">
<description>&lt;B&gt;MKS4&lt;/B&gt;, 13.4 x 4 mm, grid 10.16 mm</description>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="3.81" y1="0" x2="-2.286" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-6.096" y1="2.032" x2="6.096" y2="2.032" width="0.1524" layer="21"/>
<wire x1="6.604" y1="1.524" x2="6.604" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-2.032" x2="-6.096" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-1.524" x2="-6.604" y2="1.524" width="0.1524" layer="21"/>
<wire x1="6.096" y1="2.032" x2="6.604" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="-2.032" x2="6.604" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-1.524" x2="-6.096" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="1.524" x2="-6.096" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-5.08" y="0" drill="1.016" diameter="2.159" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" diameter="2.159" shape="octagon"/>
<text x="-3.429" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C10B5" urn="urn:adsk.eagle:footprint:5354/1" library_version="2">
<description>&lt;B&gt;MKS4&lt;/B&gt;, 13.4 x 5 mm, grid 10.16 mm</description>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="3.81" y1="0" x2="-2.286" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-6.096" y1="2.54" x2="6.096" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.604" y1="2.032" x2="6.604" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-2.54" x2="-6.096" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.032" x2="-6.604" y2="2.032" width="0.1524" layer="21"/>
<wire x1="6.096" y1="2.54" x2="6.604" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="-2.54" x2="6.604" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-2.032" x2="-6.096" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="2.032" x2="-6.096" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-5.08" y="0" drill="1.016" diameter="2.159" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" diameter="2.159" shape="octagon"/>
<text x="-5.08" y="2.794" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-1.905" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C10B6" urn="urn:adsk.eagle:footprint:5355/1" library_version="2">
<description>&lt;B&gt;MKS4&lt;/B&gt;, 13.4 x 6 mm, grid 10.16 mm</description>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="3.81" y1="0" x2="-2.286" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-6.096" y1="3.048" x2="6.096" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.604" y1="2.54" x2="6.604" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-3.048" x2="-6.096" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.54" x2="-6.604" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.096" y1="3.048" x2="6.604" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="-3.048" x2="6.604" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-2.54" x2="-6.096" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="2.54" x2="-6.096" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-5.08" y="0" drill="1.016" diameter="2.159" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" diameter="2.159" shape="octagon"/>
<text x="-5.08" y="3.302" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C15B5" urn="urn:adsk.eagle:footprint:5356/1" library_version="2">
<description>&lt;B&gt;MKS4&lt;/B&gt;, 18 x 5 mm, grid 15 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="2.032" x2="9.017" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-2.54" x2="-8.509" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-2.032" x2="-9.017" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="2.54" x2="8.509" y2="2.54" width="0.1524" layer="21"/>
<wire x1="8.509" y1="2.54" x2="9.017" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-2.54" x2="9.017" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-2.032" x2="-8.509" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="2.032" x2="-8.509" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" diameter="2.159" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" diameter="2.159" shape="octagon"/>
<text x="-7.493" y="2.794" size="1.397" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.032" size="1.397" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C15B6" urn="urn:adsk.eagle:footprint:5357/1" library_version="2">
<description>&lt;B&gt;MKS4&lt;/B&gt;, 18 x 6 mm, grid 15 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="2.54" x2="9.017" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-3.048" x2="-8.509" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-2.54" x2="-9.017" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="3.048" x2="8.509" y2="3.048" width="0.1524" layer="21"/>
<wire x1="8.509" y1="3.048" x2="9.017" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-3.048" x2="9.017" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-2.54" x2="-8.509" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="2.54" x2="-8.509" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" diameter="2.159" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" diameter="2.159" shape="octagon"/>
<text x="-7.493" y="3.302" size="1.397" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.032" size="1.397" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C15B7" urn="urn:adsk.eagle:footprint:5358/1" library_version="2">
<description>&lt;B&gt;MKS4&lt;/B&gt;, 18 x 7 mm, grid 15 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="3.048" x2="9.017" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-3.556" x2="-8.509" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-3.048" x2="-9.017" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="3.556" x2="8.509" y2="3.556" width="0.1524" layer="21"/>
<wire x1="8.509" y1="3.556" x2="9.017" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-3.556" x2="9.017" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-3.048" x2="-8.509" y2="-3.556" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="3.048" x2="-8.509" y2="3.556" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" diameter="2.159" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" diameter="2.159" shape="octagon"/>
<text x="-7.493" y="3.81" size="1.397" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.286" size="1.397" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C15B8" urn="urn:adsk.eagle:footprint:5359/1" library_version="2">
<description>&lt;B&gt;MKS4&lt;/B&gt;, 18 x 8 mm, grid 15 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="3.556" x2="9.017" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-4.064" x2="-8.509" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-3.556" x2="-9.017" y2="3.556" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="4.064" x2="8.509" y2="4.064" width="0.1524" layer="21"/>
<wire x1="8.509" y1="4.064" x2="9.017" y2="3.556" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-4.064" x2="9.017" y2="-3.556" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-3.556" x2="-8.509" y2="-4.064" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="3.556" x2="-8.509" y2="4.064" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" diameter="2.159" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" diameter="2.159" shape="octagon"/>
<text x="-7.493" y="4.318" size="1.397" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.54" size="1.397" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C15B9" urn="urn:adsk.eagle:footprint:5360/1" library_version="2">
<description>&lt;B&gt;MKS4&lt;/B&gt;, 18 x 9 mm, grid 15 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="3.937" x2="9.017" y2="-3.937" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-4.445" x2="-8.509" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-3.937" x2="-9.017" y2="3.937" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="4.445" x2="8.509" y2="4.445" width="0.1524" layer="21"/>
<wire x1="8.509" y1="4.445" x2="9.017" y2="3.937" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-4.445" x2="9.017" y2="-3.937" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-3.937" x2="-8.509" y2="-4.445" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="3.937" x2="-8.509" y2="4.445" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" diameter="2.159" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" diameter="2.159" shape="octagon"/>
<text x="-7.493" y="4.699" size="1.397" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.54" size="1.397" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C2.5-2" urn="urn:adsk.eagle:footprint:5361/1" library_version="2">
<description>&lt;B&gt;MKS2&lt;/B&gt;, 5 x 2.5 mm, grid 2.54 mm</description>
<wire x1="-2.159" y1="1.27" x2="2.159" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.27" x2="-2.159" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.27" x2="2.413" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.016" x2="-2.159" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.27" x2="2.413" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.016" x2="-2.159" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="1.27" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-1.27" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" diameter="1.6002" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" diameter="1.6002" shape="octagon"/>
<text x="-1.651" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.651" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C2.5-4" urn="urn:adsk.eagle:footprint:5362/1" library_version="2">
<description>&lt;B&gt;MKS2&lt;/B&gt;, 5 x 4 mm, grid 2.54 mm</description>
<wire x1="-2.159" y1="1.905" x2="2.159" y2="1.905" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.905" x2="-2.159" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.651" x2="2.413" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.651" x2="-2.413" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.905" x2="2.413" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.651" x2="-2.159" y2="1.905" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.905" x2="2.413" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.651" x2="-2.159" y2="-1.905" width="0.1524" layer="21" curve="90"/>
<wire x1="1.27" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-1.27" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" diameter="1.6002" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" diameter="1.6002" shape="octagon"/>
<text x="-1.651" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.651" y="-3.429" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C2.5-5" urn="urn:adsk.eagle:footprint:5363/1" library_version="2">
<description>&lt;B&gt;MKS2&lt;/B&gt;, 5 x 5 mm, grid 2.54 mm</description>
<wire x1="-2.159" y1="2.286" x2="2.159" y2="2.286" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.286" x2="-2.159" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.032" x2="2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.032" x2="-2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.286" x2="2.413" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.032" x2="-2.159" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.286" x2="2.413" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.032" x2="-2.159" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="1.27" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-1.27" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" diameter="1.6002" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" diameter="1.6002" shape="octagon"/>
<text x="-1.778" y="2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.778" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C2.5-6" urn="urn:adsk.eagle:footprint:5364/1" library_version="2">
<description>&lt;B&gt;MKS2&lt;/B&gt;, 5 x 6 mm, grid 2.54 mm</description>
<wire x1="-2.159" y1="2.794" x2="2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.794" x2="-2.159" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.54" x2="2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.54" x2="-2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.794" x2="2.413" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.54" x2="-2.159" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.794" x2="2.413" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.54" x2="-2.159" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="1.27" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-1.27" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" diameter="1.6002" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" diameter="1.6002" shape="octagon"/>
<text x="2.667" y="0.762" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.905" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C22.5B10" urn="urn:adsk.eagle:footprint:5365/1" library_version="2">
<description>&lt;B&gt;MKS4&lt;/B&gt;, 27 x 10 mm, grid 22.5 mm</description>
<wire x1="-12.827" y1="5.334" x2="12.827" y2="5.334" width="0.1524" layer="21"/>
<wire x1="13.335" y1="4.826" x2="13.335" y2="-4.826" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-5.334" x2="-12.827" y2="-5.334" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-4.826" x2="-13.335" y2="4.826" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="5.334" x2="13.335" y2="4.826" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-5.334" x2="13.335" y2="-4.826" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-4.826" x2="-12.827" y2="-5.334" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="4.826" x2="-12.827" y2="5.334" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<text x="-11.303" y="5.588" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C22.5B11" urn="urn:adsk.eagle:footprint:5366/1" library_version="2">
<description>&lt;B&gt;MKS4&lt;/B&gt;, 27 x 11 mm, grid 22.5 mm</description>
<wire x1="-12.827" y1="5.588" x2="12.827" y2="5.588" width="0.1524" layer="21"/>
<wire x1="13.335" y1="5.08" x2="13.335" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-5.588" x2="-12.827" y2="-5.588" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-5.08" x2="-13.335" y2="5.08" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="5.588" x2="13.335" y2="5.08" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-5.588" x2="13.335" y2="-5.08" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-5.08" x2="-12.827" y2="-5.588" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="5.08" x2="-12.827" y2="5.588" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<text x="-11.303" y="5.842" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C22.5B6" urn="urn:adsk.eagle:footprint:5367/1" library_version="2">
<description>&lt;B&gt;MKS4&lt;/B&gt;, 27 x 6 mm, grid 22.5 mm</description>
<wire x1="-12.827" y1="3.048" x2="12.827" y2="3.048" width="0.1524" layer="21"/>
<wire x1="13.335" y1="2.54" x2="13.335" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-3.048" x2="-12.827" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-2.54" x2="-13.335" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="3.048" x2="13.335" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-3.048" x2="13.335" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-2.54" x2="-12.827" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="2.54" x2="-12.827" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<text x="-11.303" y="3.302" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C22.5B7" urn="urn:adsk.eagle:footprint:5368/1" library_version="2">
<description>&lt;B&gt;MKS4&lt;/B&gt;, 27 x 7 mm, grid 22.5 mm</description>
<wire x1="-12.827" y1="3.556" x2="12.827" y2="3.556" width="0.1524" layer="21"/>
<wire x1="13.335" y1="3.048" x2="13.335" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-3.556" x2="-12.827" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-3.048" x2="-13.335" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="3.556" x2="13.335" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-3.556" x2="13.335" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-3.048" x2="-12.827" y2="-3.556" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="3.048" x2="-12.827" y2="3.556" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<text x="-11.303" y="3.81" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C22.5B8" urn="urn:adsk.eagle:footprint:5369/1" library_version="2">
<description>&lt;B&gt;MKS4&lt;/B&gt;, 27 x 8 mm, grid 22.5 mm</description>
<wire x1="-12.827" y1="4.318" x2="12.827" y2="4.318" width="0.1524" layer="21"/>
<wire x1="13.335" y1="3.81" x2="13.335" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-4.318" x2="-12.827" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-3.81" x2="-13.335" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="4.318" x2="13.335" y2="3.81" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-4.318" x2="13.335" y2="-3.81" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-3.81" x2="-12.827" y2="-4.318" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="3.81" x2="-12.827" y2="4.318" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<text x="-11.303" y="4.572" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C27.5B11" urn="urn:adsk.eagle:footprint:5370/1" library_version="2">
<description>&lt;B&gt;MKS4&lt;/B&gt;, 31.6 x 11 mm, grid 27.5 mm</description>
<wire x1="-15.24" y1="5.588" x2="15.24" y2="5.588" width="0.1524" layer="21"/>
<wire x1="15.748" y1="5.08" x2="15.748" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-5.588" x2="-15.24" y2="-5.588" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-5.08" x2="-15.748" y2="5.08" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="5.588" x2="15.748" y2="5.08" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-5.588" x2="15.748" y2="-5.08" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-5.08" x2="-15.24" y2="-5.588" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="5.08" x2="-15.24" y2="5.588" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" diameter="3.1496" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" diameter="3.1496" shape="octagon"/>
<text x="-13.716" y="5.842" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C27.5B13" urn="urn:adsk.eagle:footprint:5371/1" library_version="2">
<description>&lt;B&gt;MKS4&lt;/B&gt;, 31.6 x 13 mm, grid 27.5 mm</description>
<wire x1="-15.24" y1="6.604" x2="15.24" y2="6.604" width="0.1524" layer="21"/>
<wire x1="15.748" y1="6.096" x2="15.748" y2="-6.096" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-6.604" x2="-15.24" y2="-6.604" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-6.096" x2="-15.748" y2="6.096" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="6.604" x2="15.748" y2="6.096" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-6.604" x2="15.748" y2="-6.096" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-6.096" x2="-15.24" y2="-6.604" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="6.096" x2="-15.24" y2="6.604" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" diameter="3.1496" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" diameter="3.1496" shape="octagon"/>
<text x="-13.716" y="6.858" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C27.5B15" urn="urn:adsk.eagle:footprint:5372/1" library_version="2">
<description>&lt;B&gt;MKS4&lt;/B&gt;, 31.6 x 15 mm, grid 27.5 mm</description>
<wire x1="-15.24" y1="7.62" x2="15.24" y2="7.62" width="0.1524" layer="21"/>
<wire x1="15.748" y1="7.112" x2="15.748" y2="-7.112" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-7.62" x2="-15.24" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-7.112" x2="-15.748" y2="7.112" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="7.62" x2="15.748" y2="7.112" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-7.62" x2="15.748" y2="-7.112" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-7.112" x2="-15.24" y2="-7.62" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="7.112" x2="-15.24" y2="7.62" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" diameter="3.1496" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" diameter="3.1496" shape="octagon"/>
<text x="-13.716" y="7.874" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C27.5B17" urn="urn:adsk.eagle:footprint:5373/1" library_version="2">
<description>&lt;B&gt;MKS4&lt;/B&gt;, 31.6 x 17 mm, grid 27.5 mm</description>
<wire x1="-15.24" y1="8.509" x2="15.24" y2="8.509" width="0.1524" layer="21"/>
<wire x1="15.748" y1="8.001" x2="15.748" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-8.509" x2="-15.24" y2="-8.509" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-8.001" x2="-15.748" y2="8.001" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="8.509" x2="15.748" y2="8.001" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-8.509" x2="15.748" y2="-8.001" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-8.001" x2="-15.24" y2="-8.509" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="8.001" x2="-15.24" y2="8.509" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" diameter="3.1496" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" diameter="3.1496" shape="octagon"/>
<text x="-13.716" y="8.763" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C27.5B20" urn="urn:adsk.eagle:footprint:5374/1" library_version="2">
<description>&lt;B&gt;MKS4&lt;/B&gt;, 31.6 x 20 mm, grid 27.5 mm</description>
<wire x1="-15.24" y1="10.16" x2="15.24" y2="10.16" width="0.1524" layer="21"/>
<wire x1="15.748" y1="9.652" x2="15.748" y2="-9.652" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-10.16" x2="-15.24" y2="-10.16" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-9.652" x2="-15.748" y2="9.652" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="10.16" x2="15.748" y2="9.652" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-10.16" x2="15.748" y2="-9.652" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-9.652" x2="-15.24" y2="-10.16" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="9.652" x2="-15.24" y2="10.16" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" diameter="3.1496" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" diameter="3.1496" shape="octagon"/>
<text x="-13.589" y="10.414" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-4.318" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C27.5B9" urn="urn:adsk.eagle:footprint:5375/1" library_version="2">
<description>&lt;B&gt;MKS4&lt;/B&gt;, 31.6 x 9 mm, grid 27.5 mm</description>
<wire x1="-15.24" y1="4.572" x2="15.24" y2="4.572" width="0.1524" layer="21"/>
<wire x1="15.748" y1="4.064" x2="15.748" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-4.572" x2="-15.24" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-4.064" x2="-15.748" y2="4.064" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="4.572" x2="15.748" y2="4.064" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-4.572" x2="15.748" y2="-4.064" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-4.064" x2="-15.24" y2="-4.572" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="4.064" x2="-15.24" y2="4.572" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" diameter="3.1496" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" diameter="3.1496" shape="octagon"/>
<text x="-13.589" y="4.826" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C37.5B15" urn="urn:adsk.eagle:footprint:5376/1" library_version="2">
<description>&lt;B&gt;MKP4&lt;/B&gt;, 42 x 15 mm, grid 37.5 mm</description>
<wire x1="-20.32" y1="7.62" x2="20.32" y2="7.62" width="0.1524" layer="21"/>
<wire x1="20.828" y1="7.112" x2="20.828" y2="-7.112" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-7.62" x2="-20.32" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="-20.828" y1="-7.112" x2="-20.828" y2="7.112" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="20.32" y1="7.62" x2="20.828" y2="7.112" width="0.1524" layer="21" curve="-90"/>
<wire x1="20.32" y1="-7.62" x2="20.828" y2="-7.112" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="-7.112" x2="-20.32" y2="-7.62" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="7.112" x2="-20.32" y2="7.62" width="0.1524" layer="21" curve="-90"/>
<wire x1="-16.002" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="16.002" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-18.796" y="0" drill="1.3208" diameter="3.1496" shape="octagon"/>
<pad name="2" x="18.796" y="0" drill="1.3208" diameter="3.1496" shape="octagon"/>
<text x="-18.796" y="7.874" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C37.5B19" urn="urn:adsk.eagle:footprint:5377/1" library_version="2">
<description>&lt;B&gt;MKP4&lt;/B&gt;, 42 x 19 mm, grid 37.5 mm</description>
<wire x1="-20.32" y1="8.509" x2="20.32" y2="8.509" width="0.1524" layer="21"/>
<wire x1="20.828" y1="8.001" x2="20.828" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-8.509" x2="-20.32" y2="-8.509" width="0.1524" layer="21"/>
<wire x1="-20.828" y1="-8.001" x2="-20.828" y2="8.001" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="20.32" y1="8.509" x2="20.828" y2="8.001" width="0.1524" layer="21" curve="-90"/>
<wire x1="20.32" y1="-8.509" x2="20.828" y2="-8.001" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="-8.001" x2="-20.32" y2="-8.509" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="8.001" x2="-20.32" y2="8.509" width="0.1524" layer="21" curve="-90"/>
<wire x1="-16.002" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="16.002" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-18.796" y="0" drill="1.3208" diameter="3.1496" shape="octagon"/>
<pad name="2" x="18.796" y="0" drill="1.3208" diameter="3.1496" shape="octagon"/>
<text x="-18.796" y="8.89" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C37.5B20" urn="urn:adsk.eagle:footprint:5378/1" library_version="2">
<description>&lt;B&gt;MKP4&lt;/B&gt;, 42 x 20 mm, grid 37.5 mm</description>
<wire x1="-20.32" y1="10.16" x2="20.32" y2="10.16" width="0.1524" layer="21"/>
<wire x1="20.828" y1="9.652" x2="20.828" y2="-9.652" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-10.16" x2="-20.32" y2="-10.16" width="0.1524" layer="21"/>
<wire x1="-20.828" y1="-9.652" x2="-20.828" y2="9.652" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="20.32" y1="10.16" x2="20.828" y2="9.652" width="0.1524" layer="21" curve="-90"/>
<wire x1="20.32" y1="-10.16" x2="20.828" y2="-9.652" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="-9.652" x2="-20.32" y2="-10.16" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="9.652" x2="-20.32" y2="10.16" width="0.1524" layer="21" curve="-90"/>
<wire x1="-16.002" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="16.002" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-18.796" y="0" drill="1.3208" diameter="3.1496" shape="octagon"/>
<pad name="2" x="18.796" y="0" drill="1.3208" diameter="3.1496" shape="octagon"/>
<text x="-18.796" y="10.414" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C5B2.5" urn="urn:adsk.eagle:footprint:5379/1" library_version="2">
<description>&lt;B&gt;MKS2&lt;/B&gt;, 7.5 x 2.5 mm, grid 5.08 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="1.016" x2="-3.683" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.27" x2="3.429" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-1.016" x2="3.683" y2="1.016" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.27" x2="-3.429" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.27" x2="3.683" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-1.27" x2="3.683" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-1.016" x2="-3.429" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="1.016" x2="-3.429" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" diameter="1.6002" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" diameter="1.6002" shape="octagon"/>
<text x="-2.032" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.032" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C5B3" urn="urn:adsk.eagle:footprint:5380/1" library_version="2">
<description>&lt;B&gt;MKS2&lt;/B&gt;, 7.5 x 3 mm, grid 5.08 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="1.27" x2="-3.683" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-1.27" x2="3.683" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.524" x2="3.683" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-1.524" x2="3.683" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-1.27" x2="-3.429" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="1.27" x2="-3.429" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" diameter="1.6002" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" diameter="1.6002" shape="octagon"/>
<text x="-2.54" y="1.778" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-3.048" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C5B3.5" urn="urn:adsk.eagle:footprint:5381/1" library_version="2">
<description>&lt;B&gt;MKS2&lt;/B&gt;, 7.5 x 4 mm, grid 5.08 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="1.524" x2="-3.683" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.778" x2="3.429" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-1.524" x2="3.683" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.778" x2="-3.429" y2="1.778" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.778" x2="3.683" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-1.778" x2="3.683" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-1.524" x2="-3.429" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="1.524" x2="-3.429" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" diameter="1.6002" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" diameter="1.6002" shape="octagon"/>
<text x="-2.54" y="2.032" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-3.302" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C5B4.5" urn="urn:adsk.eagle:footprint:5382/1" library_version="2">
<description>&lt;B&gt;MKS2&lt;/B&gt;, 7.5 x 4.5 mm, grid 5.08 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.032" x2="-3.683" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-2.286" x2="3.429" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.032" x2="3.683" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.286" x2="-3.429" y2="2.286" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.286" x2="3.683" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-2.286" x2="3.683" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-2.032" x2="-3.429" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="2.032" x2="-3.429" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" diameter="1.6002" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" diameter="1.6002" shape="octagon"/>
<text x="-2.54" y="2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C5B5" urn="urn:adsk.eagle:footprint:5383/1" library_version="2">
<description>&lt;B&gt;MKS2&lt;/B&gt;, 7.5 x 5 mm, grid 5.08 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.286" x2="-3.683" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-2.54" x2="3.429" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.286" x2="3.683" y2="2.286" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.54" x2="-3.429" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.54" x2="3.683" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-2.54" x2="3.683" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-2.286" x2="-3.429" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="2.286" x2="-3.429" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" diameter="1.6002" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" diameter="1.6002" shape="octagon"/>
<text x="-2.54" y="2.794" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C5B5.5" urn="urn:adsk.eagle:footprint:5384/1" library_version="2">
<description>&lt;B&gt;MKS2&lt;/B&gt;, 7.5 x 5.5 mm, grid 5.08 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.54" x2="-3.683" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-2.794" x2="3.429" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.54" x2="3.683" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.794" x2="-3.429" y2="2.794" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.794" x2="3.683" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-2.794" x2="3.683" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-2.54" x2="-3.429" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="2.54" x2="-3.429" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" diameter="1.6002" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" diameter="1.6002" shape="octagon"/>
<text x="-2.54" y="3.048" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C5B7.2" urn="urn:adsk.eagle:footprint:5385/1" library_version="2">
<description>&lt;B&gt;MKS2&lt;/B&gt;, 7.5 x 7.2 mm, grid 5.08 mm</description>
<wire x1="-1.524" y1="0" x2="-0.4572" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.4572" y1="0" x2="-0.4572" y2="0.762" width="0.4064" layer="21"/>
<wire x1="-0.4572" y1="0" x2="-0.4572" y2="-0.762" width="0.4064" layer="21"/>
<wire x1="0.4318" y1="0.762" x2="0.4318" y2="0" width="0.4064" layer="21"/>
<wire x1="0.4318" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.4318" y1="0" x2="0.4318" y2="-0.762" width="0.4064" layer="21"/>
<wire x1="-3.683" y1="3.429" x2="-3.683" y2="-3.429" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-3.683" x2="3.429" y2="-3.683" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-3.429" x2="3.683" y2="3.429" width="0.1524" layer="21"/>
<wire x1="3.429" y1="3.683" x2="-3.429" y2="3.683" width="0.1524" layer="21"/>
<wire x1="3.429" y1="3.683" x2="3.683" y2="3.429" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-3.683" x2="3.683" y2="-3.429" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-3.429" x2="-3.429" y2="-3.683" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="3.429" x2="-3.429" y2="3.683" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" diameter="1.6002" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" diameter="1.6002" shape="octagon"/>
<text x="-2.54" y="4.064" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-2.921" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C7.5B3" urn="urn:adsk.eagle:footprint:5386/1" library_version="2">
<description>&lt;B&gt;MKS3&lt;/B&gt;, 10 x 3 mm, grid 7.62 mm</description>
<wire x1="4.826" y1="1.524" x2="-4.826" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-1.524" x2="4.826" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.826" y1="1.524" x2="5.08" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.826" y1="-1.524" x2="5.08" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="-1.27" x2="-4.826" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.27" x2="-4.826" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="0.508" y1="0" x2="2.54" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0" x2="-0.508" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="0.889" x2="-0.508" y2="0" width="0.4064" layer="21"/>
<wire x1="-0.508" y1="0" x2="-0.508" y2="-0.889" width="0.4064" layer="21"/>
<wire x1="0.508" y1="0.889" x2="0.508" y2="0" width="0.4064" layer="21"/>
<wire x1="0.508" y1="0" x2="0.508" y2="-0.889" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" diameter="1.905" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" diameter="1.905" shape="octagon"/>
<text x="-3.81" y="1.778" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-3.048" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C7.5B4" urn="urn:adsk.eagle:footprint:5387/1" library_version="2">
<description>&lt;B&gt;MKS3&lt;/B&gt;, 10 x 4 mm, grid 7.62 mm</description>
<wire x1="4.826" y1="2.032" x2="-4.826" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.778" x2="-5.08" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-2.032" x2="4.826" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.778" x2="5.08" y2="1.778" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="5.08" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.826" y1="-2.032" x2="5.08" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="-1.778" x2="-4.826" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.778" x2="-4.826" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="0" x2="2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="0" x2="-2.159" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.27" x2="-2.159" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.159" y1="0" x2="-2.159" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" diameter="1.905" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" diameter="1.905" shape="octagon"/>
<text x="-3.81" y="2.286" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-3.556" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C7.5B5" urn="urn:adsk.eagle:footprint:5388/1" library_version="2">
<description>&lt;B&gt;MKS3&lt;/B&gt;, 10 x 5 mm, grid 7.62 mm</description>
<wire x1="4.953" y1="2.54" x2="-4.953" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.286" x2="-5.207" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-2.54" x2="4.953" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.286" x2="5.207" y2="2.286" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.54" x2="5.207" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.953" y1="-2.54" x2="5.207" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="-2.286" x2="-4.953" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="2.286" x2="-4.953" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="0" x2="2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="0" x2="-2.159" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.27" x2="-2.159" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.159" y1="0" x2="-2.159" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" diameter="1.905" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" diameter="1.905" shape="octagon"/>
<text x="-3.81" y="2.794" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-4.064" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C7.5B6" urn="urn:adsk.eagle:footprint:5389/1" library_version="2">
<description>&lt;B&gt;MKS3&lt;/B&gt;, 10 x 6 mm, grid 7.62 mm</description>
<wire x1="4.953" y1="3.048" x2="-4.953" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.794" x2="-5.207" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-3.048" x2="4.953" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.794" x2="5.207" y2="2.794" width="0.1524" layer="21"/>
<wire x1="4.953" y1="3.048" x2="5.207" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.953" y1="-3.048" x2="5.207" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="-2.794" x2="-4.953" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="2.794" x2="-4.953" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="0" x2="2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="0" x2="-2.159" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.27" x2="-2.159" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.159" y1="0" x2="-2.159" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" diameter="1.905" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" diameter="1.905" shape="octagon"/>
<text x="-3.683" y="3.302" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.889" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C2.5-3" urn="urn:adsk.eagle:footprint:5390/1" library_version="2">
<description>&lt;B&gt;MKS2&lt;/B&gt;, 5 x 3 mm, grid 2.54 mm</description>
<wire x1="-2.159" y1="1.524" x2="2.159" y2="1.524" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.524" x2="-2.159" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.27" x2="2.413" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.27" x2="-2.413" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.524" x2="2.413" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.27" x2="-2.159" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.524" x2="2.413" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.27" x2="-2.159" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="1.27" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-1.27" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" diameter="1.6002" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" diameter="1.6002" shape="octagon"/>
<text x="-1.651" y="1.778" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.651" y="-3.048" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="C10B4" urn="urn:adsk.eagle:package:5400/1" type="box" library_version="2">
<description>MKS4, 13.4 x 4 mm, grid 10.16 mm</description>
<packageinstances>
<packageinstance name="C10B4"/>
</packageinstances>
</package3d>
<package3d name="C10B5" urn="urn:adsk.eagle:package:5399/1" type="box" library_version="2">
<description>MKS4, 13.4 x 5 mm, grid 10.16 mm</description>
<packageinstances>
<packageinstance name="C10B5"/>
</packageinstances>
</package3d>
<package3d name="C10B6" urn="urn:adsk.eagle:package:5401/1" type="box" library_version="2">
<description>MKS4, 13.4 x 6 mm, grid 10.16 mm</description>
<packageinstances>
<packageinstance name="C10B6"/>
</packageinstances>
</package3d>
<package3d name="C15B5" urn="urn:adsk.eagle:package:5402/1" type="box" library_version="2">
<description>MKS4, 18 x 5 mm, grid 15 mm</description>
<packageinstances>
<packageinstance name="C15B5"/>
</packageinstances>
</package3d>
<package3d name="C15B6" urn="urn:adsk.eagle:package:5403/1" type="box" library_version="2">
<description>MKS4, 18 x 6 mm, grid 15 mm</description>
<packageinstances>
<packageinstance name="C15B6"/>
</packageinstances>
</package3d>
<package3d name="C15B7" urn="urn:adsk.eagle:package:5404/1" type="box" library_version="2">
<description>MKS4, 18 x 7 mm, grid 15 mm</description>
<packageinstances>
<packageinstance name="C15B7"/>
</packageinstances>
</package3d>
<package3d name="C15B8" urn="urn:adsk.eagle:package:5409/1" type="box" library_version="2">
<description>MKS4, 18 x 8 mm, grid 15 mm</description>
<packageinstances>
<packageinstance name="C15B8"/>
</packageinstances>
</package3d>
<package3d name="C15B9" urn="urn:adsk.eagle:package:5405/1" type="box" library_version="2">
<description>MKS4, 18 x 9 mm, grid 15 mm</description>
<packageinstances>
<packageinstance name="C15B9"/>
</packageinstances>
</package3d>
<package3d name="C2.5-2" urn="urn:adsk.eagle:package:5415/1" type="box" library_version="2">
<description>MKS2, 5 x 2.5 mm, grid 2.54 mm</description>
<packageinstances>
<packageinstance name="C2.5-2"/>
</packageinstances>
</package3d>
<package3d name="C2.5-4" urn="urn:adsk.eagle:package:5408/1" type="box" library_version="2">
<description>MKS2, 5 x 4 mm, grid 2.54 mm</description>
<packageinstances>
<packageinstance name="C2.5-4"/>
</packageinstances>
</package3d>
<package3d name="C2.5-5" urn="urn:adsk.eagle:package:5407/1" type="box" library_version="2">
<description>MKS2, 5 x 5 mm, grid 2.54 mm</description>
<packageinstances>
<packageinstance name="C2.5-5"/>
</packageinstances>
</package3d>
<package3d name="C2.5-6" urn="urn:adsk.eagle:package:5406/1" type="box" library_version="2">
<description>MKS2, 5 x 6 mm, grid 2.54 mm</description>
<packageinstances>
<packageinstance name="C2.5-6"/>
</packageinstances>
</package3d>
<package3d name="C22.5B10" urn="urn:adsk.eagle:package:5411/1" type="box" library_version="2">
<description>MKS4, 27 x 10 mm, grid 22.5 mm</description>
<packageinstances>
<packageinstance name="C22.5B10"/>
</packageinstances>
</package3d>
<package3d name="C22.5B11" urn="urn:adsk.eagle:package:5412/1" type="box" library_version="2">
<description>MKS4, 27 x 11 mm, grid 22.5 mm</description>
<packageinstances>
<packageinstance name="C22.5B11"/>
</packageinstances>
</package3d>
<package3d name="C22.5B6" urn="urn:adsk.eagle:package:5410/1" type="box" library_version="2">
<description>MKS4, 27 x 6 mm, grid 22.5 mm</description>
<packageinstances>
<packageinstance name="C22.5B6"/>
</packageinstances>
</package3d>
<package3d name="C22.5B7" urn="urn:adsk.eagle:package:5414/1" type="box" library_version="2">
<description>MKS4, 27 x 7 mm, grid 22.5 mm</description>
<packageinstances>
<packageinstance name="C22.5B7"/>
</packageinstances>
</package3d>
<package3d name="C22.5B8" urn="urn:adsk.eagle:package:5413/1" type="box" library_version="2">
<description>MKS4, 27 x 8 mm, grid 22.5 mm</description>
<packageinstances>
<packageinstance name="C22.5B8"/>
</packageinstances>
</package3d>
<package3d name="C27.5B11" urn="urn:adsk.eagle:package:5416/1" type="box" library_version="2">
<description>MKS4, 31.6 x 11 mm, grid 27.5 mm</description>
<packageinstances>
<packageinstance name="C27.5B11"/>
</packageinstances>
</package3d>
<package3d name="C27.5B13" urn="urn:adsk.eagle:package:5420/1" type="box" library_version="2">
<description>MKS4, 31.6 x 13 mm, grid 27.5 mm</description>
<packageinstances>
<packageinstance name="C27.5B13"/>
</packageinstances>
</package3d>
<package3d name="C27.5B15" urn="urn:adsk.eagle:package:5417/1" type="box" library_version="2">
<description>MKS4, 31.6 x 15 mm, grid 27.5 mm</description>
<packageinstances>
<packageinstance name="C27.5B15"/>
</packageinstances>
</package3d>
<package3d name="C27.5B17" urn="urn:adsk.eagle:package:5418/1" type="box" library_version="2">
<description>MKS4, 31.6 x 17 mm, grid 27.5 mm</description>
<packageinstances>
<packageinstance name="C27.5B17"/>
</packageinstances>
</package3d>
<package3d name="C27.5B20" urn="urn:adsk.eagle:package:5421/1" type="box" library_version="2">
<description>MKS4, 31.6 x 20 mm, grid 27.5 mm</description>
<packageinstances>
<packageinstance name="C27.5B20"/>
</packageinstances>
</package3d>
<package3d name="C27.5B9" urn="urn:adsk.eagle:package:5419/1" type="box" library_version="2">
<description>MKS4, 31.6 x 9 mm, grid 27.5 mm</description>
<packageinstances>
<packageinstance name="C27.5B9"/>
</packageinstances>
</package3d>
<package3d name="C37.5B15" urn="urn:adsk.eagle:package:5425/1" type="box" library_version="2">
<description>MKP4, 42 x 15 mm, grid 37.5 mm</description>
<packageinstances>
<packageinstance name="C37.5B15"/>
</packageinstances>
</package3d>
<package3d name="C37.5B19" urn="urn:adsk.eagle:package:5422/1" type="box" library_version="2">
<description>MKP4, 42 x 19 mm, grid 37.5 mm</description>
<packageinstances>
<packageinstance name="C37.5B19"/>
</packageinstances>
</package3d>
<package3d name="C37.5B20" urn="urn:adsk.eagle:package:5423/1" type="box" library_version="2">
<description>MKP4, 42 x 20 mm, grid 37.5 mm</description>
<packageinstances>
<packageinstance name="C37.5B20"/>
</packageinstances>
</package3d>
<package3d name="C5B2.5" urn="urn:adsk.eagle:package:5426/1" type="box" library_version="2">
<description>MKS2, 7.5 x 2.5 mm, grid 5.08 mm</description>
<packageinstances>
<packageinstance name="C5B2.5"/>
</packageinstances>
</package3d>
<package3d name="C5B3" urn="urn:adsk.eagle:package:5433/1" type="box" library_version="2">
<description>MKS2, 7.5 x 3 mm, grid 5.08 mm</description>
<packageinstances>
<packageinstance name="C5B3"/>
</packageinstances>
</package3d>
<package3d name="C5B3.5" urn="urn:adsk.eagle:package:5427/1" type="box" library_version="2">
<description>MKS2, 7.5 x 4 mm, grid 5.08 mm</description>
<packageinstances>
<packageinstance name="C5B3.5"/>
</packageinstances>
</package3d>
<package3d name="C5B4.5" urn="urn:adsk.eagle:package:5424/1" type="box" library_version="2">
<description>MKS2, 7.5 x 4.5 mm, grid 5.08 mm</description>
<packageinstances>
<packageinstance name="C5B4.5"/>
</packageinstances>
</package3d>
<package3d name="C5B5" urn="urn:adsk.eagle:package:5428/1" type="box" library_version="2">
<description>MKS2, 7.5 x 5 mm, grid 5.08 mm</description>
<packageinstances>
<packageinstance name="C5B5"/>
</packageinstances>
</package3d>
<package3d name="C5B5.5" urn="urn:adsk.eagle:package:5429/1" type="box" library_version="2">
<description>MKS2, 7.5 x 5.5 mm, grid 5.08 mm</description>
<packageinstances>
<packageinstance name="C5B5.5"/>
</packageinstances>
</package3d>
<package3d name="C5B7.2" urn="urn:adsk.eagle:package:5430/1" type="box" library_version="2">
<description>MKS2, 7.5 x 7.2 mm, grid 5.08 mm</description>
<packageinstances>
<packageinstance name="C5B7.2"/>
</packageinstances>
</package3d>
<package3d name="C7.5B3" urn="urn:adsk.eagle:package:5434/1" type="box" library_version="2">
<description>MKS3, 10 x 3 mm, grid 7.62 mm</description>
<packageinstances>
<packageinstance name="C7.5B3"/>
</packageinstances>
</package3d>
<package3d name="C7.5B4" urn="urn:adsk.eagle:package:5431/1" type="box" library_version="2">
<description>MKS3, 10 x 4 mm, grid 7.62 mm</description>
<packageinstances>
<packageinstance name="C7.5B4"/>
</packageinstances>
</package3d>
<package3d name="C7.5B5" urn="urn:adsk.eagle:package:5432/1" type="box" library_version="2">
<description>MKS3, 10 x 5 mm, grid 7.62 mm</description>
<packageinstances>
<packageinstance name="C7.5B5"/>
</packageinstances>
</package3d>
<package3d name="C7.5B6" urn="urn:adsk.eagle:package:5435/1" type="box" library_version="2">
<description>MKS3, 10 x 6 mm, grid 7.62 mm</description>
<packageinstances>
<packageinstance name="C7.5B6"/>
</packageinstances>
</package3d>
<package3d name="C2.5-3" urn="urn:adsk.eagle:package:5436/1" type="box" library_version="2">
<description>MKS2, 5 x 3 mm, grid 2.54 mm</description>
<packageinstances>
<packageinstance name="C2.5-3"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="C" urn="urn:adsk.eagle:symbol:5352/1" library_version="2">
<wire x1="0" y1="-2.54" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-0.508" width="0.1524" layer="94"/>
<text x="1.524" y="0.381" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-4.699" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-1.016" x2="2.032" y2="-0.508" layer="94"/>
<rectangle x1="-2.032" y1="-2.032" x2="2.032" y2="-1.524" layer="94"/>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="C" urn="urn:adsk.eagle:component:5444/2" prefix="C" uservalue="yes" library_version="2">
<description>&lt;B&gt;CAPACITOR&lt;/B&gt;&lt;p&gt;
naming: grid - package width</description>
<gates>
<gate name="G$1" symbol="C" x="0" y="0"/>
</gates>
<devices>
<device name="10/4" package="C10B4">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5400/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="10/5" package="C10B5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5399/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="10/6" package="C10B6">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5401/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="15/5" package="C15B5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5402/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="1" constant="no"/>
</technology>
</technologies>
</device>
<device name="15/6" package="C15B6">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5403/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="15/7" package="C15B7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5404/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="15/8" package="C15B8">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5409/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="2" constant="no"/>
</technology>
</technologies>
</device>
<device name="15/9" package="C15B9">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5405/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="2.5/2" package="C2.5-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5415/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="28" constant="no"/>
</technology>
</technologies>
</device>
<device name="2.5/4" package="C2.5-4">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5408/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="6" constant="no"/>
</technology>
</technologies>
</device>
<device name="2.5/5" package="C2.5-5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5407/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="1" constant="no"/>
</technology>
</technologies>
</device>
<device name="2.5/6" package="C2.5-6">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5406/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="4" constant="no"/>
</technology>
</technologies>
</device>
<device name="22/10" package="C22.5B10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5411/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="22/11" package="C22.5B11">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5412/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="22/6" package="C22.5B6">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5410/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="22/7" package="C22.5B7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5414/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="22/8" package="C22.5B8">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5413/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="27/11" package="C27.5B11">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5416/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="1" constant="no"/>
</technology>
</technologies>
</device>
<device name="27/13" package="C27.5B13">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5420/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="27/15" package="C27.5B15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5417/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="27/17" package="C27.5B17">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5418/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="27/20" package="C27.5B20">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5421/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="27/9" package="C27.5B9">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5419/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="37/15" package="C37.5B15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5425/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="37/19" package="C37.5B19">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5422/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="37/20" package="C37.5B20">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5423/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="1" constant="no"/>
</technology>
</technologies>
</device>
<device name="5/2.5" package="C5B2.5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5426/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="10" constant="no"/>
</technology>
</technologies>
</device>
<device name="5/3" package="C5B3">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5433/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="4" constant="no"/>
</technology>
</technologies>
</device>
<device name="5/3.5" package="C5B3.5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5427/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="3" constant="no"/>
</technology>
</technologies>
</device>
<device name="5/4.5" package="C5B4.5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5424/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="5/5" package="C5B5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5428/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="1" constant="no"/>
</technology>
</technologies>
</device>
<device name="5/5.5" package="C5B5.5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5429/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="1" constant="no"/>
</technology>
</technologies>
</device>
<device name="5/7.2" package="C5B7.2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5430/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="1" constant="no"/>
</technology>
</technologies>
</device>
<device name="7.5/3" package="C7.5B3">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5434/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="5" constant="no"/>
</technology>
</technologies>
</device>
<device name="7.5/4" package="C7.5B4">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5431/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="7.5/5" package="C7.5B5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5432/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="7.5/6" package="C7.5B6">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5435/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="2,5-3" package="C2.5-3">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5436/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="38" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="resistor-power" urn="urn:adsk.eagle:library:344">
<description>&lt;b&gt;Vitrohm Power Resistors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="TO-247-2V" urn="urn:adsk.eagle:footprint:25093/1" library_version="2">
<description>&lt;b&gt;TO-247 Style  Power Package&lt;/b&gt; vertical mounting&lt;p&gt;
Source: www.token.com.tw .. power-resistor-rmg100pdf</description>
<wire x1="-7.9" y1="3.075" x2="7.9" y2="3.075" width="0.2032" layer="21"/>
<wire x1="-7.9" y1="-1.935" x2="-7.9" y2="3.075" width="0.2032" layer="21"/>
<wire x1="7.9" y1="3.075" x2="7.9" y2="-1.935" width="0.2032" layer="21"/>
<wire x1="-7.9" y1="-1.935" x2="7.9" y2="-1.935" width="0.2032" layer="51"/>
<wire x1="-1.85" y1="3" x2="-1.85" y2="-1.875" width="0.01" layer="21" style="shortdash"/>
<wire x1="1.85" y1="3.05" x2="1.85" y2="-1.875" width="0.01" layer="21" style="shortdash"/>
<wire x1="6.705" y1="-1.935" x2="7.9" y2="-1.935" width="0.2032" layer="21"/>
<wire x1="-3.455" y1="-1.935" x2="3.455" y2="-1.935" width="0.2032" layer="21"/>
<wire x1="-7.9" y1="-1.935" x2="-6.705" y2="-1.935" width="0.2032" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="2" diameter="4"/>
<pad name="2" x="5.08" y="0" drill="2" diameter="4"/>
<text x="-7.62" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-6.985" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="TO-247-2H" urn="urn:adsk.eagle:footprint:25094/1" library_version="2">
<description>&lt;b&gt;TO-247 Style  Power Package&lt;/b&gt; horizontal mounting&lt;p&gt;
Source: www.token.com.tw .. power-resistor-rmg100pdf</description>
<wire x1="-7.9" y1="5.5" x2="7.9" y2="5.5" width="0.2032" layer="21"/>
<wire x1="-7.9" y1="-15.3" x2="-7.9" y2="5.5" width="0.2032" layer="21"/>
<wire x1="7.9" y1="5.5" x2="7.9" y2="-15.3" width="0.2032" layer="21"/>
<wire x1="-7.9" y1="-15.3" x2="7.9" y2="-15.3" width="0.2032" layer="21"/>
<wire x1="-5.08" y1="-21.59" x2="-5.08" y2="-18.6" width="1.6" layer="51"/>
<wire x1="5.08" y1="-21.59" x2="5.08" y2="-18.6" width="1.6" layer="51"/>
<pad name="1" x="-5.08" y="-21.59" drill="2" diameter="4"/>
<pad name="2" x="5.08" y="-21.59" drill="2" diameter="4"/>
<text x="-7.62" y="6.35" size="1.27" layer="25">&gt;NAME</text>
<text x="-6.35" y="-6.35" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-7" y1="-18.875" x2="-3.175" y2="-15.3" layer="51"/>
<rectangle x1="3.16" y1="-18.875" x2="6.985" y2="-15.3" layer="51"/>
<hole x="0" y="0" drill="3.7"/>
</package>
</packages>
<packages3d>
<package3d name="TO-247-2V" urn="urn:adsk.eagle:package:25181/1" type="box" library_version="2">
<description>TO-247 Style  Power Package vertical mounting
Source: www.token.com.tw .. power-resistor-rmg100pdf</description>
<packageinstances>
<packageinstance name="TO-247-2V"/>
</packageinstances>
</package3d>
<package3d name="TO-247-2H" urn="urn:adsk.eagle:package:25183/1" type="box" library_version="2">
<description>TO-247 Style  Power Package horizontal mounting
Source: www.token.com.tw .. power-resistor-rmg100pdf</description>
<packageinstances>
<packageinstance name="TO-247-2H"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="R" urn="urn:adsk.eagle:symbol:25005/1" library_version="2">
<wire x1="-3.81" y1="-0.889" x2="3.81" y2="-0.889" width="0.254" layer="94"/>
<wire x1="3.81" y1="-0.889" x2="3.81" y2="0" width="0.254" layer="94"/>
<wire x1="3.81" y1="0.889" x2="-3.81" y2="0.889" width="0.254" layer="94"/>
<wire x1="-3.81" y1="-0.889" x2="-3.81" y2="0" width="0.254" layer="94"/>
<wire x1="3.81" y1="0" x2="5.08" y2="0" width="0.1524" layer="94"/>
<wire x1="3.81" y1="0" x2="3.81" y2="0.889" width="0.254" layer="94"/>
<wire x1="-5.08" y1="0" x2="-3.81" y2="0" width="0.1524" layer="94"/>
<wire x1="-3.81" y1="0" x2="-3.81" y2="0.889" width="0.254" layer="94"/>
<text x="-3.81" y="1.3716" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-2.921" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="RMG100" urn="urn:adsk.eagle:component:25185/2" prefix="R" uservalue="yes" library_version="2">
<description>&lt;b&gt;TO-247 Power Resistor - RMG 100 Series&lt;/b&gt;&lt;p&gt;
Source: www.token.com.tw .. power-resistor-rmg100pdf</description>
<gates>
<gate name="G$1" symbol="R" x="0" y="0"/>
</gates>
<devices>
<device name="-V" package="TO-247-2V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:25181/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="-H" package="TO-247-2H">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:25183/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="micro-philips" urn="urn:adsk.eagle:library:290">
<description>&lt;b&gt;Philips Microcontroller Devices&lt;/b&gt;&lt;p&gt;
http://www-eu2.semiconductors.com&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="HVQFN40" urn="urn:adsk.eagle:footprint:20122/1" library_version="4">
<description>&lt;b&gt;HVQFN40&lt;/b&gt; plastic thermal enhanced very thin quad flat package SOT618-1&lt;p&gt;
no leads; 40 terminals; body 6 x 6 x 0.85 mm&lt;br&gt;
Source: http://www.semiconductors.philips.com/acrobat_download/packages/SOT618-1.pdf</description>
<wire x1="-2.65" y1="2.2" x2="-2.65" y2="2.3" width="0.15" layer="51" curve="180"/>
<wire x1="-2.65" y1="1.7" x2="-2.65" y2="1.8" width="0.15" layer="51" curve="180"/>
<wire x1="-2.65" y1="1.2" x2="-2.65" y2="1.3" width="0.15" layer="51" curve="180"/>
<wire x1="-2.65" y1="0.7" x2="-2.65" y2="0.8" width="0.15" layer="51" curve="180"/>
<wire x1="-2.65" y1="0.2" x2="-2.65" y2="0.3" width="0.15" layer="51" curve="180"/>
<wire x1="-2.65" y1="-0.3" x2="-2.65" y2="-0.2" width="0.15" layer="51" curve="180"/>
<wire x1="-2.65" y1="-0.8" x2="-2.65" y2="-0.7" width="0.15" layer="51" curve="180"/>
<wire x1="-2.65" y1="-1.3" x2="-2.65" y2="-1.2" width="0.15" layer="51" curve="180"/>
<wire x1="-2.65" y1="-1.8" x2="-2.65" y2="-1.7" width="0.15" layer="51" curve="180"/>
<wire x1="-2.65" y1="-2.3" x2="-2.65" y2="-2.2" width="0.15" layer="51" curve="180"/>
<wire x1="-2.2" y1="-2.65" x2="-2.3" y2="-2.65" width="0.15" layer="51" curve="180"/>
<wire x1="-1.7" y1="-2.65" x2="-1.8" y2="-2.65" width="0.15" layer="51" curve="180"/>
<wire x1="-1.2" y1="-2.65" x2="-1.3" y2="-2.65" width="0.15" layer="51" curve="180"/>
<wire x1="-0.7" y1="-2.65" x2="-0.8" y2="-2.65" width="0.15" layer="51" curve="180"/>
<wire x1="-0.2" y1="-2.65" x2="-0.3" y2="-2.65" width="0.15" layer="51" curve="180"/>
<wire x1="0.3" y1="-2.65" x2="0.2" y2="-2.65" width="0.15" layer="51" curve="180"/>
<wire x1="0.8" y1="-2.65" x2="0.7" y2="-2.65" width="0.15" layer="51" curve="180"/>
<wire x1="1.3" y1="-2.65" x2="1.2" y2="-2.65" width="0.15" layer="51" curve="180"/>
<wire x1="1.8" y1="-2.65" x2="1.7" y2="-2.65" width="0.15" layer="51" curve="180"/>
<wire x1="2.3" y1="-2.65" x2="2.2" y2="-2.65" width="0.15" layer="51" curve="180"/>
<wire x1="2.65" y1="-2.2" x2="2.65" y2="-2.3" width="0.15" layer="51" curve="180"/>
<wire x1="2.65" y1="-1.7" x2="2.65" y2="-1.8" width="0.15" layer="51" curve="180"/>
<wire x1="2.65" y1="-1.2" x2="2.65" y2="-1.3" width="0.15" layer="51" curve="180"/>
<wire x1="2.65" y1="-0.7" x2="2.65" y2="-0.8" width="0.15" layer="51" curve="180"/>
<wire x1="2.65" y1="-0.2" x2="2.65" y2="-0.3" width="0.15" layer="51" curve="180"/>
<wire x1="2.65" y1="0.3" x2="2.65" y2="0.2" width="0.15" layer="51" curve="180"/>
<wire x1="2.65" y1="0.8" x2="2.65" y2="0.7" width="0.15" layer="51" curve="180"/>
<wire x1="2.65" y1="1.3" x2="2.65" y2="1.2" width="0.15" layer="51" curve="180"/>
<wire x1="2.65" y1="1.8" x2="2.65" y2="1.7" width="0.15" layer="51" curve="180"/>
<wire x1="2.65" y1="2.3" x2="2.65" y2="2.2" width="0.15" layer="51" curve="180"/>
<wire x1="2.2" y1="2.65" x2="2.3" y2="2.65" width="0.15" layer="51" curve="180"/>
<wire x1="1.7" y1="2.65" x2="1.8" y2="2.65" width="0.15" layer="51" curve="180"/>
<wire x1="1.2" y1="2.65" x2="1.3" y2="2.65" width="0.15" layer="51" curve="180"/>
<wire x1="0.7" y1="2.65" x2="0.8" y2="2.65" width="0.15" layer="51" curve="180"/>
<wire x1="0.2" y1="2.65" x2="0.3" y2="2.65" width="0.15" layer="51" curve="180"/>
<wire x1="-0.3" y1="2.65" x2="-0.2" y2="2.65" width="0.15" layer="51" curve="180"/>
<wire x1="-0.8" y1="2.65" x2="-0.7" y2="2.65" width="0.15" layer="51" curve="180"/>
<wire x1="-1.3" y1="2.65" x2="-1.2" y2="2.65" width="0.15" layer="51" curve="180"/>
<wire x1="-1.8" y1="2.65" x2="-1.7" y2="2.65" width="0.15" layer="51" curve="180"/>
<wire x1="-2.3" y1="2.65" x2="-2.2" y2="2.65" width="0.15" layer="51" curve="180"/>
<wire x1="-2.9" y1="2.9" x2="2.9" y2="2.9" width="0.2032" layer="51"/>
<wire x1="2.9" y1="2.9" x2="2.9" y2="-2.9" width="0.2032" layer="51"/>
<wire x1="2.9" y1="-2.9" x2="-2.9" y2="-2.9" width="0.2032" layer="51"/>
<wire x1="-2.9" y1="-2.9" x2="-2.9" y2="2.9" width="0.2032" layer="51"/>
<wire x1="-2.65" y1="1.7" x2="-2.65" y2="1.8" width="0.15" layer="51" curve="180"/>
<wire x1="-2.65" y1="1.2" x2="-2.65" y2="1.3" width="0.15" layer="51" curve="180"/>
<wire x1="-2.65" y1="0.7" x2="-2.65" y2="0.8" width="0.15" layer="51" curve="180"/>
<wire x1="-2.65" y1="0.2" x2="-2.65" y2="0.3" width="0.15" layer="51" curve="180"/>
<wire x1="-2.65" y1="-0.3" x2="-2.65" y2="-0.2" width="0.15" layer="51" curve="180"/>
<wire x1="-2.65" y1="-0.8" x2="-2.65" y2="-0.7" width="0.15" layer="51" curve="180"/>
<wire x1="-2.65" y1="-1.3" x2="-2.65" y2="-1.2" width="0.15" layer="51" curve="180"/>
<wire x1="-2.65" y1="-1.8" x2="-2.65" y2="-1.7" width="0.15" layer="51" curve="180"/>
<wire x1="-2.65" y1="-2.3" x2="-2.65" y2="-2.2" width="0.15" layer="51" curve="180"/>
<wire x1="-2.65" y1="-0.8" x2="-2.65" y2="-0.7" width="0.15" layer="51" curve="180"/>
<wire x1="-2.65" y1="-1.3" x2="-2.65" y2="-1.2" width="0.15" layer="51" curve="180"/>
<wire x1="-2.65" y1="-1.8" x2="-2.65" y2="-1.7" width="0.15" layer="51" curve="180"/>
<wire x1="-2.65" y1="-2.3" x2="-2.65" y2="-2.2" width="0.15" layer="51" curve="180"/>
<wire x1="-2.2" y1="-2.65" x2="-2.3" y2="-2.65" width="0.15" layer="51" curve="180"/>
<wire x1="-1.7" y1="-2.65" x2="-1.8" y2="-2.65" width="0.15" layer="51" curve="180"/>
<wire x1="-1.2" y1="-2.65" x2="-1.3" y2="-2.65" width="0.15" layer="51" curve="180"/>
<wire x1="-0.7" y1="-2.65" x2="-0.8" y2="-2.65" width="0.15" layer="51" curve="180"/>
<wire x1="-0.2" y1="-2.65" x2="-0.3" y2="-2.65" width="0.15" layer="51" curve="180"/>
<wire x1="-1.7" y1="-2.65" x2="-1.8" y2="-2.65" width="0.15" layer="51" curve="180"/>
<wire x1="-1.2" y1="-2.65" x2="-1.3" y2="-2.65" width="0.15" layer="51" curve="180"/>
<wire x1="-0.7" y1="-2.65" x2="-0.8" y2="-2.65" width="0.15" layer="51" curve="180"/>
<wire x1="-0.2" y1="-2.65" x2="-0.3" y2="-2.65" width="0.15" layer="51" curve="180"/>
<wire x1="0.3" y1="-2.65" x2="0.2" y2="-2.65" width="0.15" layer="51" curve="180"/>
<wire x1="0.8" y1="-2.65" x2="0.7" y2="-2.65" width="0.15" layer="51" curve="180"/>
<wire x1="1.3" y1="-2.65" x2="1.2" y2="-2.65" width="0.15" layer="51" curve="180"/>
<wire x1="1.8" y1="-2.65" x2="1.7" y2="-2.65" width="0.15" layer="51" curve="180"/>
<wire x1="2.3" y1="-2.65" x2="2.2" y2="-2.65" width="0.15" layer="51" curve="180"/>
<wire x1="0.8" y1="-2.65" x2="0.7" y2="-2.65" width="0.15" layer="51" curve="180"/>
<wire x1="1.3" y1="-2.65" x2="1.2" y2="-2.65" width="0.15" layer="51" curve="180"/>
<wire x1="1.8" y1="-2.65" x2="1.7" y2="-2.65" width="0.15" layer="51" curve="180"/>
<wire x1="2.3" y1="-2.65" x2="2.2" y2="-2.65" width="0.15" layer="51" curve="180"/>
<wire x1="2.65" y1="-2.2" x2="2.65" y2="-2.3" width="0.15" layer="51" curve="180"/>
<wire x1="2.65" y1="-1.7" x2="2.65" y2="-1.8" width="0.15" layer="51" curve="180"/>
<wire x1="2.65" y1="-1.2" x2="2.65" y2="-1.3" width="0.15" layer="51" curve="180"/>
<wire x1="2.65" y1="-0.7" x2="2.65" y2="-0.8" width="0.15" layer="51" curve="180"/>
<wire x1="2.65" y1="-0.2" x2="2.65" y2="-0.3" width="0.15" layer="51" curve="180"/>
<wire x1="2.65" y1="-1.7" x2="2.65" y2="-1.8" width="0.15" layer="51" curve="180"/>
<wire x1="2.65" y1="-1.2" x2="2.65" y2="-1.3" width="0.15" layer="51" curve="180"/>
<wire x1="2.65" y1="-0.7" x2="2.65" y2="-0.8" width="0.15" layer="51" curve="180"/>
<wire x1="2.65" y1="-0.2" x2="2.65" y2="-0.3" width="0.15" layer="51" curve="180"/>
<wire x1="2.65" y1="0.3" x2="2.65" y2="0.2" width="0.15" layer="51" curve="180"/>
<wire x1="2.65" y1="0.8" x2="2.65" y2="0.7" width="0.15" layer="51" curve="180"/>
<wire x1="2.65" y1="1.3" x2="2.65" y2="1.2" width="0.15" layer="51" curve="180"/>
<wire x1="2.65" y1="1.8" x2="2.65" y2="1.7" width="0.15" layer="51" curve="180"/>
<wire x1="2.65" y1="2.3" x2="2.65" y2="2.2" width="0.15" layer="51" curve="180"/>
<wire x1="2.65" y1="0.8" x2="2.65" y2="0.7" width="0.15" layer="51" curve="180"/>
<wire x1="2.65" y1="1.3" x2="2.65" y2="1.2" width="0.15" layer="51" curve="180"/>
<wire x1="2.65" y1="1.8" x2="2.65" y2="1.7" width="0.15" layer="51" curve="180"/>
<wire x1="2.65" y1="2.3" x2="2.65" y2="2.2" width="0.15" layer="51" curve="180"/>
<wire x1="2.2" y1="2.65" x2="2.3" y2="2.65" width="0.15" layer="51" curve="180"/>
<wire x1="1.7" y1="2.65" x2="1.8" y2="2.65" width="0.15" layer="51" curve="180"/>
<wire x1="1.2" y1="2.65" x2="1.3" y2="2.65" width="0.15" layer="51" curve="180"/>
<wire x1="0.7" y1="2.65" x2="0.8" y2="2.65" width="0.15" layer="51" curve="180"/>
<wire x1="0.2" y1="2.65" x2="0.3" y2="2.65" width="0.15" layer="51" curve="180"/>
<wire x1="1.7" y1="2.65" x2="1.8" y2="2.65" width="0.15" layer="51" curve="180"/>
<wire x1="1.2" y1="2.65" x2="1.3" y2="2.65" width="0.15" layer="51" curve="180"/>
<wire x1="0.7" y1="2.65" x2="0.8" y2="2.65" width="0.15" layer="51" curve="180"/>
<wire x1="0.2" y1="2.65" x2="0.3" y2="2.65" width="0.15" layer="51" curve="180"/>
<wire x1="-0.3" y1="2.65" x2="-0.2" y2="2.65" width="0.15" layer="51" curve="180"/>
<wire x1="-0.8" y1="2.65" x2="-0.7" y2="2.65" width="0.15" layer="51" curve="180"/>
<wire x1="-1.3" y1="2.65" x2="-1.2" y2="2.65" width="0.15" layer="51" curve="180"/>
<wire x1="-1.8" y1="2.65" x2="-1.7" y2="2.65" width="0.15" layer="51" curve="180"/>
<wire x1="-2.3" y1="2.65" x2="-2.2" y2="2.65" width="0.15" layer="51" curve="180"/>
<wire x1="-0.8" y1="2.65" x2="-0.7" y2="2.65" width="0.15" layer="51" curve="180"/>
<wire x1="-1.3" y1="2.65" x2="-1.2" y2="2.65" width="0.15" layer="51" curve="180"/>
<wire x1="-1.8" y1="2.65" x2="-1.7" y2="2.65" width="0.15" layer="51" curve="180"/>
<wire x1="-2.3" y1="2.65" x2="-2.2" y2="2.65" width="0.15" layer="51" curve="180"/>
<circle x="-2.65" y="2.25" radius="0.175" width="0" layer="29"/>
<circle x="-2.65" y="2.25" radius="0.1274" width="0" layer="31"/>
<circle x="-2.65" y="1.75" radius="0.175" width="0" layer="29"/>
<circle x="-2.65" y="1.75" radius="0.1274" width="0" layer="31"/>
<circle x="-2.65" y="1.25" radius="0.175" width="0" layer="29"/>
<circle x="-2.65" y="1.25" radius="0.1274" width="0" layer="31"/>
<circle x="-2.65" y="0.75" radius="0.175" width="0" layer="29"/>
<circle x="-2.65" y="0.75" radius="0.1274" width="0" layer="31"/>
<circle x="-2.65" y="0.25" radius="0.175" width="0" layer="29"/>
<circle x="-2.65" y="0.25" radius="0.1274" width="0" layer="31"/>
<circle x="-2.65" y="-0.25" radius="0.175" width="0" layer="29"/>
<circle x="-2.65" y="-0.25" radius="0.1274" width="0" layer="31"/>
<circle x="-2.65" y="-0.75" radius="0.175" width="0" layer="29"/>
<circle x="-2.65" y="-0.75" radius="0.1274" width="0" layer="31"/>
<circle x="-2.65" y="-1.25" radius="0.175" width="0" layer="29"/>
<circle x="-2.65" y="-1.25" radius="0.1274" width="0" layer="31"/>
<circle x="-2.65" y="-1.75" radius="0.175" width="0" layer="29"/>
<circle x="-2.65" y="-1.75" radius="0.1274" width="0" layer="31"/>
<circle x="-2.65" y="-2.25" radius="0.175" width="0" layer="29"/>
<circle x="-2.65" y="-2.25" radius="0.1274" width="0" layer="31"/>
<circle x="-2.25" y="-2.65" radius="0.175" width="0" layer="29"/>
<circle x="-2.25" y="-2.65" radius="0.1274" width="0" layer="31"/>
<circle x="-1.75" y="-2.65" radius="0.175" width="0" layer="29"/>
<circle x="-1.75" y="-2.65" radius="0.1274" width="0" layer="31"/>
<circle x="-1.25" y="-2.65" radius="0.175" width="0" layer="29"/>
<circle x="-1.25" y="-2.65" radius="0.1274" width="0" layer="31"/>
<circle x="-0.75" y="-2.65" radius="0.175" width="0" layer="29"/>
<circle x="-0.75" y="-2.65" radius="0.1274" width="0" layer="31"/>
<circle x="-0.25" y="-2.65" radius="0.175" width="0" layer="29"/>
<circle x="-0.25" y="-2.65" radius="0.1274" width="0" layer="31"/>
<circle x="0.25" y="-2.65" radius="0.175" width="0" layer="29"/>
<circle x="0.25" y="-2.65" radius="0.1274" width="0" layer="31"/>
<circle x="0.75" y="-2.65" radius="0.175" width="0" layer="29"/>
<circle x="0.75" y="-2.65" radius="0.1274" width="0" layer="31"/>
<circle x="1.25" y="-2.65" radius="0.175" width="0" layer="29"/>
<circle x="1.25" y="-2.65" radius="0.1274" width="0" layer="31"/>
<circle x="1.75" y="-2.65" radius="0.175" width="0" layer="29"/>
<circle x="1.75" y="-2.65" radius="0.1274" width="0" layer="31"/>
<circle x="2.25" y="-2.65" radius="0.175" width="0" layer="29"/>
<circle x="2.25" y="-2.65" radius="0.1274" width="0" layer="31"/>
<circle x="2.65" y="-2.25" radius="0.175" width="0" layer="29"/>
<circle x="2.65" y="-2.25" radius="0.1274" width="0" layer="31"/>
<circle x="2.65" y="-1.75" radius="0.175" width="0" layer="29"/>
<circle x="2.65" y="-1.75" radius="0.1274" width="0" layer="31"/>
<circle x="2.65" y="-1.25" radius="0.175" width="0" layer="29"/>
<circle x="2.65" y="-1.25" radius="0.1274" width="0" layer="31"/>
<circle x="2.65" y="-0.75" radius="0.175" width="0" layer="29"/>
<circle x="2.65" y="-0.75" radius="0.1274" width="0" layer="31"/>
<circle x="2.65" y="-0.25" radius="0.175" width="0" layer="29"/>
<circle x="2.65" y="-0.25" radius="0.1274" width="0" layer="31"/>
<circle x="2.65" y="0.25" radius="0.175" width="0" layer="29"/>
<circle x="2.65" y="0.25" radius="0.1274" width="0" layer="31"/>
<circle x="2.65" y="0.75" radius="0.175" width="0" layer="29"/>
<circle x="2.65" y="0.75" radius="0.1274" width="0" layer="31"/>
<circle x="2.65" y="1.25" radius="0.175" width="0" layer="29"/>
<circle x="2.65" y="1.25" radius="0.1274" width="0" layer="31"/>
<circle x="2.65" y="1.75" radius="0.175" width="0" layer="29"/>
<circle x="2.65" y="1.75" radius="0.1274" width="0" layer="31"/>
<circle x="2.65" y="2.25" radius="0.175" width="0" layer="29"/>
<circle x="2.65" y="2.25" radius="0.1274" width="0" layer="31"/>
<circle x="2.25" y="2.65" radius="0.175" width="0" layer="29"/>
<circle x="2.25" y="2.65" radius="0.1274" width="0" layer="31"/>
<circle x="1.75" y="2.65" radius="0.175" width="0" layer="29"/>
<circle x="1.75" y="2.65" radius="0.1274" width="0" layer="31"/>
<circle x="1.25" y="2.65" radius="0.175" width="0" layer="29"/>
<circle x="1.25" y="2.65" radius="0.1274" width="0" layer="31"/>
<circle x="0.75" y="2.65" radius="0.175" width="0" layer="29"/>
<circle x="0.75" y="2.65" radius="0.1274" width="0" layer="31"/>
<circle x="0.25" y="2.65" radius="0.175" width="0" layer="29"/>
<circle x="0.25" y="2.65" radius="0.1274" width="0" layer="31"/>
<circle x="-0.25" y="2.65" radius="0.175" width="0" layer="29"/>
<circle x="-0.25" y="2.65" radius="0.1274" width="0" layer="31"/>
<circle x="-0.75" y="2.65" radius="0.175" width="0" layer="29"/>
<circle x="-0.75" y="2.65" radius="0.1274" width="0" layer="31"/>
<circle x="-1.25" y="2.65" radius="0.175" width="0" layer="29"/>
<circle x="-1.25" y="2.65" radius="0.1274" width="0" layer="31"/>
<circle x="-1.75" y="2.65" radius="0.175" width="0" layer="29"/>
<circle x="-1.75" y="2.65" radius="0.1274" width="0" layer="31"/>
<circle x="-2.25" y="2.65" radius="0.175" width="0" layer="29"/>
<circle x="-2.25" y="2.65" radius="0.1274" width="0" layer="31"/>
<smd name="1" x="-2.75" y="2.25" dx="0.3" dy="0.5" layer="1" rot="R90" stop="no" cream="no"/>
<smd name="2" x="-2.75" y="1.75" dx="0.3" dy="0.5" layer="1" rot="R90" stop="no" cream="no"/>
<smd name="3" x="-2.75" y="1.25" dx="0.3" dy="0.5" layer="1" rot="R90" stop="no" cream="no"/>
<smd name="4" x="-2.75" y="0.75" dx="0.3" dy="0.5" layer="1" rot="R90" stop="no" cream="no"/>
<smd name="5" x="-2.75" y="0.25" dx="0.3" dy="0.5" layer="1" rot="R90" stop="no" cream="no"/>
<smd name="6" x="-2.75" y="-0.25" dx="0.3" dy="0.5" layer="1" rot="R90" stop="no" cream="no"/>
<smd name="7" x="-2.75" y="-0.75" dx="0.3" dy="0.5" layer="1" rot="R90" stop="no" cream="no"/>
<smd name="8" x="-2.75" y="-1.25" dx="0.3" dy="0.5" layer="1" rot="R90" stop="no" cream="no"/>
<smd name="9" x="-2.75" y="-1.75" dx="0.3" dy="0.5" layer="1" rot="R90" stop="no" cream="no"/>
<smd name="10" x="-2.75" y="-2.25" dx="0.3" dy="0.5" layer="1" rot="R90" stop="no" cream="no"/>
<smd name="11" x="-2.25" y="-2.75" dx="0.3" dy="0.5" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="12" x="-1.75" y="-2.75" dx="0.3" dy="0.5" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="13" x="-1.25" y="-2.75" dx="0.3" dy="0.5" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="14" x="-0.75" y="-2.75" dx="0.3" dy="0.5" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="15" x="-0.25" y="-2.75" dx="0.3" dy="0.5" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="16" x="0.25" y="-2.75" dx="0.3" dy="0.5" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="17" x="0.75" y="-2.75" dx="0.3" dy="0.5" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="18" x="1.25" y="-2.75" dx="0.3" dy="0.5" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="19" x="1.75" y="-2.75" dx="0.3" dy="0.5" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="20" x="2.25" y="-2.75" dx="0.3" dy="0.5" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="21" x="2.75" y="-2.25" dx="0.3" dy="0.5" layer="1" rot="R270" stop="no" cream="no"/>
<smd name="22" x="2.75" y="-1.75" dx="0.3" dy="0.5" layer="1" rot="R270" stop="no" cream="no"/>
<smd name="23" x="2.75" y="-1.25" dx="0.3" dy="0.5" layer="1" rot="R270" stop="no" cream="no"/>
<smd name="24" x="2.75" y="-0.75" dx="0.3" dy="0.5" layer="1" rot="R270" stop="no" cream="no"/>
<smd name="25" x="2.75" y="-0.25" dx="0.3" dy="0.5" layer="1" rot="R270" stop="no" cream="no"/>
<smd name="26" x="2.75" y="0.25" dx="0.3" dy="0.5" layer="1" rot="R270" stop="no" cream="no"/>
<smd name="27" x="2.75" y="0.75" dx="0.3" dy="0.5" layer="1" rot="R270" stop="no" cream="no"/>
<smd name="28" x="2.75" y="1.25" dx="0.3" dy="0.5" layer="1" rot="R270" stop="no" cream="no"/>
<smd name="29" x="2.75" y="1.75" dx="0.3" dy="0.5" layer="1" rot="R270" stop="no" cream="no"/>
<smd name="30" x="2.75" y="2.25" dx="0.3" dy="0.5" layer="1" rot="R270" stop="no" cream="no"/>
<smd name="31" x="2.25" y="2.75" dx="0.3" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="32" x="1.75" y="2.75" dx="0.3" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="33" x="1.25" y="2.75" dx="0.3" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="34" x="0.75" y="2.75" dx="0.3" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="35" x="0.25" y="2.75" dx="0.3" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="36" x="-0.25" y="2.75" dx="0.3" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="37" x="-0.75" y="2.75" dx="0.3" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="38" x="-1.25" y="2.75" dx="0.3" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="39" x="-1.75" y="2.75" dx="0.3" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="40" x="-2.25" y="2.75" dx="0.3" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="TH" x="0" y="0" dx="4.1" dy="4.1" layer="1" stop="no" cream="no"/>
<text x="-3.175" y="3.175" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.95" y1="2.075" x2="-2.7" y2="2.425" layer="51" rot="R90"/>
<rectangle x1="-2.95" y1="1.575" x2="-2.7" y2="1.925" layer="51" rot="R90"/>
<rectangle x1="-2.95" y1="1.075" x2="-2.7" y2="1.425" layer="51" rot="R90"/>
<rectangle x1="-2.95" y1="0.575" x2="-2.7" y2="0.925" layer="51" rot="R90"/>
<rectangle x1="-2.95" y1="0.075" x2="-2.7" y2="0.425" layer="51" rot="R90"/>
<rectangle x1="-2.95" y1="-0.425" x2="-2.7" y2="-0.075" layer="51" rot="R90"/>
<rectangle x1="-2.95" y1="-0.925" x2="-2.7" y2="-0.575" layer="51" rot="R90"/>
<rectangle x1="-2.95" y1="-1.425" x2="-2.7" y2="-1.075" layer="51" rot="R90"/>
<rectangle x1="-2.95" y1="-1.925" x2="-2.7" y2="-1.575" layer="51" rot="R90"/>
<rectangle x1="-2.95" y1="-2.425" x2="-2.7" y2="-2.075" layer="51" rot="R90"/>
<rectangle x1="-2.375" y1="-3" x2="-2.125" y2="-2.65" layer="51" rot="R180"/>
<rectangle x1="-1.875" y1="-3" x2="-1.625" y2="-2.65" layer="51" rot="R180"/>
<rectangle x1="-1.375" y1="-3" x2="-1.125" y2="-2.65" layer="51" rot="R180"/>
<rectangle x1="-0.875" y1="-3" x2="-0.625" y2="-2.65" layer="51" rot="R180"/>
<rectangle x1="-0.375" y1="-3" x2="-0.125" y2="-2.65" layer="51" rot="R180"/>
<rectangle x1="0.125" y1="-3" x2="0.375" y2="-2.65" layer="51" rot="R180"/>
<rectangle x1="0.625" y1="-3" x2="0.875" y2="-2.65" layer="51" rot="R180"/>
<rectangle x1="1.125" y1="-3" x2="1.375" y2="-2.65" layer="51" rot="R180"/>
<rectangle x1="1.625" y1="-3" x2="1.875" y2="-2.65" layer="51" rot="R180"/>
<rectangle x1="2.125" y1="-3" x2="2.375" y2="-2.65" layer="51" rot="R180"/>
<rectangle x1="2.7" y1="-2.425" x2="2.95" y2="-2.075" layer="51" rot="R270"/>
<rectangle x1="2.7" y1="-1.925" x2="2.95" y2="-1.575" layer="51" rot="R270"/>
<rectangle x1="2.7" y1="-1.425" x2="2.95" y2="-1.075" layer="51" rot="R270"/>
<rectangle x1="2.7" y1="-0.925" x2="2.95" y2="-0.575" layer="51" rot="R270"/>
<rectangle x1="2.7" y1="-0.425" x2="2.95" y2="-0.075" layer="51" rot="R270"/>
<rectangle x1="2.7" y1="0.075" x2="2.95" y2="0.425" layer="51" rot="R270"/>
<rectangle x1="2.7" y1="0.575" x2="2.95" y2="0.925" layer="51" rot="R270"/>
<rectangle x1="2.7" y1="1.075" x2="2.95" y2="1.425" layer="51" rot="R270"/>
<rectangle x1="2.7" y1="1.575" x2="2.95" y2="1.925" layer="51" rot="R270"/>
<rectangle x1="2.7" y1="2.075" x2="2.95" y2="2.425" layer="51" rot="R270"/>
<rectangle x1="2.125" y1="2.65" x2="2.375" y2="3" layer="51"/>
<rectangle x1="1.625" y1="2.65" x2="1.875" y2="3" layer="51"/>
<rectangle x1="1.125" y1="2.65" x2="1.375" y2="3" layer="51"/>
<rectangle x1="0.625" y1="2.65" x2="0.875" y2="3" layer="51"/>
<rectangle x1="0.125" y1="2.65" x2="0.375" y2="3" layer="51"/>
<rectangle x1="-0.375" y1="2.65" x2="-0.125" y2="3" layer="51"/>
<rectangle x1="-0.875" y1="2.65" x2="-0.625" y2="3" layer="51"/>
<rectangle x1="-1.375" y1="2.65" x2="-1.125" y2="3" layer="51"/>
<rectangle x1="-1.875" y1="2.65" x2="-1.625" y2="3" layer="51"/>
<rectangle x1="-2.375" y1="2.65" x2="-2.125" y2="3" layer="51"/>
<rectangle x1="-2.85" y1="0" x2="0" y2="2.85" layer="51"/>
<rectangle x1="-3.025" y1="2.075" x2="-2.65" y2="2.425" layer="29"/>
<rectangle x1="-2.975" y1="2.125" x2="-2.65" y2="2.375" layer="31"/>
<rectangle x1="-2.95" y1="1.575" x2="-2.7" y2="1.925" layer="51" rot="R90"/>
<rectangle x1="-3.025" y1="1.575" x2="-2.65" y2="1.925" layer="29"/>
<rectangle x1="-2.975" y1="1.625" x2="-2.65" y2="1.875" layer="31"/>
<rectangle x1="-2.95" y1="1.075" x2="-2.7" y2="1.425" layer="51" rot="R90"/>
<rectangle x1="-3.025" y1="1.075" x2="-2.65" y2="1.425" layer="29"/>
<rectangle x1="-2.975" y1="1.125" x2="-2.65" y2="1.375" layer="31"/>
<rectangle x1="-2.95" y1="0.575" x2="-2.7" y2="0.925" layer="51" rot="R90"/>
<rectangle x1="-3.025" y1="0.575" x2="-2.65" y2="0.925" layer="29"/>
<rectangle x1="-2.975" y1="0.625" x2="-2.65" y2="0.875" layer="31"/>
<rectangle x1="-2.95" y1="0.075" x2="-2.7" y2="0.425" layer="51" rot="R90"/>
<rectangle x1="-3.025" y1="0.075" x2="-2.65" y2="0.425" layer="29"/>
<rectangle x1="-2.975" y1="0.125" x2="-2.65" y2="0.375" layer="31"/>
<rectangle x1="-2.95" y1="-0.425" x2="-2.7" y2="-0.075" layer="51" rot="R90"/>
<rectangle x1="-2.95" y1="-0.925" x2="-2.7" y2="-0.575" layer="51" rot="R90"/>
<rectangle x1="-2.95" y1="-1.425" x2="-2.7" y2="-1.075" layer="51" rot="R90"/>
<rectangle x1="-2.95" y1="-1.925" x2="-2.7" y2="-1.575" layer="51" rot="R90"/>
<rectangle x1="-2.95" y1="-2.425" x2="-2.7" y2="-2.075" layer="51" rot="R90"/>
<rectangle x1="-3.025" y1="-0.425" x2="-2.65" y2="-0.075" layer="29"/>
<rectangle x1="-2.975" y1="-0.375" x2="-2.65" y2="-0.125" layer="31"/>
<rectangle x1="-2.95" y1="-0.925" x2="-2.7" y2="-0.575" layer="51" rot="R90"/>
<rectangle x1="-3.025" y1="-0.925" x2="-2.65" y2="-0.575" layer="29"/>
<rectangle x1="-2.975" y1="-0.875" x2="-2.65" y2="-0.625" layer="31"/>
<rectangle x1="-2.95" y1="-1.425" x2="-2.7" y2="-1.075" layer="51" rot="R90"/>
<rectangle x1="-3.025" y1="-1.425" x2="-2.65" y2="-1.075" layer="29"/>
<rectangle x1="-2.975" y1="-1.375" x2="-2.65" y2="-1.125" layer="31"/>
<rectangle x1="-2.95" y1="-1.925" x2="-2.7" y2="-1.575" layer="51" rot="R90"/>
<rectangle x1="-3.025" y1="-1.925" x2="-2.65" y2="-1.575" layer="29"/>
<rectangle x1="-2.975" y1="-1.875" x2="-2.65" y2="-1.625" layer="31"/>
<rectangle x1="-2.95" y1="-2.425" x2="-2.7" y2="-2.075" layer="51" rot="R90"/>
<rectangle x1="-3.025" y1="-2.425" x2="-2.65" y2="-2.075" layer="29"/>
<rectangle x1="-2.975" y1="-2.375" x2="-2.65" y2="-2.125" layer="31"/>
<rectangle x1="-2.375" y1="-3" x2="-2.125" y2="-2.65" layer="51" rot="R180"/>
<rectangle x1="-1.875" y1="-3" x2="-1.625" y2="-2.65" layer="51" rot="R180"/>
<rectangle x1="-1.375" y1="-3" x2="-1.125" y2="-2.65" layer="51" rot="R180"/>
<rectangle x1="-0.875" y1="-3" x2="-0.625" y2="-2.65" layer="51" rot="R180"/>
<rectangle x1="-0.375" y1="-3" x2="-0.125" y2="-2.65" layer="51" rot="R180"/>
<rectangle x1="-2.4375" y1="-3.0125" x2="-2.0625" y2="-2.6625" layer="29" rot="R90"/>
<rectangle x1="-2.4125" y1="-2.9375" x2="-2.0875" y2="-2.6875" layer="31" rot="R90"/>
<rectangle x1="-1.875" y1="-3" x2="-1.625" y2="-2.65" layer="51" rot="R180"/>
<rectangle x1="-1.9375" y1="-3.0125" x2="-1.5625" y2="-2.6625" layer="29" rot="R90"/>
<rectangle x1="-1.9125" y1="-2.9375" x2="-1.5875" y2="-2.6875" layer="31" rot="R90"/>
<rectangle x1="-1.375" y1="-3" x2="-1.125" y2="-2.65" layer="51" rot="R180"/>
<rectangle x1="-1.4375" y1="-3.0125" x2="-1.0625" y2="-2.6625" layer="29" rot="R90"/>
<rectangle x1="-1.4125" y1="-2.9375" x2="-1.0875" y2="-2.6875" layer="31" rot="R90"/>
<rectangle x1="-0.875" y1="-3" x2="-0.625" y2="-2.65" layer="51" rot="R180"/>
<rectangle x1="-0.9375" y1="-3.0125" x2="-0.5625" y2="-2.6625" layer="29" rot="R90"/>
<rectangle x1="-0.9125" y1="-2.9375" x2="-0.5875" y2="-2.6875" layer="31" rot="R90"/>
<rectangle x1="-0.375" y1="-3" x2="-0.125" y2="-2.65" layer="51" rot="R180"/>
<rectangle x1="-0.4375" y1="-3.0125" x2="-0.0625" y2="-2.6625" layer="29" rot="R90"/>
<rectangle x1="-0.4125" y1="-2.9375" x2="-0.0875" y2="-2.6875" layer="31" rot="R90"/>
<rectangle x1="0.125" y1="-3" x2="0.375" y2="-2.65" layer="51" rot="R180"/>
<rectangle x1="0.625" y1="-3" x2="0.875" y2="-2.65" layer="51" rot="R180"/>
<rectangle x1="1.125" y1="-3" x2="1.375" y2="-2.65" layer="51" rot="R180"/>
<rectangle x1="1.625" y1="-3" x2="1.875" y2="-2.65" layer="51" rot="R180"/>
<rectangle x1="2.125" y1="-3" x2="2.375" y2="-2.65" layer="51" rot="R180"/>
<rectangle x1="0.0625" y1="-3.0125" x2="0.4375" y2="-2.6625" layer="29" rot="R90"/>
<rectangle x1="0.0875" y1="-2.9375" x2="0.4125" y2="-2.6875" layer="31" rot="R90"/>
<rectangle x1="0.625" y1="-3" x2="0.875" y2="-2.65" layer="51" rot="R180"/>
<rectangle x1="0.5625" y1="-3.0125" x2="0.9375" y2="-2.6625" layer="29" rot="R90"/>
<rectangle x1="0.5875" y1="-2.9375" x2="0.9125" y2="-2.6875" layer="31" rot="R90"/>
<rectangle x1="1.125" y1="-3" x2="1.375" y2="-2.65" layer="51" rot="R180"/>
<rectangle x1="1.0625" y1="-3.0125" x2="1.4375" y2="-2.6625" layer="29" rot="R90"/>
<rectangle x1="1.0875" y1="-2.9375" x2="1.4125" y2="-2.6875" layer="31" rot="R90"/>
<rectangle x1="1.625" y1="-3" x2="1.875" y2="-2.65" layer="51" rot="R180"/>
<rectangle x1="1.5625" y1="-3.0125" x2="1.9375" y2="-2.6625" layer="29" rot="R90"/>
<rectangle x1="1.5875" y1="-2.9375" x2="1.9125" y2="-2.6875" layer="31" rot="R90"/>
<rectangle x1="2.125" y1="-3" x2="2.375" y2="-2.65" layer="51" rot="R180"/>
<rectangle x1="2.0625" y1="-3.0125" x2="2.4375" y2="-2.6625" layer="29" rot="R90"/>
<rectangle x1="2.0875" y1="-2.9375" x2="2.4125" y2="-2.6875" layer="31" rot="R90"/>
<rectangle x1="2.7" y1="-2.425" x2="2.95" y2="-2.075" layer="51" rot="R270"/>
<rectangle x1="2.7" y1="-1.925" x2="2.95" y2="-1.575" layer="51" rot="R270"/>
<rectangle x1="2.7" y1="-1.425" x2="2.95" y2="-1.075" layer="51" rot="R270"/>
<rectangle x1="2.7" y1="-0.925" x2="2.95" y2="-0.575" layer="51" rot="R270"/>
<rectangle x1="2.7" y1="-0.425" x2="2.95" y2="-0.075" layer="51" rot="R270"/>
<rectangle x1="2.65" y1="-2.425" x2="3.025" y2="-2.075" layer="29" rot="R180"/>
<rectangle x1="2.65" y1="-2.375" x2="2.975" y2="-2.125" layer="31" rot="R180"/>
<rectangle x1="2.7" y1="-1.925" x2="2.95" y2="-1.575" layer="51" rot="R270"/>
<rectangle x1="2.65" y1="-1.925" x2="3.025" y2="-1.575" layer="29" rot="R180"/>
<rectangle x1="2.65" y1="-1.875" x2="2.975" y2="-1.625" layer="31" rot="R180"/>
<rectangle x1="2.7" y1="-1.425" x2="2.95" y2="-1.075" layer="51" rot="R270"/>
<rectangle x1="2.65" y1="-1.425" x2="3.025" y2="-1.075" layer="29" rot="R180"/>
<rectangle x1="2.65" y1="-1.375" x2="2.975" y2="-1.125" layer="31" rot="R180"/>
<rectangle x1="2.7" y1="-0.925" x2="2.95" y2="-0.575" layer="51" rot="R270"/>
<rectangle x1="2.65" y1="-0.925" x2="3.025" y2="-0.575" layer="29" rot="R180"/>
<rectangle x1="2.65" y1="-0.875" x2="2.975" y2="-0.625" layer="31" rot="R180"/>
<rectangle x1="2.7" y1="-0.425" x2="2.95" y2="-0.075" layer="51" rot="R270"/>
<rectangle x1="2.65" y1="-0.425" x2="3.025" y2="-0.075" layer="29" rot="R180"/>
<rectangle x1="2.65" y1="-0.375" x2="2.975" y2="-0.125" layer="31" rot="R180"/>
<rectangle x1="2.7" y1="0.075" x2="2.95" y2="0.425" layer="51" rot="R270"/>
<rectangle x1="2.7" y1="0.575" x2="2.95" y2="0.925" layer="51" rot="R270"/>
<rectangle x1="2.7" y1="1.075" x2="2.95" y2="1.425" layer="51" rot="R270"/>
<rectangle x1="2.7" y1="1.575" x2="2.95" y2="1.925" layer="51" rot="R270"/>
<rectangle x1="2.7" y1="2.075" x2="2.95" y2="2.425" layer="51" rot="R270"/>
<rectangle x1="2.65" y1="0.075" x2="3.025" y2="0.425" layer="29" rot="R180"/>
<rectangle x1="2.65" y1="0.125" x2="2.975" y2="0.375" layer="31" rot="R180"/>
<rectangle x1="2.7" y1="0.575" x2="2.95" y2="0.925" layer="51" rot="R270"/>
<rectangle x1="2.65" y1="0.575" x2="3.025" y2="0.925" layer="29" rot="R180"/>
<rectangle x1="2.65" y1="0.625" x2="2.975" y2="0.875" layer="31" rot="R180"/>
<rectangle x1="2.7" y1="1.075" x2="2.95" y2="1.425" layer="51" rot="R270"/>
<rectangle x1="2.65" y1="1.075" x2="3.025" y2="1.425" layer="29" rot="R180"/>
<rectangle x1="2.65" y1="1.125" x2="2.975" y2="1.375" layer="31" rot="R180"/>
<rectangle x1="2.7" y1="1.575" x2="2.95" y2="1.925" layer="51" rot="R270"/>
<rectangle x1="2.65" y1="1.575" x2="3.025" y2="1.925" layer="29" rot="R180"/>
<rectangle x1="2.65" y1="1.625" x2="2.975" y2="1.875" layer="31" rot="R180"/>
<rectangle x1="2.7" y1="2.075" x2="2.95" y2="2.425" layer="51" rot="R270"/>
<rectangle x1="2.65" y1="2.075" x2="3.025" y2="2.425" layer="29" rot="R180"/>
<rectangle x1="2.65" y1="2.125" x2="2.975" y2="2.375" layer="31" rot="R180"/>
<rectangle x1="2.125" y1="2.65" x2="2.375" y2="3" layer="51"/>
<rectangle x1="1.625" y1="2.65" x2="1.875" y2="3" layer="51"/>
<rectangle x1="1.125" y1="2.65" x2="1.375" y2="3" layer="51"/>
<rectangle x1="0.625" y1="2.65" x2="0.875" y2="3" layer="51"/>
<rectangle x1="0.125" y1="2.65" x2="0.375" y2="3" layer="51"/>
<rectangle x1="2.0625" y1="2.6625" x2="2.4375" y2="3.0125" layer="29" rot="R270"/>
<rectangle x1="2.0875" y1="2.6875" x2="2.4125" y2="2.9375" layer="31" rot="R270"/>
<rectangle x1="1.625" y1="2.65" x2="1.875" y2="3" layer="51"/>
<rectangle x1="1.5625" y1="2.6625" x2="1.9375" y2="3.0125" layer="29" rot="R270"/>
<rectangle x1="1.5875" y1="2.6875" x2="1.9125" y2="2.9375" layer="31" rot="R270"/>
<rectangle x1="1.125" y1="2.65" x2="1.375" y2="3" layer="51"/>
<rectangle x1="1.0625" y1="2.6625" x2="1.4375" y2="3.0125" layer="29" rot="R270"/>
<rectangle x1="1.0875" y1="2.6875" x2="1.4125" y2="2.9375" layer="31" rot="R270"/>
<rectangle x1="0.625" y1="2.65" x2="0.875" y2="3" layer="51"/>
<rectangle x1="0.5625" y1="2.6625" x2="0.9375" y2="3.0125" layer="29" rot="R270"/>
<rectangle x1="0.5875" y1="2.6875" x2="0.9125" y2="2.9375" layer="31" rot="R270"/>
<rectangle x1="0.125" y1="2.65" x2="0.375" y2="3" layer="51"/>
<rectangle x1="0.0625" y1="2.6625" x2="0.4375" y2="3.0125" layer="29" rot="R270"/>
<rectangle x1="0.0875" y1="2.6875" x2="0.4125" y2="2.9375" layer="31" rot="R270"/>
<rectangle x1="-0.375" y1="2.65" x2="-0.125" y2="3" layer="51"/>
<rectangle x1="-0.875" y1="2.65" x2="-0.625" y2="3" layer="51"/>
<rectangle x1="-1.375" y1="2.65" x2="-1.125" y2="3" layer="51"/>
<rectangle x1="-1.875" y1="2.65" x2="-1.625" y2="3" layer="51"/>
<rectangle x1="-2.375" y1="2.65" x2="-2.125" y2="3" layer="51"/>
<rectangle x1="-0.4375" y1="2.6625" x2="-0.0625" y2="3.0125" layer="29" rot="R270"/>
<rectangle x1="-0.4125" y1="2.6875" x2="-0.0875" y2="2.9375" layer="31" rot="R270"/>
<rectangle x1="-0.875" y1="2.65" x2="-0.625" y2="3" layer="51"/>
<rectangle x1="-0.9375" y1="2.6625" x2="-0.5625" y2="3.0125" layer="29" rot="R270"/>
<rectangle x1="-0.9125" y1="2.6875" x2="-0.5875" y2="2.9375" layer="31" rot="R270"/>
<rectangle x1="-1.375" y1="2.65" x2="-1.125" y2="3" layer="51"/>
<rectangle x1="-1.4375" y1="2.6625" x2="-1.0625" y2="3.0125" layer="29" rot="R270"/>
<rectangle x1="-1.4125" y1="2.6875" x2="-1.0875" y2="2.9375" layer="31" rot="R270"/>
<rectangle x1="-1.875" y1="2.65" x2="-1.625" y2="3" layer="51"/>
<rectangle x1="-1.9375" y1="2.6625" x2="-1.5625" y2="3.0125" layer="29" rot="R270"/>
<rectangle x1="-1.9125" y1="2.6875" x2="-1.5875" y2="2.9375" layer="31" rot="R270"/>
<rectangle x1="-2.375" y1="2.65" x2="-2.125" y2="3" layer="51"/>
<rectangle x1="-2.4375" y1="2.6625" x2="-2.0625" y2="3.0125" layer="29" rot="R270"/>
<rectangle x1="-2.4125" y1="2.6875" x2="-2.0875" y2="2.9375" layer="31" rot="R270"/>
</package>
</packages>
<packages3d>
<package3d name="HVQFN40" urn="urn:adsk.eagle:package:20196/1" type="box" library_version="4">
<description>HVQFN40 plastic thermal enhanced very thin quad flat package SOT618-1
no leads; 40 terminals; body 6 x 6 x 0.85 mm
Source: http://www.semiconductors.philips.com/acrobat_download/packages/SOT618-1.pdf</description>
<packageinstances>
<packageinstance name="HVQFN40"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="PN531" urn="urn:adsk.eagle:symbol:20121/1" library_version="4">
<wire x1="-12.7" y1="30.48" x2="12.7" y2="30.48" width="0.254" layer="94"/>
<wire x1="12.7" y1="30.48" x2="12.7" y2="-30.48" width="0.254" layer="94"/>
<wire x1="12.7" y1="-30.48" x2="-12.7" y2="-30.48" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-30.48" x2="-12.7" y2="30.48" width="0.254" layer="94"/>
<text x="-12.7" y="31.75" size="1.778" layer="95">&gt;NAME</text>
<text x="-12.7" y="-33.02" size="1.778" layer="96">&gt;VALUE</text>
<pin name="DVDD" x="-15.24" y="27.94" length="short" direction="in"/>
<pin name="DVSS" x="-15.24" y="-27.94" length="short" direction="pwr"/>
<pin name="P32_INT0" x="15.24" y="15.24" length="short" rot="R180"/>
<pin name="P33_INT1" x="15.24" y="12.7" length="short" rot="R180"/>
<pin name="!RSTOUT" x="15.24" y="-15.24" length="short" rot="R180"/>
<pin name="IRQ" x="15.24" y="-17.78" length="short" direction="out" rot="R180"/>
<pin name="SCK" x="15.24" y="-2.54" length="short" rot="R180"/>
<pin name="MISO" x="15.24" y="-5.08" length="short" rot="R180"/>
<pin name="MOSI" x="15.24" y="-7.62" length="short" rot="R180"/>
<pin name="NSS" x="15.24" y="-10.16" length="short" rot="R180"/>
<pin name="P30" x="15.24" y="20.32" length="short" rot="R180"/>
<pin name="P31" x="15.24" y="17.78" length="short" rot="R180"/>
<pin name="P34" x="15.24" y="10.16" length="short" rot="R180"/>
<pin name="P35" x="15.24" y="7.62" length="short" rot="R180"/>
<pin name="OSC2IN" x="-15.24" y="15.24" length="short" direction="in"/>
<pin name="VBUS" x="15.24" y="2.54" length="short" direction="in" rot="R180"/>
<pin name="DELATT" x="15.24" y="-12.7" length="short" direction="out" rot="R180"/>
<pin name="TESTEN" x="-15.24" y="-20.32" length="short" direction="in"/>
<pin name="OSC2OUT" x="-15.24" y="12.7" length="short" direction="out"/>
<pin name="PVDD" x="15.24" y="25.4" length="short" direction="pwr" rot="R180"/>
<pin name="SVDD" x="15.24" y="27.94" length="short" direction="pwr" rot="R180"/>
<pin name="!RSTPD" x="15.24" y="0" length="short" direction="in" rot="R180"/>
<pin name="OSCOUT" x="-15.24" y="17.78" length="short" direction="out"/>
<pin name="OSCIN" x="-15.24" y="20.32" length="short" direction="in"/>
<pin name="TVDD" x="-15.24" y="2.54" length="short" direction="pwr"/>
<pin name="TVSS1" x="-15.24" y="7.62" length="short" direction="pwr"/>
<pin name="TX1" x="-15.24" y="5.08" length="short" direction="out"/>
<pin name="TX2" x="-15.24" y="0" length="short" direction="out"/>
<pin name="AVDD" x="-15.24" y="25.4" length="short" direction="pwr"/>
<pin name="AVSS" x="-15.24" y="-25.4" length="short" direction="pwr"/>
<pin name="RX" x="-15.24" y="-7.62" length="short" direction="in"/>
<pin name="VMID" x="-15.24" y="-5.08" length="short" direction="pwr"/>
<pin name="AUX1" x="-15.24" y="-10.16" length="short" direction="out"/>
<pin name="AUX2" x="-15.24" y="-12.7" length="short" direction="out"/>
<pin name="SIGOUT" x="15.24" y="-25.4" length="short" direction="out" rot="R180"/>
<pin name="SIGIN" x="15.24" y="-22.86" length="short" direction="in" rot="R180"/>
<pin name="LOADMOD" x="-15.24" y="10.16" length="short" direction="out"/>
<pin name="TVSS2" x="-15.24" y="-2.54" length="short" direction="pwr"/>
<pin name="I0" x="-15.24" y="-15.24" length="short" direction="in"/>
<pin name="I1" x="-15.24" y="-17.78" length="short" direction="in"/>
<pin name="TH" x="5.08" y="-33.02" visible="pad" length="short" direction="pas" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PN531" urn="urn:adsk.eagle:component:20230/2" prefix="IC" library_version="4">
<description>&lt;b&gt;Near Field Communication Transmission module&lt;/b&gt;&lt;p&gt;
Source: http://www.semiconductors.philips.com/acrobat_download/other/identification/sfs_pn531_rev2.0.pdf</description>
<gates>
<gate name="G$1" symbol="PN531" x="0" y="0"/>
</gates>
<devices>
<device name="" package="HVQFN40">
<connects>
<connect gate="G$1" pin="!RSTOUT" pad="29"/>
<connect gate="G$1" pin="!RSTPD" pad="38"/>
<connect gate="G$1" pin="AUX1" pad="12"/>
<connect gate="G$1" pin="AUX2" pad="13"/>
<connect gate="G$1" pin="AVDD" pad="8"/>
<connect gate="G$1" pin="AVSS" pad="11"/>
<connect gate="G$1" pin="DELATT" pad="30"/>
<connect gate="G$1" pin="DVDD" pad="39"/>
<connect gate="G$1" pin="DVSS" pad="1"/>
<connect gate="G$1" pin="I0" pad="16"/>
<connect gate="G$1" pin="I1" pad="17"/>
<connect gate="G$1" pin="IRQ" pad="28"/>
<connect gate="G$1" pin="LOADMOD" pad="2"/>
<connect gate="G$1" pin="MISO" pad="33"/>
<connect gate="G$1" pin="MOSI" pad="32"/>
<connect gate="G$1" pin="NSS" pad="31"/>
<connect gate="G$1" pin="OSC2IN" pad="20"/>
<connect gate="G$1" pin="OSC2OUT" pad="19"/>
<connect gate="G$1" pin="OSCIN" pad="14"/>
<connect gate="G$1" pin="OSCOUT" pad="15"/>
<connect gate="G$1" pin="P30" pad="27"/>
<connect gate="G$1" pin="P31" pad="26"/>
<connect gate="G$1" pin="P32_INT0" pad="37"/>
<connect gate="G$1" pin="P33_INT1" pad="36"/>
<connect gate="G$1" pin="P34" pad="22"/>
<connect gate="G$1" pin="P35" pad="21"/>
<connect gate="G$1" pin="PVDD" pad="35"/>
<connect gate="G$1" pin="RX" pad="10"/>
<connect gate="G$1" pin="SCK" pad="34"/>
<connect gate="G$1" pin="SIGIN" pad="24"/>
<connect gate="G$1" pin="SIGOUT" pad="23"/>
<connect gate="G$1" pin="SVDD" pad="25"/>
<connect gate="G$1" pin="TESTEN" pad="18"/>
<connect gate="G$1" pin="TH" pad="TH"/>
<connect gate="G$1" pin="TVDD" pad="5"/>
<connect gate="G$1" pin="TVSS1" pad="3"/>
<connect gate="G$1" pin="TVSS2" pad="7"/>
<connect gate="G$1" pin="TX1" pad="4"/>
<connect gate="G$1" pin="TX2" pad="6"/>
<connect gate="G$1" pin="VBUS" pad="40"/>
<connect gate="G$1" pin="VMID" pad="9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:20196/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="PN5310A3HN/C203,51" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="70R7886" constant="no"/>
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="crystal" urn="urn:adsk.eagle:library:204">
<description>&lt;b&gt;Crystals and Crystal Resonators&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="FC-12M" urn="urn:adsk.eagle:footprint:12056/1" library_version="2">
<description>&lt;b&gt;kHz RANGE CRYSTAL UNIT&lt;/b&gt;&lt;p&gt;
LOW PROFILE SMD&lt;b&gt;
Source: Epson Toyocom FC-12M.pdf</description>
<wire x1="-0.925" y1="0.5" x2="0.925" y2="0.5" width="0.2032" layer="51"/>
<wire x1="0.925" y1="0.5" x2="0.925" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="0.925" y1="-0.5" x2="-0.925" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="-0.925" y1="-0.5" x2="-0.925" y2="0.5" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="0.175" width="0" layer="21"/>
<smd name="1" x="-0.85" y="0" dx="0.7" dy="1.4" layer="1"/>
<smd name="2" x="0.85" y="0" dx="0.7" dy="1.4" layer="1"/>
<text x="-1.025" y="1.025" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.025" y="-2.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="FC-12M" urn="urn:adsk.eagle:package:12123/1" type="box" library_version="2">
<description>kHz RANGE CRYSTAL UNIT
LOW PROFILE SMD
Source: Epson Toyocom FC-12M.pdf</description>
<packageinstances>
<packageinstance name="FC-12M"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="Q" urn="urn:adsk.eagle:symbol:11991/1" library_version="2">
<wire x1="1.016" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="1.524" x2="-0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-0.381" y1="-1.524" x2="0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="-1.524" x2="0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="1.524" x2="-0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="1.016" y1="1.778" x2="1.016" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.778" x2="-1.016" y2="-1.778" width="0.254" layer="94"/>
<text x="2.54" y="1.016" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.159" y="-1.143" size="0.8636" layer="93">1</text>
<text x="1.524" y="-1.143" size="0.8636" layer="93">2</text>
<pin name="2" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="FC-12M" urn="urn:adsk.eagle:component:12181/2" prefix="Q" library_version="2">
<description>&lt;b&gt;kHz RANGE CRYSTAL UNIT&lt;/b&gt;&lt;p&gt;
LOW PROFILE SMD&lt;b&gt;
Source: Epson Toyocom FC-12M.pdf</description>
<gates>
<gate name="G$1" symbol="Q" x="0" y="0"/>
</gates>
<devices>
<device name="" package="FC-12M">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:12123/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="2" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="switch-omron" urn="urn:adsk.eagle:library:377">
<description>&lt;b&gt;Omron Switches&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="D-GS" urn="urn:adsk.eagle:symbol:27480/1" library_version="3">
<wire x1="0" y1="-3.175" x2="0" y2="-2.54" width="0.254" layer="95"/>
<wire x1="0" y1="2.54" x2="0" y2="3.175" width="0.254" layer="95"/>
<wire x1="0" y1="-2.54" x2="-0.635" y2="0" width="0.254" layer="95"/>
<wire x1="-2.54" y1="0" x2="-1.905" y2="0" width="0.1524" layer="95"/>
<wire x1="-1.27" y1="0" x2="-0.635" y2="0" width="0.1524" layer="95"/>
<wire x1="-0.635" y1="0" x2="-1.27" y2="2.54" width="0.254" layer="95"/>
<wire x1="0" y1="-3.175" x2="0" y2="-5.08" width="0.1524" layer="95"/>
<wire x1="0" y1="3.175" x2="0" y2="5.08" width="0.1524" layer="95"/>
<circle x="-3.81" y="0" radius="1.27" width="0.254" layer="95"/>
<text x="-6.35" y="-1.905" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="-3.81" y="2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="D-GS" urn="urn:adsk.eagle:component:27488/2" prefix="S" uservalue="yes" library_version="3">
<description>&lt;b&gt;SWITCH&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="D-GS" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-coax" urn="urn:adsk.eagle:library:133">
<description>&lt;b&gt;Coax Connectors&lt;/b&gt;&lt;p&gt;
Radiall  and M/A COM.&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="SMA-SMD" urn="urn:adsk.eagle:footprint:6194/1" library_version="2">
<description>&lt;b&gt;SMA 50 Ohm Straight Jack Receptacle - Surface Mount&lt;/b&gt;&lt;p&gt;
www.johnsoncomponents.com&lt;br&gt;
Source:  http://catalog.digikey.com .. 142-0711-201, 205, 202, 206.pdf</description>
<wire x1="-1.1" y1="3.2" x2="1.1" y2="3.2" width="0.2032" layer="21"/>
<wire x1="1.1" y1="-3.2" x2="-1.1" y2="-3.2" width="0.2032" layer="21"/>
<wire x1="-3.1999" y1="3.1999" x2="3.2" y2="3.2" width="0.2032" layer="51"/>
<wire x1="3.2" y1="3.2" x2="3.1999" y2="-3.1999" width="0.2032" layer="51"/>
<wire x1="3.1999" y1="-3.1999" x2="-3.2" y2="-3.2" width="0.2032" layer="51"/>
<wire x1="-3.2" y1="-3.2" x2="-3.1999" y2="3.1999" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="3.1999" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.7" width="0.2032" layer="21"/>
<smd name="1" x="0" y="0" dx="1.73" dy="1.73" layer="1" roundness="100"/>
<smd name="2@2" x="-2.375" y="-2.6" dx="2.35" dy="1.9" layer="1"/>
<smd name="2@1" x="-2.375" y="2.6" dx="2.35" dy="1.9" layer="1"/>
<smd name="2@5" x="-2.8" y="0" dx="1.5" dy="3.4" layer="1"/>
<smd name="2@4" x="2.375" y="2.6" dx="2.35" dy="1.9" layer="1" rot="R180"/>
<smd name="2@3" x="2.375" y="-2.6" dx="2.35" dy="1.9" layer="1" rot="R180"/>
<smd name="2@6" x="2.8" y="0" dx="1.5" dy="3.4" layer="1" rot="R180"/>
<text x="-2.54" y="4.445" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-5.715" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="SMA-SMD" urn="urn:adsk.eagle:package:6220/1" type="box" library_version="2">
<description>SMA 50 Ohm Straight Jack Receptacle - Surface Mount
www.johnsoncomponents.com
Source:  http://catalog.digikey.com .. 142-0711-201, 205, 202, 206.pdf</description>
<packageinstances>
<packageinstance name="SMA-SMD"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="BU-BNC" urn="urn:adsk.eagle:symbol:6169/1" library_version="2">
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="-2.54" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="0" y1="-2.54" x2="-0.762" y2="-1.778" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-0.508" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0.254" x2="-0.762" y2="0.254" width="0.254" layer="94"/>
<wire x1="-0.762" y1="0.254" x2="-0.508" y2="0" width="0.254" layer="94"/>
<wire x1="-0.508" y1="0" x2="-0.762" y2="-0.254" width="0.254" layer="94"/>
<wire x1="-0.762" y1="-0.254" x2="-2.54" y2="-0.254" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.54" y="3.302" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="2" x="2.54" y="-2.54" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SMA-SMD" urn="urn:adsk.eagle:component:6241/2" prefix="X" library_version="2">
<description>&lt;b&gt;SMA 50 Ohm Straight Jack Receptacle - Surface Mount&lt;/b&gt;&lt;p&gt;
www.johnsoncomponents.com&lt;br&gt;
Source:  http://catalog.digikey.com .. 142-0711-201, 205, 202, 206.pdf</description>
<gates>
<gate name="G$1" symbol="BU-BNC" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SMA-SMD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2@1 2@2 2@3 2@4 2@5 2@6"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:6220/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="C1" library="capacitor-wima" library_urn="urn:adsk.eagle:library:116" deviceset="C" device="5/4.5" package3d_urn="urn:adsk.eagle:package:5424/1"/>
<part name="C2" library="capacitor-wima" library_urn="urn:adsk.eagle:library:116" deviceset="C" device="5/4.5" package3d_urn="urn:adsk.eagle:package:5424/1"/>
<part name="C3" library="capacitor-wima" library_urn="urn:adsk.eagle:library:116" deviceset="C" device="5/4.5" package3d_urn="urn:adsk.eagle:package:5424/1"/>
<part name="C4" library="capacitor-wima" library_urn="urn:adsk.eagle:library:116" deviceset="C" device="5/4.5" package3d_urn="urn:adsk.eagle:package:5424/1"/>
<part name="C5" library="capacitor-wima" library_urn="urn:adsk.eagle:library:116" deviceset="C" device="5/4.5" package3d_urn="urn:adsk.eagle:package:5424/1"/>
<part name="C6" library="capacitor-wima" library_urn="urn:adsk.eagle:library:116" deviceset="C" device="5/4.5" package3d_urn="urn:adsk.eagle:package:5424/1"/>
<part name="C7" library="capacitor-wima" library_urn="urn:adsk.eagle:library:116" deviceset="C" device="5/4.5" package3d_urn="urn:adsk.eagle:package:5424/1"/>
<part name="C8" library="capacitor-wima" library_urn="urn:adsk.eagle:library:116" deviceset="C" device="5/4.5" package3d_urn="urn:adsk.eagle:package:5424/1"/>
<part name="C9" library="capacitor-wima" library_urn="urn:adsk.eagle:library:116" deviceset="C" device="5/4.5" package3d_urn="urn:adsk.eagle:package:5424/1"/>
<part name="C10" library="capacitor-wima" library_urn="urn:adsk.eagle:library:116" deviceset="C" device="5/4.5" package3d_urn="urn:adsk.eagle:package:5424/1"/>
<part name="C11" library="capacitor-wima" library_urn="urn:adsk.eagle:library:116" deviceset="C" device="5/4.5" package3d_urn="urn:adsk.eagle:package:5424/1"/>
<part name="R15" library="resistor-power" library_urn="urn:adsk.eagle:library:344" deviceset="RMG100" device="-V" package3d_urn="urn:adsk.eagle:package:25181/1"/>
<part name="L1" library="SamacSys_Parts" deviceset="LQG15WZ82NG02D" device=""/>
<part name="L6" library="SamacSys_Parts" deviceset="LQG15WZ82NG02D" device=""/>
<part name="IC1" library="micro-philips" library_urn="urn:adsk.eagle:library:290" deviceset="PN531" device="" package3d_urn="urn:adsk.eagle:package:20196/1"/>
<part name="C12" library="capacitor-wima" library_urn="urn:adsk.eagle:library:116" deviceset="C" device="5/4.5" package3d_urn="urn:adsk.eagle:package:5424/1"/>
<part name="Q2" library="crystal" library_urn="urn:adsk.eagle:library:204" deviceset="FC-12M" device="" package3d_urn="urn:adsk.eagle:package:12123/1"/>
<part name="S1" library="switch-omron" library_urn="urn:adsk.eagle:library:377" deviceset="D-GS" device=""/>
<part name="X1" library="con-coax" library_urn="urn:adsk.eagle:library:133" deviceset="SMA-SMD" device="" package3d_urn="urn:adsk.eagle:package:6220/1"/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="C1" gate="G$1" x="-27.94" y="124.46" smashed="yes">
<attribute name="NAME" x="-26.416" y="124.841" size="1.778" layer="95"/>
<attribute name="VALUE" x="-26.416" y="119.761" size="1.778" layer="96"/>
</instance>
<instance part="C2" gate="G$1" x="10.16" y="114.3" smashed="yes">
<attribute name="NAME" x="11.684" y="114.681" size="1.778" layer="95"/>
<attribute name="VALUE" x="11.684" y="109.601" size="1.778" layer="96"/>
</instance>
<instance part="C3" gate="G$1" x="45.72" y="109.22" smashed="yes" rot="R90">
<attribute name="NAME" x="42.799" y="110.744" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="50.419" y="110.744" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C4" gate="G$1" x="35.56" y="78.74" smashed="yes" rot="R90">
<attribute name="NAME" x="35.179" y="80.264" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="40.259" y="80.264" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C5" gate="G$1" x="30.48" y="86.36" smashed="yes" rot="R90">
<attribute name="NAME" x="22.479" y="85.344" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="35.179" y="87.884" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C6" gate="G$1" x="30.48" y="66.04" smashed="yes">
<attribute name="NAME" x="32.004" y="66.421" size="1.778" layer="95"/>
<attribute name="VALUE" x="32.004" y="61.341" size="1.778" layer="96"/>
</instance>
<instance part="C7" gate="G$1" x="2.54" y="114.3" smashed="yes">
<attribute name="NAME" x="4.064" y="114.681" size="1.778" layer="95"/>
<attribute name="VALUE" x="4.064" y="109.601" size="1.778" layer="96"/>
</instance>
<instance part="C8" gate="G$1" x="38.1" y="66.04" smashed="yes">
<attribute name="NAME" x="39.624" y="66.421" size="1.778" layer="95"/>
<attribute name="VALUE" x="39.624" y="61.341" size="1.778" layer="96"/>
</instance>
<instance part="C9" gate="G$1" x="121.92" y="68.58" smashed="yes">
<attribute name="NAME" x="123.444" y="71.501" size="1.778" layer="95"/>
<attribute name="VALUE" x="123.444" y="63.881" size="1.778" layer="96"/>
</instance>
<instance part="C10" gate="G$1" x="17.78" y="73.66" smashed="yes" rot="R90">
<attribute name="NAME" x="17.399" y="75.184" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="22.479" y="75.184" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C11" gate="G$1" x="-17.78" y="162.56" smashed="yes">
<attribute name="NAME" x="-11.176" y="155.321" size="1.778" layer="95"/>
<attribute name="VALUE" x="-16.256" y="157.861" size="1.778" layer="96"/>
</instance>
<instance part="R15" gate="G$1" x="63.5" y="66.04" smashed="yes" rot="R90">
<attribute name="NAME" x="62.1284" y="62.23" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="66.421" y="62.23" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="L1" gate="G$1" x="-5.08" y="81.28" smashed="yes" rot="R90">
<attribute name="NAME" x="-11.43" y="97.79" size="1.778" layer="95" rot="R90" align="center-left"/>
<attribute name="VALUE" x="-8.89" y="97.79" size="1.778" layer="96" rot="R90" align="center-left"/>
</instance>
<instance part="L6" gate="G$1" x="-27.94" y="127" smashed="yes" rot="R90">
<attribute name="NAME" x="-34.29" y="143.51" size="1.778" layer="95" rot="R90" align="center-left"/>
<attribute name="VALUE" x="-31.75" y="143.51" size="1.778" layer="96" rot="R90" align="center-left"/>
</instance>
<instance part="IC1" gate="G$1" x="88.9" y="83.82" smashed="yes">
<attribute name="NAME" x="76.2" y="115.57" size="1.778" layer="95"/>
<attribute name="VALUE" x="76.2" y="50.8" size="1.778" layer="96"/>
</instance>
<instance part="C12" gate="G$1" x="124.46" y="109.22" smashed="yes" rot="R270">
<attribute name="NAME" x="119.761" y="115.316" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="119.761" y="107.696" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="Q2" gate="G$1" x="40.64" y="81.28" smashed="yes" rot="R90">
<attribute name="NAME" x="39.624" y="83.82" size="1.778" layer="91" rot="R90"/>
<attribute name="VALUE" x="43.18" y="83.82" size="1.778" layer="91" rot="R90"/>
</instance>
<instance part="S1" gate="G$1" x="48.26" y="96.52" smashed="yes" rot="R180">
<attribute name="NAME" x="54.61" y="98.425" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="52.07" y="93.98" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="X1" gate="G$1" x="-7.62" y="165.1" smashed="yes">
<attribute name="VALUE" x="-10.16" y="160.02" size="1.778" layer="96"/>
<attribute name="NAME" x="-10.16" y="168.402" size="1.778" layer="95"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="N$1" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="TX1"/>
<wire x1="73.66" y1="88.9" x2="55.88" y2="88.9" width="0.1524" layer="91"/>
<wire x1="55.88" y1="88.9" x2="55.88" y2="96.52" width="0.1524" layer="91"/>
<wire x1="55.88" y1="96.52" x2="53.34" y2="96.52" width="0.1524" layer="91"/>
<label x="63.5" y="93.98" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="TVDD"/>
<wire x1="73.66" y1="86.36" x2="40.64" y2="86.36" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="2"/>
<junction x="35.56" y="86.36"/>
<pinref part="Q2" gate="G$1" pin="2"/>
<wire x1="40.64" y1="86.36" x2="35.56" y2="86.36" width="0.1524" layer="91"/>
<wire x1="40.64" y1="83.82" x2="40.64" y2="86.36" width="0.1524" layer="91"/>
<junction x="40.64" y="86.36"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="C5" gate="G$1" pin="1"/>
<junction x="27.94" y="86.36"/>
<wire x1="27.94" y1="86.36" x2="27.94" y2="78.74" width="0.1524" layer="91"/>
<wire x1="27.94" y1="78.74" x2="33.02" y2="78.74" width="0.1524" layer="91"/>
<pinref part="C4" gate="G$1" pin="1"/>
<junction x="33.02" y="78.74"/>
<wire x1="27.94" y1="78.74" x2="12.7" y2="78.74" width="0.1524" layer="91"/>
<junction x="27.94" y="78.74"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="C4" gate="G$1" pin="2"/>
<junction x="40.64" y="78.74"/>
<wire x1="40.64" y1="78.74" x2="50.8" y2="78.74" width="0.1524" layer="91"/>
<wire x1="50.8" y1="78.74" x2="50.8" y2="83.82" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="TX2"/>
<wire x1="50.8" y1="83.82" x2="73.66" y2="83.82" width="0.1524" layer="91"/>
<pinref part="Q2" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="TVSS2"/>
<wire x1="73.66" y1="81.28" x2="55.88" y2="81.28" width="0.1524" layer="91"/>
<wire x1="55.88" y1="81.28" x2="55.88" y2="73.66" width="0.1524" layer="91"/>
<wire x1="55.88" y1="73.66" x2="22.86" y2="73.66" width="0.1524" layer="91"/>
<pinref part="C10" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="AVDD"/>
<wire x1="73.66" y1="109.22" x2="50.8" y2="109.22" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="VMID"/>
<wire x1="73.66" y1="78.74" x2="60.96" y2="78.74" width="0.1524" layer="91"/>
<wire x1="60.96" y1="78.74" x2="60.96" y2="68.58" width="0.1524" layer="91"/>
<wire x1="60.96" y1="68.58" x2="38.1" y2="68.58" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="38.1" y1="68.58" x2="30.48" y2="68.58" width="0.1524" layer="91"/>
<wire x1="30.48" y1="68.58" x2="22.86" y2="68.58" width="0.1524" layer="91"/>
<junction x="30.48" y="68.58"/>
<pinref part="C8" gate="G$1" pin="1"/>
<junction x="38.1" y="68.58"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="C6" gate="G$1" pin="2"/>
<pinref part="C8" gate="G$1" pin="2"/>
<wire x1="30.48" y1="60.96" x2="35.56" y2="60.96" width="0.1524" layer="91"/>
<wire x1="35.56" y1="60.96" x2="38.1" y2="60.96" width="0.1524" layer="91"/>
<wire x1="35.56" y1="60.96" x2="35.56" y2="58.42" width="0.1524" layer="91"/>
<wire x1="35.56" y1="58.42" x2="22.86" y2="58.42" width="0.1524" layer="91"/>
<junction x="35.56" y="60.96"/>
<pinref part="R15" gate="G$1" pin="1"/>
<wire x1="63.5" y1="60.96" x2="48.26" y2="60.96" width="0.1524" layer="91"/>
<wire x1="48.26" y1="60.96" x2="48.26" y2="58.42" width="0.1524" layer="91"/>
<wire x1="48.26" y1="58.42" x2="35.56" y2="58.42" width="0.1524" layer="91"/>
<junction x="35.56" y="58.42"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="RX"/>
<wire x1="73.66" y1="76.2" x2="63.5" y2="76.2" width="0.1524" layer="91"/>
<wire x1="63.5" y1="76.2" x2="63.5" y2="71.12" width="0.1524" layer="91"/>
<pinref part="R15" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="AUX1"/>
<wire x1="73.66" y1="73.66" x2="66.04" y2="73.66" width="0.1524" layer="91"/>
<wire x1="66.04" y1="73.66" x2="66.04" y2="50.8" width="0.1524" layer="91"/>
<wire x1="66.04" y1="50.8" x2="-5.08" y2="50.8" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="50.8" x2="-5.08" y2="81.28" width="0.1524" layer="91"/>
<pinref part="L1" gate="G$1" pin="1"/>
<pinref part="C7" gate="G$1" pin="2"/>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="10.16" y1="109.22" x2="2.54" y2="109.22" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="81.28" x2="2.54" y2="81.28" width="0.1524" layer="91"/>
<wire x1="2.54" y1="81.28" x2="2.54" y2="109.22" width="0.1524" layer="91"/>
<junction x="-5.08" y="81.28"/>
<junction x="2.54" y="109.22"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="C1" gate="G$1" pin="1"/>
<pinref part="L6" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="L6" gate="G$1" pin="2"/>
<wire x1="-27.94" y1="147.32" x2="-27.94" y2="165.1" width="0.1524" layer="91"/>
<wire x1="-27.94" y1="165.1" x2="-17.78" y2="165.1" width="0.1524" layer="91"/>
<pinref part="C11" gate="G$1" pin="1"/>
<wire x1="-17.78" y1="165.1" x2="-10.16" y2="165.1" width="0.1524" layer="91"/>
<junction x="-17.78" y="165.1"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="OSCIN"/>
<wire x1="73.66" y1="104.14" x2="22.86" y2="104.14" width="0.1524" layer="91"/>
<wire x1="22.86" y1="104.14" x2="22.86" y2="116.84" width="0.1524" layer="91"/>
<wire x1="22.86" y1="116.84" x2="10.16" y2="116.84" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="1"/>
<pinref part="C7" gate="G$1" pin="1"/>
<wire x1="10.16" y1="116.84" x2="2.54" y2="116.84" width="0.1524" layer="91"/>
<junction x="10.16" y="116.84"/>
<wire x1="10.16" y1="116.84" x2="10.16" y2="124.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="43.18" y1="109.22" x2="38.1" y2="109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="DELATT"/>
<wire x1="104.14" y1="71.12" x2="121.92" y2="71.12" width="0.1524" layer="91"/>
<pinref part="C9" gate="G$1" pin="1"/>
<junction x="121.92" y="71.12"/>
<wire x1="121.92" y1="71.12" x2="139.7" y2="71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="MISO"/>
<wire x1="104.14" y1="78.74" x2="134.62" y2="78.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PVDD"/>
<wire x1="104.14" y1="109.22" x2="119.38" y2="109.22" width="0.1524" layer="91"/>
<pinref part="C12" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<wire x1="55.88" y1="124.46" x2="127" y2="124.46" width="0.1524" layer="91"/>
<pinref part="C12" gate="G$1" pin="1"/>
<wire x1="134.62" y1="124.46" x2="127" y2="124.46" width="0.1524" layer="91"/>
<wire x1="127" y1="124.46" x2="127" y2="109.22" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="DVDD"/>
<wire x1="73.66" y1="111.76" x2="55.88" y2="111.76" width="0.1524" layer="91"/>
<wire x1="55.88" y1="111.76" x2="55.88" y2="124.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="AUX2"/>
<wire x1="73.66" y1="71.12" x2="68.58" y2="71.12" width="0.1524" layer="91"/>
<wire x1="68.58" y1="71.12" x2="68.58" y2="30.48" width="0.1524" layer="91"/>
<wire x1="-27.94" y1="30.48" x2="-27.94" y2="101.6" width="0.1524" layer="91"/>
<pinref part="L1" gate="G$1" pin="2"/>
<wire x1="-5.08" y1="101.6" x2="-27.94" y2="101.6" width="0.1524" layer="91"/>
<wire x1="-27.94" y1="101.6" x2="-27.94" y2="119.38" width="0.1524" layer="91"/>
<junction x="-27.94" y="101.6"/>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="68.58" y1="30.48" x2="-27.94" y2="30.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<wire x1="48.26" y1="91.44" x2="33.02" y2="91.44" width="0.1524" layer="91"/>
<wire x1="33.02" y1="91.44" x2="33.02" y2="99.06" width="0.1524" layer="91"/>
<wire x1="33.02" y1="99.06" x2="30.48" y2="99.06" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="OSC2IN"/>
<wire x1="73.66" y1="99.06" x2="66.04" y2="99.06" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
